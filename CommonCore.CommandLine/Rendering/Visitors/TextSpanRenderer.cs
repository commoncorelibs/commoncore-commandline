﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.CommandLine.IO;

namespace CommonCore.CommandLine.Rendering.Visitors
{
    public class TextSpanRenderer : ASpanRenderer
    {
        public TextSpanRenderer(IStandardStreamWriter writer, Region region)
            : base(writer, region) { }

        private static int _maxTop = 0;
        protected override void SetCursorPosition(int? left = null, int? top = null)
        {
            bool change = top > _maxTop;
            int diff = change ? top.Value - _maxTop : 0;
            if (change)
            {
                if (diff > 1) { throw new System.InvalidOperationException("Not sure if this can happen here. Let me know if it does."); }
                this.Writer.WriteLine();
                if (left > 0) { this.Writer.Write(new string(' ', left.Value)); }
                _maxTop = top.Value;
            }
        }

        protected override void TryClearRemainingWidth() => this.ClearRemainingWidth();
    }
}
