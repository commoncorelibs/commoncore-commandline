# Vinland Solutions CommonCore.CommandLine Library
> ?

[![License](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/license/mit.svg)](https://opensource.org/licenses/MIT)
[![Platform](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/platform/netstandard/2.0_2.1.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Repository](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/repository.svg)](https://gitlab.com/commoncorelibs/commoncore-commandline)
[![Releases](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/releases.svg)](https://gitlab.com/commoncorelibs/commoncore-commandline/-/releases)
[![Documentation](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/documentation.svg)](https://commoncorelibs.gitlab.io/commoncore-commandline/)  
[![Nuget](https://badgen.net/nuget/v/VinlandSolutions.CommonCore.CommandLine/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.CommonCore.CommandLine/)
[![Pipeline](https://gitlab.com/commoncorelibs/commoncore-commandline/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/commoncore-commandline/pipelines)
[![Coverage](https://gitlab.com/commoncorelibs/commoncore-commandline/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/commoncore-commandline/reports/index.html)

**CommonCore.CommandLine** is a .Net Standard library for simplifying the creation of dotnet console applications.

The library provides a rendering layer for [System.CommandLine](https://github.com/dotnet/command-line-api), auto-generation of the console application structure from normal code and documentation comments, and various other features that allow quick application development with minimal console-related markup.

## Installation

The official release versions of the **CommonCore.CommandLine** library are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore.CommandLine/) and can be installed using the standard console means, or found through the Visual Studio NuGet Package Managers.

## Usage

```csharp
[EnableDefaultExceptionReporting]
[EnableDefaultErrorReporting]
[EnableDefaultGlobalOptionHelp]
[EnableDefaultLocalOptionLicense]
[EnableDefaultLocalOptionVersion]
[AppMITLicense]
static class Program
{
    /// <summary>
    /// This is an example Hello World application
    /// using the <c>CommonCore.CommandLine</c> library.
    /// </summary>
    [CommandEntryPoint]
    static void Main()
    {
        Console.WriteLine("Hello World!");
    }
}
```

Details, examples, and the full API reference can be found in the [documentation](https://commoncorelibs.gitlab.io/commoncore-commandline/) and those that wish to jump right in can start with the [library introduction](https://commoncorelibs.gitlab.io/commoncore-commandline/docs/index.html).

## Projects

The **CommonCore.CommandLine** repository is composed of three projects with the listed dependencies:

* **CommonCore.CommandLine**: The dotnet standard library project.
  * [System.CommandLine](https://github.com/dotnet/command-line-api)
* **CommonCore.CommandLine.Docs**: The project for generating api documentation.
  * [DocFX](https://github.com/dotnet/docfx)
* **CommonCore.CommandLine.Tests**: The project for running unit tests and generating coverage reports.
  * [xUnit](https://github.com/xunit/xunit)
  * [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Links

- Repository: https://gitlab.com/commoncorelibs/commoncore-commandline
- Issues: https://gitlab.com/commoncorelibs/commoncore-commandline/issues
- Docs: https://commoncorelibs.gitlab.io/commoncore-commandline/index.html
- Nuget: https://www.nuget.org/packages/VinlandSolutions.CommonCore.CommandLine/

## Credits

This library is a heavily modified fork of the [MIT](https://github.com/dotnet/command-line-api/blob/master/LICENSE.md) licensed **System.CommandLine.Rendering** package of the dotnet [System.CommandLine](https://github.com/dotnet/command-line-api) project.
