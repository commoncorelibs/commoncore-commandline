﻿//#region Copyright (c) 2020 Jay Jeckel
//// Copyright (c) 2020 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System.CommandLine;

//namespace CommonCore.CommandLine
//{
//    public class ExclusiveOption : Option
//    {
//        public ExclusiveOption(string alias, string description = null)
//            : base(alias, description) { }
//    }

//    public static class ExclusiveOptionExtensions
//    {
//        public static bool IsExclusive(this IOption option) => option is ExclusiveOption;
//    }
//}
