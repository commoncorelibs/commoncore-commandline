﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.Linq;
using System.Reflection;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Exceptions;
using CommonCore.CommandLine.Extensions;
using CommonCore.CommandLine.Help;
using CommonCore.CommandLine.Invocation;
using CommonCore.CommandLine.Symbols;

namespace CommonCore.CommandLine.AutoBuild.Extensions
{
    public static class CommandInfoExtensions
    {
        public static IEnumerable<Argument> ToArguments(this CommandInfo info)
            => from p in info.Parameters where p.IsArgument() select p.BuildArgument();

        public static IEnumerable<Option> ToOptions(this CommandInfo info)
            => from p in info.Parameters where p.IsOption() select p.BuildOption();

        public static HelpInfo ToHelp(this CommandInfo info, HelpDocReader reader = null)
            => (reader ?? Services.HelpDocReader).ParseHelp(info.Info ?? throw new ArgumentNullException(nameof(info)));

        public static CoreCommand ToRootCommand(this CommandInfo info)
        {
            var command = info.ToCommand();
            Type type = info.Info.DeclaringType;

            AppInfo.Instance = new AppInfo(type);

            if (type.HasCustomAttribute<EnableDefaultExceptionReportingAttribute>())
            { command.AddMiddleware(new DefaultExceptionHandlerMiddleware()); }

            if (type.HasCustomAttribute<EnableDefaultErrorReportingAttribute>())
            { command.AddMiddleware(new DefaultErrorReportingMiddleware()); }

            var helpAttribute = type.GetCustomAttribute<EnableDefaultGlobalOptionHelpAttribute>();
            if (helpAttribute != null)
            {
                command.AddGlobalOption(new Option<bool>(new[] { "--help" },
                    "Print the program help information.")
                { IsHidden = helpAttribute.Hidden });

                command.AddMiddleware(new DefaultGlobalOptionHelpMiddleware());
            }

            var licenseAttribute = type.GetCustomAttribute<EnableDefaultLocalOptionLicenseAttribute>();
            if (licenseAttribute != null)
            {
                command.AddOption(new Option<bool>(new[] { "--license" },
                    "Print the program license information.")
                { IsHidden = licenseAttribute.Hidden });

                command.AddMiddleware(new DefaultGlobalOptionLicenseMiddleware());
            }

            var versionAttribute = type.GetCustomAttribute<EnableDefaultLocalOptionVersionAttribute>();
            if (versionAttribute != null)
            {
                command.AddOption(new Option<bool>(new[] { "--version" },
                    "Print the program version information.")
                { IsHidden = versionAttribute.Hidden });

                command.AddMiddleware(new DefaultGlobalOptionVersionMiddleware());
            }

            return command;
        }

        public static CoreCommand ToCommand(this CommandInfo info)
        {
            Thrower.If.IsDefault(info, nameof(info)).Throw();

            var command = new CoreCommand(info.CommandName);
            command.AddArgument(info.ToArguments());
            command.AddOption(info.ToOptions());
            command.HelpInfo = info.ToHelp();
            command.Handler = CommandHandler.Create(info.Info);

            command.AddValidator(info.Validators.Select(method => method.CreateDelegate<ValidateSymbol<CommandResult>>()));
            command.AddMiddleware(info.Middlewares.Select(ware
                => new Middleware(ware.OrderBase, ware.OrderOffset, ware.Method.CreateDelegate<InvocationMiddleware>())));
            //command.AddCommand(info.Commands.Select(subinfo => subinfo.ToCommand()));
            //command.AddUsage(info.Commands.Where(subinfo => subinfo.IncludeInParentUsage).Select(subinfo => subinfo.ToCommand().ToUsage()));
            foreach (var verbinfo in info.Commands)
            {
                var verb = verbinfo.ToCommand();
                command.AddCommand(verb);
                if (verbinfo.IncludeInParentUsage)
                { command.AddUsage(verb); }
            }

            var help = command.HelpInfo;
            if (help != null)
            {
                command.Description = help.SummaryViews?.FirstOrDefault()?.ToString();
                if (help.ParamViewPairs != null)
                {
                    var options = command.Options.ToArray();
                    var arguments = command.Arguments.ToArray();

                    foreach (var pair in help.ParamViewPairs)
                    {
                        Symbol symbol
                            = (Symbol)options.FirstOrDefault(o => pair.Name == o.Name || o.HasAlias(pair.Name))
                            ?? arguments.FirstOrDefault(a => pair.Name == a.Name || a.HasAlias(pair.Name));
                        if (symbol != null) { symbol.Description = pair.Views?[0]?.ToString(); }
                    }
                }
            }

            object[] arg = new[] { command };
            foreach (var initializer in info.Initializers)
            { initializer.Invoke(null, arg); }

            return command;
        }
    }
}
