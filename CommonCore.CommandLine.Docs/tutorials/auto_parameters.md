﻿# Auto Parameters

The library is able to populate command methods with several objects that are used as part of processing the command line. These are known as **auto-parameters** and there are currently eight of them by default: `Commander`, `AppInfo`, `Services`, `InvocationContext`, `BindingContext`, `ParseResult`, `IConsole`, and `CancellationToken`. Of them, the most useful are `IConsole` and, to a lesser extent, `AppInfo` and `ParseResult`; the former allows writing to the associated console and the later two represent standard information about the application and the result of parsing the command line, respectively.

Auto-parameters are ignored when the auto-build system creates options and arguments from a command method.

As a demonstration, add `IConsole console` parameter to the `Main()` method and use the `IConsole` instead of `System.Console` to write `"Hello World!!"`. When the command method is called by the auto-build system, the auto-parameter will be automatically set to the `IConsole` associated with the parsing process.

```csharp
[CommandEntryPoint]
static void Main(IConsole console)
{
    //Console.WriteLine("Hello World!");
    console.Out.WriteLine("Hello World!!");
}
```

Run the application and **Hello World!!**, with two exclamation marks instead of one, will be written to the console.
