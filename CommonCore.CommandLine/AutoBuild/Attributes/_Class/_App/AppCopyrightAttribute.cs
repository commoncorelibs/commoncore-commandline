﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Reflection;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="AppCopyrightAttribute"/> <c>class</c> is used to provide the system
    /// with copyright information about the implementing console application.
    /// 
    /// <para>
    /// The attribute should be applied to the implementing console application's main <c>class</c>,
    /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> <c>method</c>.
    /// This is generally the <c>Program</c> class.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class AppCopyrightAttribute : Attribute
    {
        /// <summary>
        /// Gets the copyright notice of the application or the default copyright
        /// notice built from available information.
        /// See <see cref="BuildDefaultCopyright(Assembly)"/> for more details.
        /// </summary>
        public string Copyright
        {
            get => this._copyright ??= this.BuildDefaultCopyright(AssemblyHelper.GetAssembly());
            set => this._copyright = value;
        }
        private string _copyright = null;

        /// <summary>
        /// Gets the holder name of the application copyright.
        /// If this property is set then <see cref="CopyrightYear"/>
        /// must also be set.
        /// </summary>
        public string CopyrightHolder { get; set; }

        /// <summary>
        /// Gets the year of the application copyright.
        /// If this property is set then <see cref="CopyrightHolder"/>
        /// must also be set.
        /// </summary>
        public string CopyrightYear { get; set; }

        /// <summary>
        /// Builds the default copyright notice for the specified assembly.
        /// If <see cref="CopyrightHolder"/> and <see cref="CopyrightYear"/>
        /// are set, then they will be used to build the copyright notice.
        /// If one of <see cref="CopyrightHolder"/> and <see cref="CopyrightYear"/>
        /// are set, but the other isn't, then an exception will be thrown.
        /// If neither <see cref="CopyrightHolder"/> nor <see cref="CopyrightYear"/>
        /// are set, then the assembly copyright value will be used.
        /// If no assembly copyright value is found, then the literal string
        /// <c>Copyright (c) 0000 UNKNOWN</c> will be returned.
        /// </summary>
        /// <param name="assembly">The assembly for which to build a copyright notice.</param>
        /// <returns>The default license summary string.</returns>
        public string BuildDefaultCopyright(Assembly assembly)
        {
            if (this.CopyrightHolder is null && this.CopyrightYear is null)
            { return assembly.GetCopyright() ?? "Copyright (c) 0000 UNKNOWN"; }
            else if (this.CopyrightHolder is null || this.CopyrightYear is null)
            { throw new InvalidOperationException($"CopyrightHolder '{this.CopyrightHolder}' and CopyrightYear '{this.CopyrightYear}' must both be null or non-null."); }
            else
            { return $"Copyright (c) {this.CopyrightYear} {this.CopyrightHolder}"; }
        }
    }
}
