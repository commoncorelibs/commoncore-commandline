﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion


namespace CommonCore.CommandLine.Rendering.Views.Spans
{
    public class BackColorSpan : AColorSpan
    {
        public BackColorSpan(string name, Ansi ansiControlCode)
            : base(name, ansiControlCode) { }

        public BackColorSpan(Color rgbColor)
            : base(rgbColor, AnsiForeColors.Rgb(rgbColor.R, rgbColor.G, rgbColor.B)) { }

        public BackColorSpan(byte r, byte g, byte b)
            : this(new Color(r, g, b)) { }

        public static BackColorSpan Reset() => new BackColorSpan(nameof(Reset), AnsiBackColors.Default);

        public static BackColorSpan Black() => new BackColorSpan(nameof(Black), AnsiBackColors.Black);

        public static BackColorSpan Red() => new BackColorSpan(nameof(Red), AnsiBackColors.Red);

        public static BackColorSpan Green() => new BackColorSpan(nameof(Green), AnsiBackColors.Green);

        public static BackColorSpan Yellow() => new BackColorSpan(nameof(Yellow), AnsiBackColors.Yellow);

        public static BackColorSpan Blue() => new BackColorSpan(nameof(Blue), AnsiBackColors.Blue);

        public static BackColorSpan Magenta() => new BackColorSpan(nameof(Magenta), AnsiBackColors.Magenta);

        public static BackColorSpan Cyan() => new BackColorSpan(nameof(Cyan), AnsiBackColors.Cyan);

        public static BackColorSpan White() => new BackColorSpan(nameof(White), AnsiBackColors.White);

        public static BackColorSpan DarkGray() => new BackColorSpan(nameof(DarkGray), AnsiBackColors.BrightBlack);

        public static BackColorSpan LightRed() => new BackColorSpan(nameof(LightRed), AnsiBackColors.BrightRed);

        public static BackColorSpan LightGreen() => new BackColorSpan(nameof(LightGreen), AnsiBackColors.BrightGreen);

        public static BackColorSpan LightYellow() => new BackColorSpan(nameof(LightYellow), AnsiBackColors.BrightYellow);

        public static BackColorSpan LightBlue() => new BackColorSpan(nameof(LightBlue), AnsiBackColors.BrightBlue);

        public static BackColorSpan LightMagenta() => new BackColorSpan(nameof(LightMagenta), AnsiBackColors.BrightMagenta);

        public static BackColorSpan LightCyan() => new BackColorSpan(nameof(LightCyan), AnsiBackColors.BrightCyan);

        public static BackColorSpan LightGray() => new BackColorSpan(nameof(LightGray), AnsiBackColors.BrightWhite);

        public static BackColorSpan Rgb(byte r, byte g, byte b) => new BackColorSpan(r, g, b);
    }
}
