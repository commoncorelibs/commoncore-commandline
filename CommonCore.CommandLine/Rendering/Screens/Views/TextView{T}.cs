﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.CommandLine.Rendering.Views
{
    public class TextView<T> : TextView
    {
        public TextView(T value)
        {
            this.Value = value;
        }

        // TODO: Should make this settable.
        // Would reformat value to span and call update.
        public T Value { get; }

        public override Size Measure(IConsoleRenderer renderer, Size maxSize)
        {
            this.EnsureSpanCreated(renderer);
            return base.Measure(renderer, maxSize);
        }

        public override void Render(IConsoleRenderer renderer, Region region)
        {
            this.EnsureSpanCreated(renderer);
            base.Render(renderer, region);
        }

        private void EnsureSpanCreated(IConsoleRenderer renderer)
            => this.Span ??= renderer.Formatter.Format(this.Value);
    }
}
