﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine.Suggestions;
using System.Linq;

namespace CommonCore.CommandLine.Suggestions
{
    public class SuggestSource : ISuggestionSource
    {
        public static ISuggestionSource Create(IEnumerable<string> suggestions)
        {
        //    if (type is null) { throw new ArgumentNullException(nameof(type)); }
        //    suggestions ??= Array.Empty<string>();

        //    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
        //    { type = type.GetGenericArguments().Single(); }

            return new SuggestSource(_ => suggestions);
        }
        public SuggestSource(Suggest suggestions)
            => this.SuggestDelegate = suggestions ?? throw new ArgumentNullException(nameof(suggestions));

        protected Suggest SuggestDelegate { get; }

        public IEnumerable<string> GetSuggestions(string textToMatch = null)
            => this.SuggestDelegate(textToMatch);
    }
}
