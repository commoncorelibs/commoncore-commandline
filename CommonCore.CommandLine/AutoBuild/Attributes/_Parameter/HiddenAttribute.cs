﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="HiddenAttribute"/> <c>class</c>
    /// provides a way to signal the system that a command method parameter
    /// should be hidden from display on default screens.
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class HiddenAttribute : Attribute { }
}
