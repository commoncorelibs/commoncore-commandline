﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine.Binding;
using System.CommandLine.Parsing;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Factories
{
    public class TypeParseProvider : AServiceProvider<ParseArgument<object>>
    {
        public static TypeParseProvider Instance
        {
            get => _instance ??= new TypeParseProvider();
            set
            {
                if (_instance != null) { throw new InvalidOperationException($"{nameof(TypeParseProvider)}.{nameof(Instance)} singleton cannot be set because it is already initialized."); }
                _instance = value ?? throw new InvalidOperationException($"{nameof(TypeParseProvider)}.{nameof(Instance)} singleton cannot be set to 'null'.");
            }
        }
        private static TypeParseProvider _instance = null;

        public static IDictionary<Type, Func<string, object>> GetArgumentConverterDict()
        {
            //Dictionary<Type, Func<string, object>> ArgumentConverter._converters
            var argumentConverterType = typeof(BindingContext).Assembly.GetType("System.CommandLine.Binding.ArgumentConverter");
            return ReflectionHelper.GetFieldValue<IDictionary<Type, Func<string, object>>>(argumentConverterType, "_converters");
        }

        public TypeParseProvider() : base(new Dictionary<Type, ParseArgument<object>>()) { }
    }
}
