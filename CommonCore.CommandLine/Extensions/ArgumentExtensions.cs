﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.CommandLine;
using System.Linq;

namespace CommonCore.CommandLine.Extensions
{
    public static class ArgumentExtensions
    {
        public static string ToHelpInvocation(this IArgument argument)
            => $"<{argument.ToInvocation() ?? "null"}>";

        public static string ToHelpDescription(this IArgument argument)
        {
            var desc = argument.ToSummary();
            var meta = argument.ToMeta();
            if (desc is null && meta is null) { return null; }
            return desc is null ? meta : meta is null ? desc : $"[{meta}] {desc}";
        }

        public static string ToInvocation(this IArgument argument)
        {
            var descriptor = argument.ToDescriptor();
            return descriptor ?? argument.ValueName;
        }

        public static string ToDescriptor(this IArgument argument)
        {
            if (argument.ValueType == typeof(bool) || argument.ValueType == typeof(bool?)) { return null; }
            var suggestions = argument.GetSuggestions().ToArray();
            if (suggestions.Length > 0 && argument.ValueType.IsEnum)
            { suggestions = suggestions.Select(s => s.ToLower()).ToArray(); }
            return suggestions.Length == 0 ? argument.Name : string.Join("|", suggestions);
        }

        public static string ToDescription(this IArgument argument)
        {
            var desc = argument.Description;
            return string.IsNullOrWhiteSpace(desc) ? null : desc;
        }

        public static string ToMeta(this IArgument argument)
        {
            var hint = argument.ToDefaultHint();
            var list = new List<string>();
            if (!argument.HasDefaultValue) { list.Add("REQUIRED"); }
            if (hint != null) { list.Add($"default: {hint}"); }
            return list.Count == 0 ? null : string.Join(", ", list);
        }

        public static string ToDefaultHint(this IArgument argument)
        {
            if (!argument.HasDefaultValue) { return null; }
            if (argument.GetDefaultValue() is null) { return null; }
            if (argument.ValueType == typeof(bool) || argument.ValueType == typeof(bool?))
            {
                var value = argument.GetDefaultValue();
                if (value is null || !(bool)value) { return null; }
            }
            return (argument.GetDefaultValue()?.ToString() ?? "null");
        }
    }
}
