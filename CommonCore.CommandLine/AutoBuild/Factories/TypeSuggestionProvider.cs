﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine.Suggestions;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Factories
{
    public class TypeSuggestionProvider : AServiceProvider<ISuggestionSource>
    {
        public static TypeSuggestionProvider Instance
        {
            get => _instance ??= new TypeSuggestionProvider();
            set
            {
                if (_instance != null) { throw new InvalidOperationException($"{nameof(TypeConversionProvider)}.{nameof(Instance)} singleton cannot be set because it is already initialized."); }
                _instance = value ?? throw new InvalidOperationException($"{nameof(TypeConversionProvider)}.{nameof(Instance)} singleton cannot be set to 'null'.");
            }
        }
        private static TypeSuggestionProvider _instance = null;

        public static IDictionary<Type, ISuggestionSource> GetSuggestionSourceDict()
        {
            //ConcurrentDictionary<Type, ISuggestionSource> SuggestionSource._suggestionSourcesByType
            var suggestionSourceType = typeof(SuggestionSource);
            return ReflectionHelper.GetFieldValue<IDictionary<Type, ISuggestionSource>>(suggestionSourceType, "_suggestionSourcesByType");
        }

        public TypeSuggestionProvider() : base(GetSuggestionSourceDict()) { }
    }
}
