﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine.Parsing;
using System.Reflection;
using CommonCore.CommandLine.Exceptions;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="ValidatorReferenceAttribute"/> <c>class</c>
    /// provides support for command validators.
    /// 
    /// <para>
    /// Validators allow argument and option values to be analyzed after parsing,
    /// but before the command method is invoked. With them, values can be rejected
    /// and an error message provided to the user explaining the problem. Validators
    /// do not allow changing of the value, only implicitly accepting it by not
    /// returning an error message or explicitly rejecting it by providing a message.
    /// Multiple validator references can be declared and they will be called in
    /// declaration order.
    /// </para>
    /// 
    /// <para>
    /// The attribute takes a type and a method name that points to the method
    /// representing the initializer. If the type is not specified, then the declaring
    /// type of the marked method will be used.
    /// </para>
    /// 
    /// <para>
    /// Middleware methods must be <c>static</c>, have a return type of <see cref="string"/>,
    /// and accept exactly one parameter of type <see cref="CommandResult"/>.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidatorReferenceAttribute : Attribute
    {
        public ValidatorReferenceAttribute(string methodName)
            : this(null, methodName) { }

        public ValidatorReferenceAttribute(Type declaringType, string methodName)
        {
            Thrower.If.IsEmpty(methodName, nameof(methodName)).Throw();

            this.DeclaringType = declaringType;
            this.MethodName = methodName;
        }

        /// <summary>
        /// Gets the type containing <see cref="MethodName"/> or <c>null</c>.
        /// </summary>
        public Type DeclaringType { get; }

        /// <summary>
        /// Gets the name of the referenced method.
        /// </summary>
        public string MethodName { get; }

        /// <summary>
        /// Gets the <see cref="MethodInfo"/> from the <see cref="DeclaringType"/> and <see cref="MethodName"/>
        /// members. If <see cref="DeclaringType"/> is <c>null</c>, then <paramref name="fallbackType"/> is
        /// used instead; if both are null, then an exception is thrown.
        /// </summary>
        /// <param name="fallbackType">The <see cref="Type"/> to use if <see cref="DeclaringType"/> is <c>null</c>.</param>
        /// <returns>The <see cref="MethodInfo"/> represented by the attribute instance.</returns>
        /// <exception cref="ArgumentNullException">If <see cref="DeclaringType"/> and <paramref name="fallbackType"/> are both <c>null</c>.</exception>
        public MethodInfo GetMethod(Type fallbackType)
        {
            var type = this.DeclaringType ?? fallbackType ?? throw new ArgumentNullException(nameof(fallbackType));
            var name = this.MethodName;
            var method = ReflectionHelper.GetMethodInfo(type, name);

            if (method is null)
            { throw new MethodAccessException($"Validator reference method '{name}' on type '{type}' could not be found."); }

            if (!method.IsStatic)
            { throw new MethodAccessException($"Validator reference method '{name}' on type '{type}' MUST be static."); }

            if (method.ReturnType != typeof(string))
            { throw new MethodAccessException($"Validator reference method '{name}' on type '{type}' MUST have a return type of '{typeof(string)}'."); }

            var parameters = method.GetParameters();
            if (parameters.Length != 1 || parameters[0].ParameterType != typeof(CommandResult))
            { throw new MethodAccessException($"Validator reference method '{name}' on type '{type}' MUST accept only one parameter of type {typeof(CommandResult)}."); }

            return method;
        }
    }
}
