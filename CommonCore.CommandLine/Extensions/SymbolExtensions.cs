﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.CommandLine;
using System.Linq;

namespace CommonCore.CommandLine.Extensions
{
    public static class SymbolExtensions
    {
        public static IEnumerable<TSymbol> GetAncestry<TSymbol>(this ISymbol symbol, bool includeSelf = true)
            where TSymbol : ISymbol
        {
            while (symbol != null)
            {
                if (includeSelf) { yield return (TSymbol)symbol; }
                symbol = symbol.Parents.OfType<TSymbol>().FirstOrDefault();
            }
        }

        public static string ToSummary(this ISymbol symbol)
        {
            var parts = symbol.ToDescription()?.Split("\n");
            if (parts is null) { return null; }

            int first = -1;
            int last = -1;
            for (int index = 0; index < parts.Length; index++)
            {
                if (first < 0)
                {
                    if (string.IsNullOrWhiteSpace(parts[index])) { continue; }
                    first = index;
                    continue;
                }

                if (!string.IsNullOrWhiteSpace(parts[index])) { continue; }
                last = index;
                break;
            }
            if (first < 0) { return null; }
            if (last < 0) { last = parts.Length; }
            return string.Join(' ', parts, first, last - first);
        }

        public static string ToExplanation(this ISymbol symbol)
        {
            var parts = symbol.ToDescription()?.Split("\n");
            if (parts is null) { return null; }

            int first = -1;
            int last = -1;
            int stage = 0;
            for (int index = 0; index < parts.Length; index++)
            {
                if (stage == 0)
                {
                    if (string.IsNullOrWhiteSpace(parts[index])) { continue; }
                    stage = 1;
                }

                if (stage == 1)
                {
                    if (!string.IsNullOrWhiteSpace(parts[index])) { continue; }
                    stage = 2;
                }

                if (first < 0)
                {
                    if (string.IsNullOrWhiteSpace(parts[index])) { continue; }
                    first = index;
                    continue;
                }

                if (!string.IsNullOrWhiteSpace(parts[index])) { continue; }
                last = index;
                break;
            }
            if (first < 0) { return null; }
            if (last < 0) { last = parts.Length; }
            return string.Join(' ', parts, first, last - first);
        }

        public static string ToDescription(this ISymbol symbol) => string.IsNullOrWhiteSpace(symbol.Description) ? null : symbol.Description;
    }
}
