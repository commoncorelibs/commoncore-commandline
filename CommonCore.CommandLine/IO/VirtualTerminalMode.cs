﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Runtime.InteropServices;

namespace CommonCore.CommandLine.IO
{
    public sealed class VirtualTerminalMode : IDisposable
    {
        private const uint ENABLE_VIRTUAL_TERMINAL_PROCESSING = 0x0004;

        private const uint ENABLE_VIRTUAL_TERMINAL_INPUT = 0x0200;

        private const uint DISABLE_NEWLINE_AUTO_RETURN = 0x0008;

        private const int STD_OUTPUT_HANDLE = -11;

        private const int STD_INPUT_HANDLE = -10;

        [DllImport("kernel32.dll")]
        private static extern bool GetConsoleMode(IntPtr handle, out uint mode);

        [DllImport("kernel32.dll")]
        private static extern uint GetLastError();

        [DllImport("kernel32.dll")]
        private static extern bool SetConsoleMode(IntPtr handle, uint mode);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetStdHandle(int handle);

        private readonly IntPtr _stdOutHandle;
        private readonly IntPtr _stdInHandle;
        private readonly uint _originalOutputMode;
        private readonly uint _originalInputMode;

        private VirtualTerminalMode(bool isEnabled, uint? error = null)
        {
            this.IsEnabled = isEnabled;
            this.Error = error;
        }

        private VirtualTerminalMode(IntPtr stdOutHandle, uint originalOutputMode, IntPtr stdInHandle, uint originalInputMode)
        {
            this.IsEnabled = true;

            this._stdOutHandle = stdOutHandle;
            this._originalOutputMode = originalOutputMode;
            this._stdInHandle = stdInHandle;
            this._originalInputMode = originalInputMode;
        }

        public uint? Error { get; }

        public bool IsEnabled { get; }

        public static VirtualTerminalMode TryEnable()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                var stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
                var stdInHandle = GetStdHandle(STD_INPUT_HANDLE);

                if (!GetConsoleMode(stdOutHandle, out var originalOutputMode))
                {
                    return new VirtualTerminalMode(false, GetLastError());
                }

                if (!GetConsoleMode(stdInHandle, out var originalInputMode))
                {
                    return new VirtualTerminalMode(false, GetLastError());
                }

                // var requestedInputMode = originalInputMode |
                //                          ENABLE_VIRTUAL_TERMINAL_INPUT;

                var requestedOutputMode = originalOutputMode |
                                          ENABLE_VIRTUAL_TERMINAL_PROCESSING |
                                          DISABLE_NEWLINE_AUTO_RETURN;

                if (!SetConsoleMode(stdOutHandle, requestedOutputMode))
                {
                    return new VirtualTerminalMode(false, GetLastError());
                }

                // if (!SetConsoleMode(stdInHandle, requestedInputMode))
                // {
                //     return new VirtualTerminalMode(false, GetLastError());
                // }

                return new VirtualTerminalMode(stdOutHandle, originalOutputMode, stdInHandle, originalInputMode);
            }

            var terminalName = Environment.GetEnvironmentVariable("TERM");

            var isXterm = !string.IsNullOrEmpty(terminalName)
                          && terminalName.StartsWith("xterm", StringComparison.OrdinalIgnoreCase);

            // TODO: Is this a reasonable default?
            return new VirtualTerminalMode(isXterm);
        }

        private void RestoreConsoleMode()
        {
            if (this.IsEnabled)
            {
                if (this._stdOutHandle != IntPtr.Zero)
                {
                    SetConsoleMode(this._stdOutHandle, this._originalOutputMode);
                }

                // if (_stdInHandle != IntPtr.Zero)
                // {
                //    SetConsoleMode(_stdInHandle, _originalInputMode);
                // }
            }
        }

        // TODO: Properly implement the dispose pattern.
        public void Dispose()
        {
            this.RestoreConsoleMode();
            GC.SuppressFinalize(this);
        }

        ~VirtualTerminalMode()
        {
            this.RestoreConsoleMode();
        }
    }
}
