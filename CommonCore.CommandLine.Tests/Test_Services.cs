﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Binding;
using System.Diagnostics.CodeAnalysis;
using CommonCore.CommandLine.IO;
using CommonCore.CommandLine.Reflection;
using FluentAssertions;
using Xunit;

namespace CommonCore.CommandLine.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_Services
    {
        [Fact]
        public void Instance()
        {
            Services.Instance.Should().NotBeNull();
            var instance = Services.Instance;
            Services.Instance = null;
            Services.Instance.Should().BeNull();
            Services.Instance = instance;
            Services.Instance.Should().NotBeNull();
        }

        [Fact]
        public void Constructor_Default()
        {
            Action act;
            
            act = () => new Services();
            act.Should().NotThrow();

            var services = new Services();

            services.Count.Should().Be(1);
            services.Keys.Should().HaveCount(1);

            services[0].Should().NotBeNull().And.BeOfType<Services>();
        }

        [Fact]
        public void Constructor_Standard()
        {
            Action act;
            var result = new Command("root").Parse(Array.Empty<string>());

            act = () => new Services(null);
            act.Should().Throw<ArgumentNullException>();

            var binding = new BindingContext(result, new StringConsole());

            act = () => new Services(binding);
            act.Should().NotThrow();

            var services = new Services(binding);

            services.Count.Should().BeGreaterThan(0);
            services.Keys.Should().HaveCountGreaterThan(0);

            for (int index = 0; index < services.Count; index++)
            { services[index].Should().NotBeNull(); }

            // Test that any services already present in a binding context
            // are accounted for in the omitted types list. If the test
            // fails, then those new types need to be added to the list
            // so that auto generation can ignore them when looking for
            // arguments and options.
            foreach (Type key in services)
            { CCUtils.OmittedTypes.Should().Contain(key); }

            binding = new BindingContext(result, new StringConsole());
            var provider = ReflectionHelper.GetPropertyValue<IServiceProvider>(binding, "ServiceProvider");
            ReflectionHelper.SetFieldValue(provider, "_services", null);

            act = () => new Services(binding);
            act.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void Basics()
        {
            {
                var result = new Command("root").Parse(Array.Empty<string>());
                var binding = new BindingContext(result, new StringConsole());
                var services = new Services(binding);

                ((IEnumerable<Type>)services).Should().BeEquivalentTo(services.Keys);
                ((IEnumerable)services).Should().BeEquivalentTo(services.Keys);
            }


            string service = "ok";

            {
                var services = new Services();

                services.Set(service);
                services.Get<string>().Should().BeOfType<string>().And.Be(service);
                services.Remove<string>();
                services.Get<string>().Should().BeNull();
            }

            {
                var services = new Services();

                services.Set<string>((object)service);
                services.Get<string>().Should().BeOfType<string>().And.Be(service);
                services.Remove<string>();
                services.Get<string>().Should().BeNull();
            }

            {
                var services = new Services();

                services.Set(service.GetType(), service);
                services.Get(service.GetType()).Should().BeOfType<string>().And.Be(service);
                ((IServiceProvider)services).GetService(service.GetType()).Should().BeOfType<string>().And.Be(service);
                services.Remove(service.GetType());
                services.Get(service.GetType()).Should().BeNull();
                ((IServiceProvider)services).GetService(service.GetType()).Should().BeNull();
            }

            {
                var services = new Services();

                services.Set<string>(service);
                services.Get<string>().Should().BeOfType<string>().And.Be(service);
                services.Remove<string>();
                services.Get<string>().Should().BeNull();
            }

            {
                var services = new Services();

                services.Set(service.GetType(), provider => service);
                services.Get(service.GetType()).Should().BeOfType<string>().And.Be(service);
                ((IServiceProvider)services).GetService(service.GetType()).Should().BeOfType<string>().And.Be(service);
                services.Remove(service.GetType());
                services.Get(service.GetType()).Should().BeNull();
                ((IServiceProvider)services).GetService(service.GetType()).Should().BeNull();
            }

            {
                var services = new Services();

                services.Set<string>(provider => service);
                services.Get<string>().Should().BeOfType<string>().And.Be(service);
                services.Remove<string>();
                services.Get<string>().Should().BeNull();
            }

            {
                var services = new Services();
                var type = typeof(string);

                {
                    services[type].Should().BeNull();
                    services.ContainsKey(type).Should().BeFalse();
                    services.ContainsKey<string>().Should().BeFalse();
                    services.TryGetValue(type, out object obj).Should().BeFalse();
                    obj.Should().Be(default);
                    services.TryGetValue<string>(out string str).Should().BeFalse();
                    str.Should().Be(default);
                }

                services.Set(service);

                {
                    services[type].Should().Be(service);
                    services.ContainsKey(type).Should().BeTrue();
                    services.ContainsKey<string>().Should().BeTrue();
                    services.TryGetValue(type, out object obj).Should().BeTrue();
                    obj.Should().Be(service);
                    services.TryGetValue<string>(out string str).Should().BeTrue();
                    str.Should().Be(service);
                }
            }
        }

        [Fact]
        public void CopyTo()
        {
            var result = new Command("root").Parse(Array.Empty<string>());
            var binding = new BindingContext(result, new StringConsole());
            var bindingServices = new Services(binding);
            var services = new Services();

            bindingServices.Count.Should().BeGreaterThan(0);
            services.Count.Should().Be(1);

            bindingServices.CopyTo(services);
            services.Count.Should().Be(1 + bindingServices.Count);
        }

        [Fact]
        public void GetServices()
        {
            var result = new Command("root").Parse(Array.Empty<string>());
            var binding = new BindingContext(result, new StringConsole());

            binding.GetServices().Should().NotBeNull();
            binding.GetServices().Should().NotBeNull();
        }
    }
}
