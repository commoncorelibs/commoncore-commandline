﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerStepThrough]
    public static class AnsiClears
    {
        public static Ansi EntireScreen { get; } = Ansi.From('J', 2);

        public static Ansi Line { get; } = Ansi.From('K', 2);

        public static Ansi ToBeginningOfLine { get; } = Ansi.From('K', 1);

        public static Ansi ToBeginningOfScreen { get; } = Ansi.From('J', 1);

        public static Ansi ToEndOfLine { get; } = Ansi.From('K');

        public static Ansi ToEndOfScreen { get; } = Ansi.From('J');
    }
}
