﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerDisplay("{Open.Code}, {Close.Code}")]
    public readonly struct AnsiPair : IEquatable<AnsiPair>
    {
        public static AnsiPair None { get; } = default;

        public static AnsiPair From(char suffix, int argOpen, int argClose)
            => new AnsiPair(Ansi.From(suffix, argOpen), Ansi.From(suffix, argClose));

        public static AnsiPair From(char suffixOpen, int argOpen, char suffixClose, int argClose)
            => new AnsiPair(Ansi.From(suffixOpen, argOpen), Ansi.From(suffixClose, argClose));

        public static AnsiPair FromPrivate(char suffixOpen, int argOpen, char suffixClose, int argClose)
            => new AnsiPair(Ansi.FromPrivate(suffixOpen, argOpen), Ansi.FromPrivate(suffixClose, argClose));

        public AnsiPair(Ansi open, Ansi close)
        {
            this.Open = open;
            this.Close = close;
        }

        public readonly Ansi Open { get; }

        public readonly Ansi Close { get; }

        public readonly override string ToString()
            => $"[{this.Open.Code}, {this.Close.Code}]";

        public readonly override int GetHashCode()
            => HashCode.Combine(this.Open, this.Close);

        public readonly override bool Equals(object obj)
            => obj is AnsiPair pair && this.Equals(pair);

        public readonly bool Equals(AnsiPair other)
            => this.Open.Equals(other.Open) && this.Close.Equals(other.Close);

        public static bool operator ==(AnsiPair left, AnsiPair right) => left.Equals(right);

        public static bool operator !=(AnsiPair left, AnsiPair right) => !left.Equals(right);
    }
}
