﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="AppAllRightsReservedLicense"/> <c>convienence</c> <c>class</c> provides an
    /// implementation of <see cref="AppLicenseAttribute"/> populated with data for
    /// projects with no license.
    /// 
    /// <para>
    /// This is the default used if the implementing console application is not decorated with
    /// any other <see cref="AppLicenseAttribute"/> attribute.
    /// </para>
    /// 
    /// <para>
    /// The attribute should be applied to the implementing console application's main <c>class</c>,
    /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> <c>method</c>.
    /// This is generally the <c>Program</c> class.
    /// </para>
    /// </summary>
    public class AppAllRightsReservedLicense : AppLicenseAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppAllRightsReservedLicense"/> <c>class</c>
        /// populated with information related to the implementing console application.
        /// </summary>
        public AppAllRightsReservedLicense()
        {
            this.LicenseSummary = "All Rights Reserved";
        }
    }
}
