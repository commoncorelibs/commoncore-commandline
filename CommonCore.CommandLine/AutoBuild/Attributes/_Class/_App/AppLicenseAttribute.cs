﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.IO;
using System.Text;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="AppLicenseAttribute"/> <c>class</c> is used to provide the system
    /// with license information about the implementing console application.
    /// 
    /// <para>
    /// The attribute should be applied to the implementing console application's main <c>class</c>,
    /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> <c>method</c>.
    /// This is generally the <c>Program</c> class.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class AppLicenseAttribute : Attribute
    {
        /// <summary>
        /// Gets the name of the application license. Example: The MIT License
        /// </summary>
        public string LicenseName { get; set; }

        /// <summary>
        /// Gets the id of the application license. Example: MIT
        /// </summary>
        public string LicenseId { get; set; }

        /// <summary>
        /// Gets the url of the application license. Example: https://opensource.org/licenses/MIT
        /// </summary>
        public string LicenseUrl { get; set; }

        /// <summary>
        /// Gets the file path of a local copy of the application license.
        /// </summary>
        public string LicenseFile { get; set; }

        /// <summary>
        /// Gets the summary text of the application license or the
        /// default summary text built from the <see cref="LicenseName"/>,
        /// <see cref="LicenseId"/>, and <see cref="LicenseUrl"/> values.
        /// See <see cref="BuildDefaultLicenseSummary()"/> for more details.
        /// </summary>
        public string LicenseSummary
        {
            get => this._licenseSummary ??= this.BuildDefaultLicenseSummary();
            set => this._licenseSummary = value;
        }
        private string _licenseSummary = null;

        /// <summary>
        /// Gets the full text of the application license or an attempt will be
        /// made to load the license text from file.
        /// The use of four substitution placeholders is supported:
        /// <c>{Company}</c>, <c>{Title}</c>, <c>{Version}</c>, and <c>{Copyright}</c>.
        /// These placeholders will then be replaced by appropriate <see cref="AppInfo"/>
        /// values when the license text is displayed.
        /// See <see cref="BuildDefaultLicenseText()"/> for more details.
        /// </summary>
        public string LicenseText
        {
            get => this._licenseText ??= this.BuildDefaultLicenseText();
            set => this._licenseText = value;
        }
        private string _licenseText = null;

        /// <summary>
        /// Builds the default license summary text from the <see cref="LicenseName"/>,
        /// <see cref="LicenseId"/>, and <see cref="LicenseUrl"/> values.
        /// The desired format is <c>name (id): url</c>, but formatting will adapt to
        /// missing values with the order: name and url, id and url, name and id, only
        /// name, only id, only url, and finally if no values have been found the
        /// literal string <c>All Rights Reserved</c> will be returned.
        /// </summary>
        /// <returns>The default license summary string.</returns>
        public string BuildDefaultLicenseSummary()
        {
            bool hasName = !string.IsNullOrEmpty(this.LicenseName);
            bool hasId = !string.IsNullOrEmpty(this.LicenseId);
            bool hasUrl = !string.IsNullOrEmpty(this.LicenseUrl);
            if (hasName && hasId && hasUrl) { return $"{this.LicenseName} ({this.LicenseId}): {this.LicenseUrl}"; }
            else if (hasName && hasUrl) { return $"{this.LicenseName}: {this.LicenseUrl}"; }
            else if (hasId && hasUrl) { return $"{this.LicenseId}: {this.LicenseUrl}"; }
            else if (hasName && hasId) { return $"{this.LicenseName} ({this.LicenseId})"; }
            else if (hasName) { return this.LicenseName; }
            else if (hasId) { return this.LicenseId; }
            else if (hasUrl) { return this.LicenseUrl; }
            else { return "All Rights Reserved"; }
        }

        /// <summary>
        /// Builds the default license text by attempting to load various files.
        /// First, if <see cref="LicenseFile"/> is set, then the text will be loaded from that file.
        /// Note that no error or exception will be caused if the file path does not exist,
        /// instead an <c>empty</c> string will be returned.
        /// Otherwise, if <see cref="LicenseFile"/> is not set, then the files <c>LICENSE</c>,
        /// <c>LICENSE.md</c>, and <c>LICENSE.txt</c> will be loaded in that order, returning the
        /// text of the first one found. 
        /// If none of the files were found or loaded, then an <c>empty</c> string is returned.
        /// </summary>
        /// <returns>The default license text string or <c>empty</c> string.</returns>
        public string BuildDefaultLicenseText()
        {
            string text = null;
            if (!string.IsNullOrEmpty(this.LicenseFile))
            { try { text = File.ReadAllText(this.LicenseFile); } catch (FileNotFoundException) { } }
            else if (text is null) { try { text = File.ReadAllText("./LICENSE"); } catch (FileNotFoundException) { } }
            else if (text is null) { try { text = File.ReadAllText("./LICENSE.md"); } catch (FileNotFoundException) { } }
            else if (text is null) { try { text = File.ReadAllText("./LICENSE.txt"); } catch (FileNotFoundException) { } }
            return text ?? string.Empty;
        }

        /// <summary>
        /// Substitutes appropriate values from the specified <paramref name="appInfo"/>
        /// for placeholders in the specified <paramref name="licenseText"/>.
        /// The supported placeholders are:
        /// <c>{Company}</c>, <c>{Title}</c>, <c>{Version}</c>, and <c>{Copyright}</c>.
        /// </summary>
        /// <param name="licenseText"></param>
        /// <param name="appInfo"></param>
        /// <returns></returns>
        public static string ApplyLicenseTextSubstitutions(string licenseText, AppInfo appInfo)
        {
            if (string.IsNullOrEmpty(licenseText)) { return string.Empty; }

            var builder = new StringBuilder(licenseText);
            builder.Replace("{Company}", appInfo.Company);
            builder.Replace("{Title}", appInfo.Title);
            builder.Replace("{Version}", appInfo.Version);
            builder.Replace("{Copyright}", appInfo.Copyright);
            return builder.ToString();
        }
    }
}
