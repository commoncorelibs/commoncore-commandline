﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

namespace CommonCore.CommandLine.Rendering.Views.Spans
{
    public class StyleSpan : ControlSpan
    {
        public StyleSpan(string name, Ansi ansiControlCode)
            : base(name, ansiControlCode) { }

        public static StyleSpan AttributesOff() => new StyleSpan(nameof(AttributesOff), AnsiStyles.Reset);

        public static StyleSpan BrightOn() => new StyleSpan(nameof(BrightOn), AnsiStyles.Bright.Open);
        public static StyleSpan BrightOff() => new StyleSpan(nameof(BrightOff), AnsiStyles.Bright.Close);

        public static StyleSpan FaintOn() => new StyleSpan(nameof(FaintOn), AnsiStyles.Faint.Open);
        public static StyleSpan FaintOff() => new StyleSpan(nameof(FaintOff), AnsiStyles.Faint.Close);

        //public static StyleSpan ItalicOn() => new StyleSpan(nameof(ItalicOn), AnsiStyles.Italic.Open);
        //public static StyleSpan ItalicOff() => new StyleSpan(nameof(ItalicOff), AnsiStyles.Italic.Close);

        public static StyleSpan UnderlineOn() => new StyleSpan(nameof(UnderlineOn), AnsiStyles.Underline.Open);
        public static StyleSpan UnderlineOff() => new StyleSpan(nameof(UnderlineOff), AnsiStyles.Underline.Close);

        //public static StyleSpan SlowBlinkOn() => new StyleSpan(nameof(SlowBlinkOn), AnsiStyles.SlowBlink.Close);
        //public static StyleSpan SlowBlinkOff() => new StyleSpan(nameof(SlowBlinkOff), AnsiStyles.SlowBlink.Open);

        //public static StyleSpan RapidBlinkOn() => new StyleSpan(nameof(RapidBlinkOn), AnsiStyles.RapidBlink.Open);
        //public static StyleSpan RapidBlinkOff() => new StyleSpan(nameof(RapidBlinkOff), AnsiStyles.RapidBlink.Close);

        public static StyleSpan ReverseOn() => new StyleSpan(nameof(ReverseOn), AnsiStyles.Reverse.Open);
        public static StyleSpan ReverseOff() => new StyleSpan(nameof(ReverseOff), AnsiStyles.Reverse.Close);

        //public static StyleSpan HiddenOn() => new StyleSpan(nameof(HiddenOn), AnsiStyles.Hidden.Open);
        //public static StyleSpan HiddenOff() => new StyleSpan(nameof(HiddenOff), AnsiStyles.Hidden.Close);

        //public static StyleSpan StrikeOn() => new StyleSpan(nameof(StrikeOn), AnsiStyles.Strike.Open);
        //public static StyleSpan StrikeOff() => new StyleSpan(nameof(StrikeOff), AnsiStyles.Strike.Close);
    }
}
