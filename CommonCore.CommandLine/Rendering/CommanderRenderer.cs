﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine;
using CommonCore.CommandLine.Extensions;
using CommonCore.CommandLine.IO;
using CommonCore.CommandLine.Rendering.Views.Spans;
using CommonCore.CommandLine.Rendering.Visitors;

namespace CommonCore.CommandLine.Rendering
{
    public class CommanderRenderer : IConsoleRenderer
    {
        public CommanderRenderer(ERenderMode mode = ERenderMode.Auto, int outputWidth = 100, bool resetAfterRender = false)
        {
            this.Console = Services.Instance.Get<IConsole>();
            this.Region = new Region(0, 0, outputWidth, int.MaxValue, false);
            this.RenderMode = mode;

            this.Terminal = this.Console as ITerminal;
            this.ResetAfterRender = resetAfterRender;
        }

        public IConsole Console { get; }

        public Region Region { get; }

        public ERenderMode RenderMode { get; protected set; }

        public TextSpanFormatter Formatter { get; protected set; } = new TextSpanFormatter();

        public bool ResetAfterRender { get; }

        public ITerminal Terminal { get; }

        public virtual void Render(ISpan span, Region region = default)
        {
            span ??= ContentSpan.Empty();
            if (region == default) { region = this.Region; }
            else if (region.Width > this.Region.Width)
            { region = region.Clone(this.Region.Width, region.Height); }

            if (this.ResetAfterRender)
            { span = new ContainerSpan(span, ForeColorSpan.Reset(), BackColorSpan.Reset()); }

            if (this.RenderMode == ERenderMode.Auto)
            { this.RenderMode = this.Terminal?.DetectOutputMode() ?? ERenderMode.PlainText; }

            ASpanVisitor visitor = this.RenderMode switch
            {
                ERenderMode.Terminal => new TerminalSpanRenderer(this.Terminal, region.Clone(false)),
                ERenderMode.Ansi => new AnsiTextSpanRenderer(this.Console, region.Clone(false)),
                _ => new TextSpanRenderer(this.Console.Out, region.Clone(false))
            };
            visitor.Visit(span);
        }

        public virtual Size Measure(ISpan span, Size maxSize)
            => MeasureSpan(span, maxSize);

        public static Size MeasureSpan(ISpan span, Size maxSize)
        {
            var measuringVisitor = new SpanMeasuringRenderer(new Region(0, 0, maxSize.Width, maxSize.Height, false));
            measuringVisitor.Visit(span);
            return new Size(measuringVisitor.Width, measuringVisitor.Height);
        }
    }
}
