﻿using System;
using FluentAssertions;
using Xunit;
using CommonCore.CommandLine.Extensions;
using System.Linq;

namespace CommonCore.CommandLine.Tests
{
    public class Test_MiscExtensions
    {
        [Theory]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("a", false)]
        [InlineData(" ", false)]
        [InlineData("\t", false)]
        [InlineData("\n", true)]
        [InlineData("\r\n", true)]
        public void IsNewLine(string subject, bool expected)
        {
            subject.AsSpan().IsNewLine().Should().Be(expected);
            subject.AsMemory().IsNewLine().Should().Be(expected);
        }

        [Theory]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("a", false)]
        [InlineData(" ", true)]
        [InlineData("a ", true)]
        [InlineData("a\t", true)]
        [InlineData("a\n", true)]
        [InlineData("a\r\n", true)]
        public void EndsWithWhitespace(string subject, bool expected)
            => subject.AsSpan().EndsWithWhitespace().Should().Be(expected);

        [Theory]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("a", false)]
        [InlineData(" ", true)]
        [InlineData(" a", true)]
        [InlineData("\ta", true)]
        [InlineData("\na", true)]
        [InlineData("\r\na", true)]
        public void StartsWithWhitespace(string subject, bool expected)
            => subject.AsSpan().StartsWithWhitespace().Should().Be(expected);

        [Theory]
        [InlineData("a", EWordType.Char)]
        [InlineData(" ", EWordType.Whitespace)]
        [InlineData("\t", EWordType.Whitespace)]
        [InlineData("\r", EWordType.Whitespace)]
        [InlineData("\ra", EWordType.Whitespace)]
        [InlineData("\n", EWordType.SlashN)]
        [InlineData("\r\n", EWordType.SlashRN)]
        public void GetWordType(string subject, EWordType expected)
            => subject.AsSpan().GetWordType(0).Should().Be(expected);

        [Fact]
        public void SplitForWidth()
        {
            var subject = "0123456789";
            var expected = new[] { "012", "345", "678", "9" };
            subject.AsMemory().Yield().SplitForWidth(3).Select(m => m.ToString()).Should().BeEquivalentTo(expected);

            expected = new[] { subject };
            subject.AsMemory().Yield().SplitForWidth(0).Select(m => m.ToString()).Should().BeEquivalentTo(expected);
            subject.AsMemory().Yield().SplitForWidth(10).Select(m => m.ToString()).Should().BeEquivalentTo(expected);

            subject = "\r\n";
            expected = new[] { subject };
            subject.AsMemory().Yield().SplitForWidth(1).Select(m => m.ToString()).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void SplitForWrapping()
        {
            var subject = " \t 0\n123\r\n45 6\t7 89";
            var expected = new[] { " \t ", "0", "\n", "123", "\r\n", "45", " ", "6", "\t", "7", " ", "89" };
            subject.AsMemory().SplitForWrapping().Select(m => m.ToString()).Should().BeEquivalentTo(expected);
        }

        [Theory]
        [InlineData("0123456789", -1, "0123456789")]
        [InlineData("0123456789", 0, "")]
        [InlineData("0123456789", 10, "0123456789")]
        [InlineData("0123456789 ", 10, "0123456789")]
        [InlineData("0123456789", 5, "01234")]
        public void Truncate(string subject, int maxWidth, string expected)
            => subject.AsSpan().Truncate(maxWidth).ToString().Should().Be(expected);
    }
}
