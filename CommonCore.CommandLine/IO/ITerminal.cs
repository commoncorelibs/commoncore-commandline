﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine;

namespace CommonCore.CommandLine.IO
{
    public interface ITerminal : IConsole
    {
        ConsoleColor BackgroundColor { get; set; }

        ConsoleColor ForegroundColor { get; set; }

        void ResetColor();

        void Clear();

        //int CursorLeft { get; set; }

        //int CursorTop { get; set; }

        //void SetCursorPosition(int left, int top);

        void HideCursor();

        void ShowCursor();
    }
}
