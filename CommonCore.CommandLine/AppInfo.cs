﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Reflection;
using System.Text;
using CommonCore.CommandLine.AutoBuild.Attributes;

namespace CommonCore.CommandLine
{
    /// <summary>
    /// The <see cref="AppInfo"/> <c>class</c> provides information related
    /// to the implementing console application.
    /// </summary>
    public class AppInfo
    {
        /// <summary>
        /// Gets the <c>singleton</c> <c>instance</c> of the <see cref="AppInfo"/> <c>class</c>.
        /// </summary>
        public static AppInfo Instance
        {
            get => _instance;
            set
            {
                if (_instance != null) { throw new InvalidOperationException($"{nameof(AppInfo)}.{nameof(Instance)} singleton cannot be set because it is already initialized."); }
                _instance = value ?? throw new InvalidOperationException($"{nameof(AppInfo)}.{nameof(Instance)} singleton cannot be set to 'null'.");
                Services.Instance.Set(AppInfo.Instance);
            }
        }
        /// <summary>
        /// Backing <c>field</c> for the <see cref="Instance"/> <c>property</c>.
        /// </summary>
        private static AppInfo _instance = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppInfo"/> <c>class</c>
        /// populated with information related to the implementing console application.
        /// </summary>
        /// <param name="type">
        /// The <see cref="Type"/> of the implementing console application's main <c>class</c>,
        /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> marked <c>method</c>.
        /// This is generally the <c>Program</c> class.
        /// </param>
        public AppInfo(Type type)
            : this(type.GetCustomAttribute<AppTitleAttribute>(),
                  type.GetCustomAttribute<AppCopyrightAttribute>(),
                  type.GetCustomAttribute<AppLicenseAttribute>()) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppInfo"/> <c>class</c>
        /// populated with information related to the implementing console application
        /// and the specified attribute arguments.
        /// </summary>
        /// <param name="titleAttribute"></param>
        /// <param name="copyrightAttribute"></param>
        /// <param name="licenseAttribute"></param>
        public AppInfo(AppTitleAttribute titleAttribute, AppCopyrightAttribute copyrightAttribute, AppLicenseAttribute licenseAttribute)
        {
            var title = titleAttribute ?? new AppTitleAttribute();
            this.DebugMode = title.IsDebug();
            this.Company = title.Company;
            this.Title = title.Title;
            this.Version = title.Version;

            var copyright = copyrightAttribute ?? new AppCopyrightAttribute();
            this.Copyright = copyright.Copyright;
            this.CopyrightHolder = copyright.CopyrightHolder;
            this.CopyrightYear = copyright.CopyrightYear;

            var license = licenseAttribute ?? new AppAllRightsReservedLicense();
            this.LicenseName = license.LicenseName;
            this.LicenseId = license.LicenseId;
            this.LicenseUrl = license.LicenseUrl;
            this.LicenseFile = license.LicenseFile;
            this.LicenseSummary = license.LicenseSummary;
            this.LicenseText = AppLicenseAttribute.ApplyLicenseTextSubstitutions(license.LicenseText, this);
        }

        /// <summary>
        /// Gets <c>true</c> if the application assembly was compiled in debug mode;
        /// otherwise <c>false</c>.
        /// </summary>
        public bool DebugMode { get; set; }

        /// <summary>
        /// Gets the title of the application.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets the version of the application.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Gets the company name of the application.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Gets the copyright notice of the application.
        /// </summary>
        public string Copyright { get; set; }

        /// <summary>
        /// Gets the holder name of the application copyright.
        /// </summary>
        public string CopyrightHolder { get; set; }

        /// <summary>
        /// Gets the year of the application copyright.
        /// </summary>
        public string CopyrightYear { get; set; }

        /// <summary>
        /// Gets the name of the application license. Example: The MIT License
        /// </summary>
        public string LicenseName { get; set; }

        /// <summary>
        /// Gets the id of the application license. Example: MIT
        /// </summary>
        public string LicenseId { get; set; }

        /// <summary>
        /// Gets the url of the application license. Example: https://opensource.org/licenses/MIT
        /// </summary>
        public string LicenseUrl { get; set; }

        /// <summary>
        /// Gets the file path of a local copy of the application license.
        /// </summary>
        public string LicenseFile { get; set; }

        /// <summary>
        /// Gets the summary text of the application license.
        /// </summary>
        public string LicenseSummary { get; set; }

        /// <summary>
        /// Gets the full text of the application license.
        /// </summary>
        public string LicenseText { get; set; }

        //public AppUsage[] Usages { get; set; }
    }
}
