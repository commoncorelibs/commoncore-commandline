﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.CommandLine.Rendering.Views
{
    public abstract class AView : IView
    {
        public event EventHandler Updated;

        public abstract void Render(IConsoleRenderer renderer, Region region = default);

        public abstract Size Measure(IConsoleRenderer renderer, Size maxSize);

        protected void OnUpdated() => this.Updated?.Invoke(this, EventArgs.Empty);
    }
}
