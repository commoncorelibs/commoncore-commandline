﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine.Binding;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Factories
{
    public class TypeConversionProvider : AServiceProvider<Func<string, object>>
    {
        public static TypeConversionProvider Instance
        {
            get => _instance ??= new TypeConversionProvider();
            set
            {
                if (_instance != null) { throw new InvalidOperationException($"{nameof(TypeConversionProvider)}.{nameof(Instance)} singleton cannot be set because it is already initialized."); }
                _instance = value ?? throw new InvalidOperationException($"{nameof(TypeConversionProvider)}.{nameof(Instance)} singleton cannot be set to 'null'.");
            }
        }
        private static TypeConversionProvider _instance = null;

        public static IDictionary<Type, Func<string, object>> GetArgumentConverterDict()
        {
            //Dictionary<Type, Func<string, object>> ArgumentConverter._converters
            var argumentConverterType = typeof(BindingContext).Assembly.GetType("System.CommandLine.Binding.ArgumentConverter");
            return ReflectionHelper.GetFieldValue<IDictionary<Type, Func<string, object>>>(argumentConverterType, "_converters");
        }

        public TypeConversionProvider() : base(GetArgumentConverterDict()) { }
    }
}
