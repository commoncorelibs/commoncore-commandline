﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerDisplay("{Code}")]
    public readonly struct Ansi : IEquatable<Ansi>
    {
        public static Ansi None { get; } = default;

        public static string Escape { get; } = "\x1b";

        public static Ansi From(char suffix)
            => new Ansi($"{Escape}[{suffix}");

        public static Ansi From(char suffix, int arg)
            => new Ansi($"{Escape}[{arg}{suffix}");

        public static Ansi From(char suffix, params int?[] args)
            => new Ansi($"{Escape}[{string.Join(';', args)}{suffix}");

        public static Ansi FromBracketless(char suffix)
            => new Ansi($"{Escape}{suffix}");

        public static Ansi FromPrivate(char suffix)
            => new Ansi($"{Escape}[?{suffix}");

        public static Ansi FromPrivate(char suffix, int arg)
            => new Ansi($"{Escape}[?{arg}{suffix}");

        public static Ansi FromPrivate(char suffix, params int?[] args)
            => new Ansi($"{Escape}[?{string.Join(';', args)}{suffix}");

        public Ansi(string code)
            => this.Code = code ?? throw new ArgumentNullException(nameof(code));

        public readonly string Code { get; }

        public readonly override string ToString()
            => $"[{this.Code}]";

        public readonly override int GetHashCode()
            => HashCode.Combine(this.Code);

        public readonly override bool Equals(object obj)
            => obj is Ansi ansi && this.Equals(ansi);

        public readonly bool Equals(Ansi other)
            => this.Code == other.Code;

        public static bool operator ==(Ansi left, Ansi right) => left.Equals(right);

        public static bool operator !=(Ansi left, Ansi right) => !left.Equals(right);

        public static implicit operator string(Ansi ansi) => ansi.Code;

        public static implicit operator ReadOnlySpan<char>(Ansi ansi) => ansi.Code.AsSpan();

        public static implicit operator ReadOnlyMemory<char>(Ansi ansi) => ansi.Code.AsMemory();

        //public static implicit operator Ansi(string code) => new Ansi(code);
    }
}
