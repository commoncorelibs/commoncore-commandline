﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using CommonCore.CommandLine.Invocation;

namespace CommonCore.CommandLine.Symbols
{
    public interface IMiddlewareProvider
    {
        List<Middleware> Middlewares { get; }
    }
}
