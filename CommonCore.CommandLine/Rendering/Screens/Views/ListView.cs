﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering.Views
{
    public class ListView : ListView<string, string>
    {
        public ListView(ColumnDefinition contentStyle = null)
            : this(null, item => item, contentStyle) { }

        public ListView(Func<string, string> contentValueFunc, ColumnDefinition contentStyle = null)
            : this(null, contentValueFunc, contentStyle) { }

        public ListView(IEnumerable<string> items, ColumnDefinition contentStyle = null)
            : this(items, item => item, contentStyle) { }

        public ListView(IEnumerable<string> items, Func<string, string> contentValueFunc, ColumnDefinition contentStyle = null)
            : base(items, contentValueFunc, contentStyle) { }
    }

    public class ListView<TItem> : ListView<TItem, string>
    {
        public ListView(ColumnDefinition contentStyle = null)
            : this(null, item => item.ToString(), contentStyle) { }

        public ListView(IEnumerable<TItem> items, ColumnDefinition contentStyle = null)
            : this(items, item => item.ToString(), contentStyle) { }

        public ListView(Func<TItem, string> contentValueFunc, ColumnDefinition contentStyle = null)
            : this(null, contentValueFunc, contentStyle) { }

        public ListView(IEnumerable<TItem> items, Func<TItem, string> contentValueFunc, ColumnDefinition contentStyle = null)
            : base(items, contentValueFunc, contentStyle) { }
    }

    public class ListView<TItem, TValue> : AItemsView<TItem>
    {
        public ListView(ColumnDefinition contentStyle = null)
            : this(null, null, contentStyle) { }

        public ListView(IEnumerable<TItem> items, ColumnDefinition contentStyle = null)
            : this(items, null, contentStyle) { }

        public ListView(Func<TItem, TValue> contentValueFunc, ColumnDefinition contentStyle = null)
            : this(null, contentValueFunc, contentStyle) { }

        public ListView(IEnumerable<TItem> items, Func<TItem, TValue> contentValueFunc, ColumnDefinition contentStyle = null)
        {
            this.Grid.Updated += this.OnGridUpdated;
            this.Items = items?.ToArray();
            this.ContentValueFunc = contentValueFunc;
            this.ContentStyle = contentStyle;
        }

        protected bool GridInitialized { get; set; }

        protected GridView Grid { get; set; } = new GridView();

        protected TableViewColumn<TItem, TValue> ContentColumn { get; set; }

        public ColumnDefinition ContentStyle
        {
            get => this._contentColumnDefinition;
            set
            {
                this.GridInitialized = false;
                this._contentColumnDefinition = value ?? ColumnDefinition.Auto();
            }
        }
        private ColumnDefinition _contentColumnDefinition = null;

        public Func<TItem, TValue> ContentValueFunc
        {
            get => this._contentFunc;
            set
            {
                this.GridInitialized = false;
                this._contentFunc = value;
            }
        }
        private Func<TItem, TValue> _contentFunc = null;

        public int Indenting { get => this.Grid.Indenting; set => this.Grid.Indenting = value; }

        public string Delimiter { get => this.Grid.Delimiter; set => this.Grid.Delimiter = value; }

        public string Prefix { get => this.Grid.Prefix; set => this.Grid.Prefix = value; }

        public string Suffix { get => this.Grid.Suffix; set => this.Grid.Suffix = value; }

        public EAlignment SymbolAlignment { get; set; }

        public string Symbol { get; set; }

        /// <summary>
        /// Ignored if <see cref="Symbol"/> is not <c>null</c>.
        /// </summary>
        public Func<TItem, int, string> SymbolFunc { get; set; }

        //public IReadOnlyList<ITableViewColumn<TItem>> Columns => this._columns;
        //private List<ITableViewColumn<TItem>> _columns = new List<ITableViewColumn<TItem>>();

        public bool HasSymbol()
            => (this.Symbol != null || this.SymbolFunc != null);

        public ColumnDefinition GetSymbolColumn()
            => this.Symbol != null
            ? ColumnDefinition.Fixed(this.Symbol.Length, this.SymbolAlignment)
            : this.SymbolFunc != null
            ? ColumnDefinition.Auto(this.SymbolAlignment)
            : null;

        public string GetSymbolValue(TItem item, int index)
            => this.Symbol
            ?? (this.SymbolFunc != null ? this.SymbolFunc(item, index) : null);

        public IView GetSymbolView(TItem item, int index, TextSpanFormatter formatter)
        {
            var value = this.GetSymbolValue(item, index);
            return value is null ? null : new TextView(formatter.Format(value));
        }

        protected void OnGridUpdated(object sender, EventArgs e) => this.OnUpdated();

        protected override void WhenItemsSet(IReadOnlyList<TItem> oldItems, IReadOnlyList<TItem> newItems)
        { this.GridInitialized = false; base.WhenItemsSet(oldItems, newItems); }

        public override void Render(IConsoleRenderer renderer, Region region)
        {
            this.EnsureInitialized(renderer);
            this.Grid.Render(renderer, region);
        }

        public override Size Measure(IConsoleRenderer renderer, Size maxSize)
        {
            this.EnsureInitialized(renderer);
            return this.Grid.Measure(renderer, maxSize);
        }

        private void EnsureInitialized(IConsoleRenderer renderer)
        {
            if (this.GridInitialized) { return; }
            //if (this.ContentStyle is null) { throw new ArgumentNullException(nameof(this.ContentStyle)); }
            if (this.ContentValueFunc is null) { throw new ArgumentNullException(nameof(this.ContentValueFunc)); }

            this.ContentColumn = new TableViewColumn<TItem, TValue>(this.ContentValueFunc, null, this.ContentStyle ?? ColumnDefinition.Auto());

            var symbolColumn = this.GetSymbolColumn();
            if (symbolColumn is null) { this.Grid.SetColumns(this.ContentColumn.ColumnDefinition); }
            else { this.Grid.SetColumns(symbolColumn, this.ContentColumn.ColumnDefinition); }

            var rowCount = this.Items.Count;
            this.Grid.SetRows(Enumerable.Repeat(RowDefinition.Auto(), rowCount).ToArray());

            for (int irow = 0; irow < this.Items.Count; irow++)
            {
                TItem item = this.Items[irow];
                IView symbolCell = this.GetSymbolView(item, irow, renderer.Formatter);
                IView contentCell = this.ContentColumn.GetCell(item, renderer.Formatter);
                if (symbolCell is null) { this.Grid.SetChild(contentCell, 0, irow); }
                else
                {
                    this.Grid.SetChild(symbolCell, 0, irow);
                    this.Grid.SetChild(contentCell, 1, irow);
                }
            }

            this.GridInitialized = true;
        }
    }
}
