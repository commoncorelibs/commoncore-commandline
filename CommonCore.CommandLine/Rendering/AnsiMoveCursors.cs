﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerStepThrough]
    public static class AnsiMoveCursors
    {
        public static Ansi Up(int lines = 1) => Ansi.From('A', lines);

        public static Ansi Down(int lines = 1) => Ansi.From('B', lines);

        public static Ansi Right(int columns = 1) => Ansi.From('C', columns);

        public static Ansi Left(int columns = 1) => Ansi.From('D', columns);

        public static Ansi NextLine(int line = 1) => Ansi.From('E', line);

        public static Ansi ToUpperLeftCorner { get; } = Ansi.From('H');

        public static Ansi ToLocation(int? left = null, int? top = null) => Ansi.From('H', top, left);
    }
}
