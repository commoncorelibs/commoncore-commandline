﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.CommandLine.Invocation
{
    public enum EMiddlewareOrder : int
    {
        Default = 0,

        PreInitialize = Initialize - 1_000,
        Initialize = -100_000,
        PostInitialize = Initialize + 1_000,

        EarlyCustomSetup = -70_000,
        SetupExceptionHandler = -64_000,
        SetupCancellation = -63_000,
        SetupConsole = -62_000,
        SetupDotnetSuggest = -61_000,
        SetupTypoCorrection = -60_000,
        LateCustomSetup = -60_000,

        EarlyCustomDirective = -50_000,
        DebugDirective = -43_000,
        ParseDirective = -42_000,
        SuggestDirective = -41_000,
        LateCustomDirective = -40_000,

        EarlyExclusiveOption = -30_000,
        ExclusiveOption = -25_000,
        LateExclusiveOption = -20_000,

        PreConfiguration = Configuration - 1_000,
        Configuration = -10_000,
        PostConfiguration = Configuration + 1_000,

        // Note: Default middleware order is 0, which falls here.

        PreErrorReporting = ErrorReporting - 1_000,
        ErrorReporting = 10_000,
        PostErrorReporting = ErrorReporting + 1_000,
    }
}
