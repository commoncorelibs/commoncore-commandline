﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace CommonCore.CommandLine.Reflection
{
    public static class AssemblyExtensions
    {
        /// <summary>
        /// Gets assembly attribute of the specified type <typeparamref name="T"/> or <c>default</c>
        /// if the attribute is not found. In the case that more than one attribute of the specified
        /// type is found, the first will be returned.
        /// </summary>
        /// <typeparam name="T">Attribute type to find.</typeparam>
        /// <param name="self">Assembly from which to get the custom attribute.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns>The specified attribute of type T or the default value of type T.</returns>
        public static T GetAttribute<T>(this Assembly self) where T : Attribute
        {
            IfSelfNull(self);
            var attributes = self.GetCustomAttributes<T>();
            return (attributes == null ? default : attributes.FirstOrDefault());
        }

        /// <summary>
        /// Gets <see cref="AssemblyCompanyAttribute.Company"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyCompanyAttribute.Company"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetCompany(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyCompanyAttribute>()?.Company ?? default;

        /// <summary>
        /// Gets <see cref="ComVisibleAttribute.Value"/> from the specified
        /// <see cref="Assembly"/> or <c>false</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="ComVisibleAttribute.Value"/> if it exists; otherwise <c>false</c>.</returns>
        public static bool GetComVisible(this Assembly self)
            => IfSelfNull(self).GetAttribute<ComVisibleAttribute>()?.Value ?? false;

        /// <summary>
        /// Gets <see cref="AssemblyConfigurationAttribute.Configuration"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyConfigurationAttribute.Configuration"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetConfiguration(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyConfigurationAttribute>()?.Configuration ?? default;

        /// <summary>
        /// Gets <see cref="AssemblyCopyrightAttribute.Copyright"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyCopyrightAttribute.Copyright"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetCopyright(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyCopyrightAttribute>()?.Copyright ?? default;

        /// <summary>
        /// Gets <see cref="CultureInfo"/> for <see cref="AssemblyCultureAttribute.Culture"/> from the specified
        /// <see cref="Assembly"/> or <see cref="AssemblyName.CultureInfo"/> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyCultureAttribute.Culture"/> if it exists; otherwise <c>default</c>.</returns>
        public static CultureInfo GetCulture(this Assembly self)
        {
            IfSelfNull(self);
            var attribute = self.GetAttribute<AssemblyCultureAttribute>();
            return attribute == null ? self.GetName().CultureInfo : CultureInfo.CreateSpecificCulture(attribute.Culture);
        }

        /// <summary>
        /// Gets <see cref="AssemblyDescriptionAttribute.Description"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyDescriptionAttribute.Description"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetDescription(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyDescriptionAttribute>()?.Description ?? default;

        /// <summary>
        /// Gets <see cref="AssemblyFileVersionAttribute.Version"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyFileVersionAttribute.Version"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetFileVersion(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyFileVersionAttribute>()?.Version ?? default;

        /// <summary>
        /// Gets <see cref="AssemblyName.FullName"/> from the specified
        /// <see cref="Assembly"/>.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyName.FullName"/>.</returns>
        public static string GetFullName(this Assembly self)
            => IfSelfNull(self).GetName().FullName;

        /// <summary>
        /// Gets <see cref="GuidAttribute.Value"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="GuidAttribute.Value"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetGuid(this Assembly self)
            => IfSelfNull(self).GetAttribute<GuidAttribute>()?.Value ?? default;

        ///// <summary>
        ///// Gets <see cref="Icon"/> associated with the specified <see cref="Assembly"/>.
        ///// </summary>
        ///// <param name="self">Assembly to query for information.</param>
        ///// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        ///// <returns><see cref="Icon"/> associated with the specified <see cref="Assembly"/>.</returns>
        //public static Icon GetIcon(this Assembly self)
        //{
        //    IfSelfNull(self);
        //    return Icon.ExtractAssociatedIcon(self.Location);
        //}

        /// <summary>
        /// Gets <see cref="AssemblyInformationalVersionAttribute.InformationalVersion"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyInformationalVersionAttribute.InformationalVersion"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetInformationalVersion(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion ?? default;

        /// <summary>
        /// Gets <see cref="AssemblyProductAttribute.Product"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyProductAttribute.Product"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetProduct(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyProductAttribute>()?.Product ?? default;

        /// <summary>
        /// Gets <see cref="AssemblyName.Name"/> from the specified
        /// <see cref="Assembly"/>.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyName.Name"/>.</returns>
        public static string GetShortName(this Assembly self)
            => IfSelfNull(self).GetName().Name;

        /// <summary>
        /// Gets <see cref="AssemblyTitleAttribute.Title"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyTitleAttribute.Title"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetTitle(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyTitleAttribute>()?.Title ?? default;

        /// <summary>
        /// Gets the value of <see cref="AssemblyTrademarkAttribute"/> from the specified
        /// <see cref="Assembly"/> or <c>default</c> if the attribute is not found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyTrademarkAttribute.Trademark"/> if it exists; otherwise <c>default</c>.</returns>
        public static string GetTrademark(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyTrademarkAttribute>()?.Trademark ?? default;

        /// <summary>
        /// Gets the value of <see cref="AssemblyVersionAttribute"/> from the specified
        /// <see cref="Assembly"/> or <see cref="AssemblyName.Version"/> if the attribute is not found
        /// or <c>default</c> if nothing is found.
        /// </summary>
        /// <param name="self">Assembly to query for information.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="self"/> is <c>null</c>.</exception>
        /// <returns><see cref="AssemblyVersionAttribute.Version"/> if it exists; otherwise <see cref="AssemblyName.Version"/>.</returns>
        public static string GetVersion(this Assembly self)
            => IfSelfNull(self).GetAttribute<AssemblyVersionAttribute>()?.Version ?? self.GetName().Version?.ToString() ?? default;

        #region IfSelfNull Methods

        /// <summary>
        /// Throws <see cref="NullReferenceException"/> if the given <paramref name="value"/> is <c>null</c>.
        /// This method is primarily intended for extension methods to verify they are being called
        /// from a valid non-null object, similar to non-extension methods.
        /// </summary>
        /// <typeparam name="T">The type of object to validate.</typeparam>
        /// <param name="value">The object to validate.</param>
        /// <exception cref="NullReferenceException">If <paramref name="value"/> is <c>null</c>.</exception>
        /// <remarks>This method is borrowed from CommonCore in prep for porting into that library.</remarks>
        //Object reference not set to an instance of an object.
        [System.Diagnostics.DebuggerStepThrough()]
        internal static T IfSelfNull<T>(T value) where T : class
        { if (value == null) { throw new NullReferenceException(); } else { return value; } }

        #endregion
    }
}
