﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion


namespace CommonCore.CommandLine.Rendering.Views
{
    public class ViewHost : ViewHost<IView>
    {
        public ViewHost()
            : base(default, default) { }

        public ViewHost(IView view)
            : base(view, default) { }

        public ViewHost(IConsoleRenderer renderer)
            : base(default, renderer) { }

        public ViewHost(IView view, IConsoleRenderer renderer)
            : base(view, renderer) { }

        //public ViewHost(SynchronizationContext synchronizationContext = null)
        //    : base(default, null, synchronizationContext) { }

        //public ViewHost(IConsoleRenderer renderer, SynchronizationContext synchronizationContext = null)
        //    : base(null, renderer, synchronizationContext) { }

        //public ViewHost(IView view, IConsoleRenderer renderer = null, SynchronizationContext synchronizationContext = null)
        //    : base(view, renderer, synchronizationContext) { }
    }

    public class ViewHost<TView>// : IDisposable
        where TView : IView
    {
        public ViewHost()
            : this(default, default) { }

        public ViewHost(TView view)
            : this(view, default) { }

        public ViewHost(IConsoleRenderer renderer)
            : this(default, renderer) { }

        public ViewHost(TView view, IConsoleRenderer renderer)
        {
            this.View = view;
            this.Renderer = renderer ?? Services.Instance.Get<IConsoleRenderer>();
        }

        //public ViewHost(SynchronizationContext synchronizationContext = null)
        //    : this(default, null, synchronizationContext) { }

        //public ViewHost(IConsoleRenderer renderer, SynchronizationContext synchronizationContext = null)
        //    : this(default, renderer, synchronizationContext) { }

        //public ViewHost(TView view, IConsoleRenderer renderer = null, SynchronizationContext synchronizationContext = null)
        //{
        //    this.View = view;
        //    this.Renderer = renderer ?? CCUtils.Services.Get<IConsoleRenderer>();
        //    this.Context = synchronizationContext ?? SynchronizationContext.Current ?? new SynchronizationContext();
        //}

        //private int _renderRequested;
        //private int _renderInProgress;

        public IConsoleRenderer Renderer { get; set; }

        public IView View { get; set; }

        //public SynchronizationContext Context { get; }

        //public TView View
        //{
        //    get => this._view;
        //    set
        //    {
        //        if (value is null) { throw new ArgumentNullException(nameof(value)); }

        //        if (this._view != null) { this._view.Updated -= this.ChildUpdated; }
        //        this._view = value;
        //        this._view.Updated += this.ChildUpdated;
        //    }
        //}
        //private TView _view;

        //protected void ChildUpdated(object sender, EventArgs e)
        //{
        //    if (Interlocked.CompareExchange(ref this._renderRequested, 1, 0) == 0)
        //    {
        //        this.Context.Post(x =>
        //        {
        //            while (Interlocked.CompareExchange(ref this._renderRequested, 0, 1) == 1)
        //            {
        //                if (Interlocked.CompareExchange(ref this._renderInProgress, 1, 0) == 0)
        //                {
        //                    this.Render();
        //                    Interlocked.Exchange(ref this._renderInProgress, 0);
        //                }
        //            }
        //        }, null);
        //    }
        //}

        public void Render()
            => this.View?.Render(this.Renderer, this.Renderer.Region);

        public void Measure()
            => this.View?.Measure(this.Renderer, new Size(this.Renderer.Region.Width, this.Renderer.Region.Height));

        #region Implement IDisposable

        ///// <summary>
        ///// Gets whether the current object has been disposed.
        ///// </summary>
        //public bool Disposed { get; private set; } = false;

        ///// <summary>
        ///// Releases all resources used by the current object.
        ///// </summary>
        //public void Dispose()
        //{
        //    this.Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        ///// <summary>
        ///// Releases managed and/or unmanaged resources used by the current object.
        ///// </summary>
        ///// <param name="disposing">Should all resources be released, else only unmanaged resources.</param>
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!this.Disposed)
        //    {
        //        if (disposing)
        //        {
        //            this.Renderer.Console.GetTerminal()?.ShowCursor();
        //            if (this._view != null) { this._view.Updated -= this.ChildUpdated; }
        //        }
        //        this.Disposed = true;
        //    }
        //}

        #endregion

    }
}
