﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.CommandLine;
using CommonCore.CommandLine.Help;
using CommonCore.CommandLine.Invocation;

namespace CommonCore.CommandLine.Symbols
{
    public class CoreCommand : Command, IMiddlewareProvider
    {
        public CoreCommand(string commandName)
            : base(commandName) { }

        public HelpInfo HelpInfo { get; set; }

        public List<Middleware> Middlewares { get; } = new List<Middleware>();

        public List<ICommand> AdditonalUsages { get; } = new List<ICommand>();

        public void AddMiddleware(Middleware middleware)
            => this.Middlewares.Add(middleware);

        public void AddMiddleware(params Middleware[] middlewares)
            => this.Middlewares.AddRange(middlewares);

        public void AddMiddleware(IEnumerable<Middleware> middlewares)
            => this.Middlewares.AddRange(middlewares);

        public void AddUsage(ICommand command)
            => this.AdditonalUsages.Add(command);

        public void AddUsage(params ICommand[] commands)
            => this.AdditonalUsages.AddRange(commands);

        public void AddUsage(IEnumerable<ICommand> commands)
            => this.AdditonalUsages.AddRange(commands);
    }
}
