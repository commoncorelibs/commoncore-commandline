﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CommonCore.CommandLine.Rendering.Views.Spans
{
    public class TextSpanFormatter : ICustomFormatter, IFormatProvider
    {
        private static Regex _formattableStringParser { get; }
            = TextSpanFormatter._formattableStringParser = new Regex(
                @"(\s*{{\s*) | (\s*}}\s*) | (?<token> \{ [0-9]+ [^\}]* \} ) | (?<text> [^\{\}]* )",
                RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

        private readonly Dictionary<Type, Func<object, ISpan>> _formatters = new Dictionary<Type, Func<object, ISpan>>();

        public void AddFormatter<T>(Func<T, ISpan> format)
            => this._formatters.Add(typeof(T), t => format((T)t) ?? ContentSpan.Empty());

        public ISpan Format(object value)
        {
            string content;

            switch (value)
            {
                case null: { return ContentSpan.Empty(); }
                case ISpan span: { return span; }
                case Ansi ansiCode: { return new ControlSpan(ansiCode.Code, ansiCode); }
                case FormattableString formattable: { content = ((IFormattable)formattable).ToString("", this); break; }
                default: { content = value.ToString(); break; }
            }

            if (this._formatters.TryGetValue(value.GetType(), out var formatter))
            { return formatter(value); }

            return string.IsNullOrEmpty(content) ? ContentSpan.Empty() : new ContentSpan(content);
        }

        public void AddFormatter<T>(Func<T, FormattableString> format)
        {
            this._formatters.Add(typeof(T), t =>
                {
                    var content = format((T)t);
                    return content is null ? ContentSpan.Empty() : this.ParseToSpan(content);
                });
        }

        object IFormatProvider.GetFormat(Type formatType) => this;

        string ICustomFormatter.Format(string format, object arg, IFormatProvider formatProvider)
            => this.Format(arg).ToString();

        public ISpan ParseToSpan(FormattableString formattableString)
        {
            var formatted = formattableString.ToString();

            var args = formattableString.GetArguments();

            if (args.Length == 0)
            {
                return this.Format(formatted);
            }
            else
            {
                return new ContainerSpan(DestructureIntoSpans().ToArray());
            }

            IEnumerable<ISpan> DestructureIntoSpans()
            {
                var partIndex = 0;
                foreach (Match match in _formattableStringParser.Matches(formattableString.Format))
                {
                    if (match.Value.Length > 0)
                    {
                        if (match.Value.StartsWith("{") && match.Value.EndsWith("}"))
                        {
                            var arg = args[partIndex++];
                            if (match.Value.Contains(":"))
                            {
                                var formatString = match.Value.Split(new[] { '{', ':', '}' }, 4)[2];
                                yield return this.Format(string.Format("{0:" + formatString + "}", arg));
                            }
                            else
                            {
                                yield return this.Format(arg);
                            }
                        }
                        else
                        {
                            yield return this.Format(match.Value);
                        }
                    }
                }
            }
        }
    }
}
