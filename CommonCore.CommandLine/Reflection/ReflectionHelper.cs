﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Reflection;

namespace CommonCore.CommandLine.Reflection
{
    public static class ReflectionHelper
    {
        public static FieldInfo GetFieldInfo<TType>(string name)
            => GetFieldInfo(typeof(TType), name);

        public static FieldInfo GetFieldInfo(Type type, string name)
        {
            if (type is null) { throw new ArgumentNullException(nameof(type)); }
            if (name is null) { throw new ArgumentNullException(nameof(name)); }

            FieldInfo info;
            do
            {
                info = type.GetField(name, BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                type = type.BaseType;
            }
            while (info is null && type != null);
            return info;
        }

        public static TValue GetFieldValue<TValue>(Type type, string name)
            => (TValue)GetFieldValue(type, null, name);

        public static object GetFieldValue(Type type, string name)
            => GetFieldValue(type, null, name);

        public static TValue GetFieldValue<TValue>(object instance, string name)
            => (TValue)GetFieldValue(instance?.GetType(), instance, name);

        public static object GetFieldValue(object instance, string name)
            => GetFieldValue(instance?.GetType(), instance, name);

        public static TValue GetFieldValue<TValue>(Type type, object instance, string name)
            => (TValue)GetFieldValue(type, instance, name);

        public static object GetFieldValue(Type type, object instance, string name)
        {
            if (type is null) { throw new ArgumentNullException(nameof(type)); }
            if (name is null) { throw new ArgumentNullException(nameof(name)); }

            var info = GetFieldInfo(type, name);
            if (info is null) { throw new ArgumentOutOfRangeException(nameof(name), string.Format("Couldn't find field '{0}' in type '{1}'.", name, type.FullName)); }
            return info.GetValue(instance);
        }

        //public static TValue GetFieldValue<TValue>(object obj, string name)
        //    => (TValue)GetFieldValue(obj, name);

        //public static object GetFieldValue(object obj, string name)
        //{
        //    if (obj is null) { throw new ArgumentNullException(nameof(obj)); }
        //    var info = GetFieldInfo(obj.GetType(), name);
        //    if (info is null) { throw new ArgumentOutOfRangeException(nameof(name), string.Format("Couldn't find field '{0}' in type '{1}'.", name, obj.GetType().FullName)); }
        //    return info.GetValue(obj);
        //}

        public static void SetFieldValue(object obj, string name, object value)
        {
            if (obj is null) { throw new ArgumentNullException(nameof(obj)); }
            var info = GetFieldInfo(obj.GetType(), name);
            if (info is null) { throw new ArgumentOutOfRangeException(nameof(name), string.Format("Couldn't find field {0} in type {1}", name, obj.GetType().FullName)); }
            info.SetValue(obj, value);
        }

        public static PropertyInfo GetPropertyInfo(Type type, string name)
        {
            PropertyInfo propInfo;
            do
            {
                propInfo = type.GetProperty(name, BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                type = type.BaseType;
            }
            while (propInfo is null && type != null);
            return propInfo;
        }

        public static TValue GetPropertyValue<TValue>(object obj, string name)
            => (TValue)GetPropertyValue(obj, name);

        public static object GetPropertyValue(object obj, string name)
        {
            if (obj is null) { throw new ArgumentNullException(nameof(obj)); }
            var info = GetPropertyInfo(obj.GetType(), name);
            if (info is null) { throw new ArgumentOutOfRangeException(nameof(name), string.Format("Couldn't find property {0} in type {1}", name, obj.GetType().FullName)); }
            return info.GetValue(obj, null);
        }

        public static void SetPropertyValue(object obj, string name, object value)
        {
            if (obj is null) { throw new ArgumentNullException(nameof(obj)); }
            var info = GetPropertyInfo(obj.GetType(), name);
            if (info is null) { throw new ArgumentOutOfRangeException(nameof(name), string.Format("Couldn't find property {0} in type {1}", name, obj.GetType().FullName)); }
            info.SetValue(obj, value, null);
        }

        public static MethodInfo GetMethodInfo(Type type, string name)
        {
            MethodInfo info;
            do
            {
                info = type.GetMethod(name, BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                type = type.BaseType;
            }
            while (info is null && type != null);
            return info;
        }

        public static TReturn CallMethodInfo<TReturn>(object obj, string name, params object[] parameters)
            => (TReturn)CallMethodInfo(obj, name, parameters);

        public static object CallMethodInfo(object obj, string name, params object[] parameters)
        {
            if (obj is null) { throw new ArgumentNullException(nameof(obj)); }
            var info = GetMethodInfo(obj.GetType(), name);
            if (info is null) { throw new ArgumentOutOfRangeException(nameof(name), string.Format("Couldn't find method {0} in type {1}", name, obj.GetType().FullName)); }
            return info.Invoke(obj, parameters);
        }
    }
}
