﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Collections.Generic;
using CommonCore.CommandLine.IO;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering.Visitors
{
    public class TerminalSpanRenderer : TextSpanRenderer
    {
        public TerminalSpanRenderer(ITerminal terminal, Region region)
            : base(terminal?.Out, region)
            => this.Terminal = terminal ?? throw new ArgumentNullException(nameof(terminal));

        private ITerminal Terminal { get; }

        public override void VisitControlSpan(ControlSpan controlSpan)
        {
        }

        public override void VisitForegroundColorSpan(ForeColorSpan span)
        {
            if (_foregroundColorMappings.TryGetValue(span.Name, out var color))
            { this.Terminal.ForegroundColor = color; }
            else
            {
                var backgroundColor = this.Terminal.BackgroundColor;
                this.Terminal.ResetColor();
                this.Terminal.BackgroundColor = backgroundColor;
            }
        }

        public override void VisitBackgroundColorSpan(BackColorSpan span)
        {
            if (_backgroundColorMappings.TryGetValue(span.Name, out var color))
            { this.Terminal.BackgroundColor = color; }
            else
            {
                var foregroundColor = this.Terminal.ForegroundColor;
                this.Terminal.ResetColor();
                this.Terminal.ForegroundColor = foregroundColor;
            }
        }

        public override void VisitCursorControlSpan(CursorControlSpan cursorControlSpan)
        {
            // TODO: Using a string to check against the text name of a method on that class,
            // only to call a method on a different class?
            // There has to be a better way; this shouldn't need more than a boolean.
            switch (cursorControlSpan.Name)
            {
                case nameof(CursorControlSpan.Hide): { this.Terminal.HideCursor(); break; }
                case nameof(CursorControlSpan.Show): { this.Terminal.ShowCursor(); break; }
            }
        }

        private static Dictionary<string, ConsoleColor> _backgroundColorMappings { get; } =
            new Dictionary<string, ConsoleColor>
            {
                [nameof(BackColorSpan.Black)] = ConsoleColor.Black,
                [nameof(BackColorSpan.Red)] = ConsoleColor.DarkRed,
                [nameof(BackColorSpan.Green)] = ConsoleColor.DarkGreen,
                [nameof(BackColorSpan.Yellow)] = ConsoleColor.DarkYellow,
                [nameof(BackColorSpan.Blue)] = ConsoleColor.DarkBlue,
                [nameof(BackColorSpan.Magenta)] = ConsoleColor.DarkMagenta,
                [nameof(BackColorSpan.Cyan)] = ConsoleColor.DarkCyan,
                [nameof(BackColorSpan.White)] = ConsoleColor.White,
                [nameof(BackColorSpan.DarkGray)] = ConsoleColor.DarkGray,
                [nameof(BackColorSpan.LightRed)] = ConsoleColor.Red,
                [nameof(BackColorSpan.LightGreen)] = ConsoleColor.Green,
                [nameof(BackColorSpan.LightYellow)] = ConsoleColor.Yellow,
                [nameof(BackColorSpan.LightBlue)] = ConsoleColor.Blue,
                [nameof(BackColorSpan.LightMagenta)] = ConsoleColor.Magenta,
                [nameof(BackColorSpan.LightCyan)] = ConsoleColor.Cyan,
                [nameof(BackColorSpan.LightGray)] = ConsoleColor.Gray,
            };

        private static Dictionary<string, ConsoleColor> _foregroundColorMappings { get; } =
            new Dictionary<string, ConsoleColor>
            {
                [nameof(ForeColorSpan.Black)] = ConsoleColor.Black,
                [nameof(ForeColorSpan.Red)] = ConsoleColor.DarkRed,
                [nameof(ForeColorSpan.Green)] = ConsoleColor.DarkGreen,
                [nameof(ForeColorSpan.Yellow)] = ConsoleColor.DarkYellow,
                [nameof(ForeColorSpan.Blue)] = ConsoleColor.DarkBlue,
                [nameof(ForeColorSpan.Magenta)] = ConsoleColor.DarkMagenta,
                [nameof(ForeColorSpan.Cyan)] = ConsoleColor.DarkCyan,
                [nameof(ForeColorSpan.White)] = ConsoleColor.White,
                [nameof(ForeColorSpan.BrightBlack)] = ConsoleColor.DarkGray,
                [nameof(ForeColorSpan.BrightRed)] = ConsoleColor.Red,
                [nameof(ForeColorSpan.BrightGreen)] = ConsoleColor.Green,
                [nameof(ForeColorSpan.BrightYellow)] = ConsoleColor.Yellow,
                [nameof(ForeColorSpan.BrightBlue)] = ConsoleColor.Blue,
                [nameof(ForeColorSpan.BrightMagenta)] = ConsoleColor.Magenta,
                [nameof(ForeColorSpan.BrightCyan)] = ConsoleColor.Cyan,
                [nameof(ForeColorSpan.BrightWhite)] = ConsoleColor.Gray,
            };
    }
}
