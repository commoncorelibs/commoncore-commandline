﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.CommandLine.Rendering
{
    public static class RegionExtensions
    {
        public static Region Clone(this Region self)
            => new Region(self.X, self.Y, self.Width, self.Height, self.IsOverwrittenOnRender);

        public static Region Clone(this Region self, bool isOverwrittenOnRender)
            => new Region(self.X, self.Y, self.Width, self.Height, isOverwrittenOnRender);

        public static Region Clone(this Region self, int? width, int? height, bool? isOverwrittenOnRender = null)
            => new Region(self.X, self.Y,
                width ?? self.Width, height ?? self.Height,
                isOverwrittenOnRender ?? self.IsOverwrittenOnRender);

        public static Region Clone(this Region self, Size size, bool? isOverwrittenOnRender = null)
            => new Region(self.X, self.Y, size.Width, size.Height,
                isOverwrittenOnRender ?? self.IsOverwrittenOnRender);

        public static Region Clone(this Region self, Point point, bool? isOverwrittenOnRender = null)
            => new Region(point.X, point.Y, self.Width, self.Height,
                isOverwrittenOnRender ?? self.IsOverwrittenOnRender);
    }
}
