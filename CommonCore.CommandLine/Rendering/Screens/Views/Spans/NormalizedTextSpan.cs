﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Text;

namespace CommonCore.CommandLine.Rendering.Views.Spans
{
    public class NormalizedTextSpan : ContentSpan, IEquatable<NormalizedTextSpan>
    {
        public NormalizedTextSpan(string content)
            : base(content is null ? string.Empty : NormalizeWhiteSpace(content)) { }

        public override int GetHashCode() => HashCode.Combine(this.Content);

        public override bool Equals(object obj) => obj is NormalizedTextSpan span && this.Equals(span);

        public bool Equals(NormalizedTextSpan other) => !(other is null) && this.Content == other.Content;

        public static bool operator ==(NormalizedTextSpan left, NormalizedTextSpan right)
            => left is null && right is null || !(left is null || right is null) || left.Equals(right);

        public static bool operator !=(NormalizedTextSpan left, NormalizedTextSpan right)
            => !(left == right);

        private static string NormalizeWhiteSpace(ReadOnlySpan<char> chars, char delimiter = ' ')
        {
            if (chars.IsEmpty) { return string.Empty; }

            var output = new StringBuilder();

            bool skip = false;
            foreach (char @char in chars)
            {
                if (char.IsWhiteSpace(@char))
                {
                    if (skip) { continue; }
                    output.Append(delimiter);
                    skip = true;
                }
                else
                {
                    output.Append(@char);
                    skip = false;
                }
            }

            return output.ToString();
        }
    }
}
