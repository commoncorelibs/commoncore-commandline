﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;

namespace CommonCore.CommandLine.Rendering.Views
{
    public class BulletedListView : BulletedListView<string, string>
    {
        public BulletedListView(ColumnDefinition contentStyle = null)
            : base(contentStyle) { }

        public BulletedListView(Func<string, string> cellFunc, ColumnDefinition contentStyle = null)
            : base(cellFunc, contentStyle) { }

        public BulletedListView(IEnumerable<string> items, ColumnDefinition contentStyle = null)
            : base(items, contentStyle) { }

        public BulletedListView(IEnumerable<string> items, Func<string, string> cellFunc, ColumnDefinition contentStyle = null)
            : base(items, cellFunc, contentStyle) { }
    }

    public class BulletedListView<TItem> : BulletedListView<TItem, string>
    {
        public BulletedListView(ColumnDefinition contentStyle = null)
            : base(contentStyle) { }

        public BulletedListView(Func<TItem, string> cellFunc, ColumnDefinition contentStyle = null)
            : base(cellFunc, contentStyle) { }

        public BulletedListView(IEnumerable<TItem> items, ColumnDefinition contentStyle = null)
            : base(items, contentStyle) { }

        public BulletedListView(IEnumerable<TItem> items, Func<TItem, string> cellFunc, ColumnDefinition contentStyle = null)
            : base(items, cellFunc, contentStyle) { }
    }

    public class BulletedListView<TItem, TValue> : ListView<TItem, TValue>
    {
        public BulletedListView(ColumnDefinition contentStyle = null)
            : base(contentStyle)
            => this.Init();

        public BulletedListView(Func<TItem, TValue> cellFunc, ColumnDefinition contentStyle = null)
            : base(cellFunc, contentStyle)
            => this.Init();

        public BulletedListView(IEnumerable<TItem> items, ColumnDefinition contentStyle = null)
            : base(items, contentStyle)
            => this.Init();

        public BulletedListView(IEnumerable<TItem> items, Func<TItem, TValue> cellFunc, ColumnDefinition contentStyle = null)
            : base(items, cellFunc, contentStyle)
            => this.Init();

        protected virtual void Init()
        {
            this.Delimiter = " ";
            this.Symbol = "*";
        }
    }
}
