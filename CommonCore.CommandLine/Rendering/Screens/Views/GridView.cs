﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering.Views
{
    public class GridView : AContainerView
    {
        public GridView()
        {
            //1 column 1 row
            this._cols.Add(ColumnDefinition.Star(1));
            this._rows.Add(RowDefinition.Star(1));

            this.CellContents = new IView[1, 1];
        }

        private readonly List<ColumnDefinition> _cols = new List<ColumnDefinition>();
        private readonly List<RowDefinition> _rows = new List<RowDefinition>();

        private IView[,] CellContents { get; set; }

        public int Indenting { get; set; } = 0;

        public string Delimiter { get; set; } = new string(' ', 2);//" | ";

        public string Prefix { get; set; } = null;//"| ";

        public string Suffix { get; set; } = null;//" |";

        public int CellCount => this.ColCount * this.RowCount;

        public int ColCount => this._cols.Count;

        public int RowCount => this._rows.Count;

        public IEnumerable<IView> GetColCells(int col)
        { for (int row = 0; row < this.RowCount; row++) { yield return this[col, row]; } }

        public IEnumerable<IView> GetRowCells(int row)
        { for (int col = 0; col < this.ColCount; col++) { yield return this[col, row]; } }

        public IView this[int col, int row] { get => this.CellContents[col, row]; set => this.SetChild(value, col, row); }

        //TODO: Should this be row, column or column, row?
        // ANSWER: It's (column, row), always (column, row) unless specific reasons otherwise.
        public void SetChild(IView child, int column, int row)
        {
            //TODO: Ensure row/column is in a valid range
            base.Add(child);
            this.CellContents[column, row] = child;
        }

        public override void Add(IView child) => throw new InvalidOperationException("Must call SetChild(IView child, int column, int row) instead.");

        public void SetColumns(params ColumnDefinition[] columns)
        {
            if (columns is null) { throw new ArgumentNullException(nameof(columns)); }
            if (this.Views.Count > 0) { throw new InvalidOperationException("Cannot change columns once children are added."); }
            if (columns.Length == 0) { throw new ArgumentException("Must specify at least one column.", nameof(columns)); }

            this._cols.Clear();
            this._cols.AddRange(columns);
            // TODO: Recreating this array in two separate places gives a WET smell.
            this.CellContents = new IView[this._cols.Count, this._rows.Count];
        }

        public void SetRows(params RowDefinition[] rows)
        {
            if (rows is null) { throw new ArgumentNullException(nameof(rows)); }
            if (this.Views.Count > 0) { throw new InvalidOperationException("Cannot change rows once children are added."); }
            if (rows.Length == 0) { throw new ArgumentException("Must specify at least one column.", nameof(rows)); }

            this._rows.Clear();
            this._rows.AddRange(rows);
            // TODO: Recreating this array in two separate places gives a WET smell.
            this.CellContents = new IView[this._cols.Count, this._rows.Count];
        }

        public override void Render(IConsoleRenderer renderer, Region region)
        {
            Size[,] sizes = this.GetCellSizes(renderer, new Size(region.Width, region.Height));

            int top = region.Top;
            for (int row = 0; row < this._rows.Count; row++)
            {
                int left = region.Left;

                if (this.Indenting > 0)
                {
                    renderer.Render(new ContentSpan(new string(' ', this.Indenting)), new Region(left, top, new Size(this.Indenting, 1)));
                    left += this.Indenting;
                }

                if (this.Prefix?.Length > 0)
                {
                    renderer.Render(new ContentSpan(this.Prefix), new Region(left, top, new Size(this.Prefix.Length, 1)));
                    left += this.Prefix.Length;
                }

                int maxRowHeight = 0;
                for (int icol = 0; icol < this._cols.Count; icol++)
                {
                    var col = this._cols[icol];
                    var cell = this.CellContents[icol, row];
                    var size = sizes[icol, row];

                    if (cell != null && size.Width > 0 && size.Height > 0)
                    {
                        var contentSize = cell.Measure(renderer, size);
                        if (size.Height == 1 && col.Alignment != default && contentSize.Width < size.Width)
                        {
                            int diff = size.Width - contentSize.Width;
                            if (col.Alignment == EAlignment.Right)
                            {
                                renderer.Render(new ContentSpan(new string(' ', diff)), new Region(left, top, new Size(diff, 1)));
                                left += diff;
                                cell.Render(renderer, new Region(left, top, new Size(contentSize.Width, size.Height)));
                                left += contentSize.Width;
                            }
                            else if (col.Alignment == EAlignment.Center)
                            {
                                bool preferLeft = true;
                                int leftWidth = diff / 2;
                                if (!preferLeft) { leftWidth += diff % 2; }
                                int rightWidth = diff - leftWidth;

                                renderer.Render(new ContentSpan(new string(' ', leftWidth)), new Region(left, top, new Size(leftWidth, 1)));
                                left += leftWidth;
                                cell.Render(renderer, new Region(left, top, new Size(contentSize.Width, size.Height)));
                                left += contentSize.Width;
                                renderer.Render(new ContentSpan(new string(' ', rightWidth)), new Region(left, top, new Size(rightWidth, 1)));
                                left += rightWidth;
                            }
                        }
                        else
                        {
                            cell.Render(renderer, new Region(left, top, size));
                            left += size.Width;
                        }
                    }
                    else
                    {
                        left += size.Width;
                    }

                    //cell.Render(renderer, new Region(left, top, size));
                    //left += size.Width;

                    ////var r = new Region(left, top, size);
                    ////var text = r.ToString().PadLeft(size.Width);
                    ////renderer.Render(new ContentSpan(text), r);
                    ////left += size.Width;


                    maxRowHeight = Math.Max(maxRowHeight, size.Height);

                    if (this.Delimiter?.Length > 0 && icol < this.ColCount - 1)
                    {
                        renderer.Render(new ContentSpan(this.Delimiter), new Region(left, top, new Size(this.Delimiter.Length, 1)));
                        left += this.Delimiter.Length;
                    }
                }

                if (this.Suffix?.Length > 0)
                {
                    renderer.Render(new ContentSpan(this.Suffix), new Region(left, top, new Size(this.Suffix.Length, 1)));
                    left += this.Suffix.Length;
                }

                top += maxRowHeight;
            }
        }

        public override Size Measure(IConsoleRenderer renderer, Size maxSize)
        {
            if (renderer is null) { throw new ArgumentNullException(nameof(renderer)); }

            Size[,] sizes = this.GetCellSizes(renderer, maxSize);

            int width = 0;
            for (int column = 0; column < this._cols.Count; column++)
            { width += sizes[column, 0].Width; }
            if (width > maxSize.Width) { width = maxSize.Width; }
            width += this.Indenting;
            width += (this.Delimiter?.Length ?? 0) * (this.ColCount - 1);
            width += this.Prefix?.Length ?? 0;
            width += this.Suffix?.Length ?? 0;

            int height = 0;
            for (int row = 0; row < this._rows.Count; row++)
            { height += sizes[0, row].Height; }
            if (height > maxSize.Height) { height = maxSize.Height; }

            return new Size(width, height);
        }

        private Size[,] GetCellSizes(IConsoleRenderer renderer, Size maxSize)
        {
            double totalColStarSize = this._cols.Where(col => col.SizeMode == ESizeMode.Star).Sum(col => col.Value);
            double totalRowStarSize = this._rows.Where(row => row.SizeMode == ESizeMode.Star).Sum(row => row.Value);

            int remainingWidth = maxSize.Width;
            remainingWidth -= this.Indenting;
            remainingWidth -= (this.Delimiter?.Length ?? 0) * (this.ColCount - 1);
            remainingWidth -= this.Prefix?.Length ?? 0;
            remainingWidth -= this.Suffix?.Length ?? 0;
            int? totalWidthForStarSizing = null;
            int? totalHeightForStarSizing = null;

            var widths = new int[this.ColCount];
            var icols = this._cols.Select((_, icol) => icol).OrderBy(icol => GetProcessOrder(this._cols[icol].SizeMode));
            foreach (int icol in icols)
            {
                var col = this._cols[icol];
                switch (col.SizeMode)
                {
                    case ESizeMode.Fixed: { widths[icol] = Math.Min((int)col.Value, remainingWidth); break; }
                    case ESizeMode.Auto:
                    {
                        widths[icol] = this.GetColCells(icol).Max(cell => cell?.Measure(renderer, new Size(remainingWidth, maxSize.Height)).Width ?? 0);
                        if (widths[icol] > remainingWidth) { widths[icol] = remainingWidth; }
                        break;
                    }
                    case ESizeMode.Star:
                    {
                        totalWidthForStarSizing ??= remainingWidth;

                        int width = (int)Math.Round(col.Value / totalColStarSize * totalWidthForStarSizing.Value);
                        if (width > widths[icol]) { widths[icol] = width; }
                        break;
                    }
                    default: { throw new InvalidOperationException($"Unknown column size mode '{col.SizeMode}'."); }
                }
                remainingWidth -= widths[icol];
            }

            int remainingHeight = maxSize.Height;
            var heights = new int[this.RowCount];
            var irows = this._rows.Select((_, irow) => irow).OrderBy(irow => GetProcessOrder(this._rows[irow].SizeMode));
            foreach (int irow in irows)
            {
                var row = this._rows[irow];
                switch (row.SizeMode)
                {
                    case ESizeMode.Fixed:
                    {
                        heights[irow] = Math.Min((int)row.Value, remainingHeight);
                        break;
                    }
                    case ESizeMode.Auto:
                    {
                        int icol = 0;
                        heights[irow] = this.GetRowCells(irow)
                            .Max(cell => cell?.Measure(renderer, new Size(widths[icol++], remainingHeight)).Height ?? 0);
                        if (heights[irow] > remainingHeight) { heights[irow] = remainingHeight; }
                        break;
                    }
                    case ESizeMode.Star:
                    {
                        totalHeightForStarSizing ??= remainingWidth;

                        int height = (int)Math.Round(row.Value / totalRowStarSize * totalHeightForStarSizing.Value);
                        if (height > heights[irow]) { heights[irow] = height; }
                        //heights[irow] = (int)Math.Round(row.Value / totalRowStarSize * maxSize.Height);
                        break;
                    }
                    default: { throw new InvalidOperationException($"Unknown row size mode '{this._rows[irow].SizeMode}'."); }
                }
                remainingHeight -= heights[irow];
            }

            var sizes = new Size[this.ColCount, this.RowCount];
            for (int icol = 0; icol < this.ColCount; icol++)
            {
                for (int irow = 0; irow < this.RowCount; irow++)
                {
                    sizes[icol, irow] = new Size(widths[icol], heights[irow]);
                }
            }
            return sizes;

            //var measuredCols = new int?[this._cols.Count];
            //var measuredRows = new int?[this._rows.Count];

            //int availableWidth = maxSize.Width;

            //availableWidth = maxSize.Width;
            //totalWidthForStarSizing = null;
            //foreach (var (col, icol) in this._cols.OrderBy(x => GetProcessOrder(x.SizeMode)).Select((x, i) => (x, i)))
            //{
            //    availableHeight = maxSize.Height;

            //    for (int irow = 0; irow < this._rows.Count; irow++)
            //    {
            //        if (measuredRows[irow] == null)
            //        {
            //            switch (this._rows[irow].SizeMode)
            //            {
            //                case SizeMode.Fixed:
            //                {
            //                    measuredRows[irow] = Math.Min((int)this._rows[irow].Value, availableHeight);
            //                    break;
            //                }
            //                case SizeMode.Star:
            //                {
            //                    measuredRows[irow] = (int)Math.Round(this._rows[irow].Value / totalRowStarSize * maxSize.Height);
            //                    break;
            //                }
            //                case SizeMode.Auto: { break; }
            //                default: { throw new InvalidOperationException($"Unknown row size mode {this._rows[irow].SizeMode}"); }
            //            }
            //        }
            //        Size childSize = null;
            //        switch (col.SizeMode)
            //        {
            //            case SizeMode.Fixed:
            //            {
            //                if (measuredCols[icol] == null)
            //                { measuredCols[icol] = Math.Min((int)col.Value, availableWidth); }
            //                break;
            //            }
            //            case SizeMode.Auto:
            //            {
            //                int width = 0;
            //                var cell = this.CellContents[icol, irow];
            //                if (cell != null)
            //                {
            //                    childSize = cell.Measure(renderer, new Size(availableWidth, availableHeight));
            //                    width += childSize.Width;
            //                    //if (icol < this.ColCount - 1) { width += this.Delimiter.Length; }
            //                }
            //                measuredCols[icol] = Math.Min(Math.Max(measuredCols[icol] ?? 0, width), availableWidth);
            //                break;
            //            }
            //            case SizeMode.Star:
            //            {
            //                if (totalWidthForStarSizing == null)
            //                {
            //                    totalWidthForStarSizing = availableWidth;
            //                }
            //                int starWidth = (int)Math.Round(col.Value / totalColStarSize * totalWidthForStarSizing.Value);
            //                if (measuredCols[icol] < starWidth)
            //                {
            //                    starWidth = measuredCols[icol].Value;
            //                }
            //                measuredCols[icol] = starWidth;
            //                break;
            //            }
            //            default: { throw new InvalidOperationException($"Unknown column size mode {col.SizeMode}"); }
            //        }

            //        if (this._rows[irow].SizeMode == SizeMode.Auto)
            //        {
            //            if (childSize == null && this.CellContents[icol, irow] is IView child)
            //            { childSize = child.Measure(renderer, new Size(availableWidth, availableHeight)); }
            //            measuredRows[irow] = Math.Min(Math.Max(childSize?.Height ?? 0, childSize?.Height ?? 0), availableHeight);
            //        }

            //        availableHeight -= measuredRows[irow].Value;
            //    }
            //    availableWidth -= measuredCols[icol].Value;
            //}

            //var rv = new Size[this._cols.Count, this._rows.Count];
            //for (int rowIndex = 0; rowIndex < this._rows.Count; rowIndex++)
            //{
            //    for (int columnIndex = 0; columnIndex < this._cols.Count; columnIndex++)
            //    {
            //        rv[columnIndex, rowIndex] = new Size(measuredCols[columnIndex].Value, measuredRows[rowIndex].Value);
            //    }
            //}
            //return rv;
        }

        private static int GetProcessOrder(ESizeMode sizeMode)
            => sizeMode switch
            {
                ESizeMode.Fixed => 0,
                ESizeMode.Auto => 1,
                ESizeMode.Star => 2,
                _ => throw new InvalidOperationException($"Unknown size mode {sizeMode}."),
            };
    }
}
