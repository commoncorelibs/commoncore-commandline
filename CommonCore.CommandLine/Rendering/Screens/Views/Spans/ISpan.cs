﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.IO;

namespace CommonCore.CommandLine.Rendering.Views.Spans
{
    public interface ISpan
    {
        int ContentLength { get; }

        ISpan Root { get; }

        ISpan Parent { get; }

        void RecalculatePositions(ISpan parent, int start);

        void WriteTo(TextWriter writer, ERenderMode outputMode);
    }
}
