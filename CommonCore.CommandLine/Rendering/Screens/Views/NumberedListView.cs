﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;

namespace CommonCore.CommandLine.Rendering.Views
{
    public class NumberedListView : NumberedListView<string, string>
    {
        public NumberedListView(ColumnDefinition contentStyle = null)
            : base(contentStyle) { }

        public NumberedListView(Func<string, string> cellFunc, ColumnDefinition contentStyle = null)
            : base(cellFunc, contentStyle) { }

        public NumberedListView(IEnumerable<string> items, ColumnDefinition contentStyle = null)
            : base(items, contentStyle) { }

        public NumberedListView(IEnumerable<string> items, Func<string, string> cellFunc, ColumnDefinition contentStyle = null)
            : base(items, cellFunc, contentStyle) { }
    }

    public class NumberedListView<TItem> : NumberedListView<TItem, string>
    {
        public NumberedListView(ColumnDefinition contentStyle = null)
            : base(contentStyle) { }

        public NumberedListView(Func<TItem, string> cellFunc, ColumnDefinition contentStyle = null)
            : base(cellFunc, contentStyle) { }

        public NumberedListView(IEnumerable<TItem> items, ColumnDefinition contentStyle = null)
            : base(items, contentStyle) { }

        public NumberedListView(IEnumerable<TItem> items, Func<TItem, string> cellFunc, ColumnDefinition contentStyle = null)
            : base(items, cellFunc, contentStyle) { }
    }

    public class NumberedListView<TItem, TValue> : ListView<TItem, TValue>
    {
        public NumberedListView(ColumnDefinition contentStyle = null)
            : base(contentStyle)
            => this.Init();

        public NumberedListView(Func<TItem, TValue> cellFunc, ColumnDefinition contentStyle = null)
            : base(cellFunc, contentStyle)
            => this.Init();

        public NumberedListView(IEnumerable<TItem> items, ColumnDefinition contentStyle = null)
            : base(items, contentStyle)
            => this.Init();

        public NumberedListView(IEnumerable<TItem> items, Func<TItem, TValue> cellFunc, ColumnDefinition contentStyle = null)
            : base(items, cellFunc, contentStyle)
            => this.Init();

        protected virtual void Init()
        {
            this.Delimiter = " ";
            this.SymbolAlignment = EAlignment.Right;
            this.SymbolFunc = (item, index) =>
            {
                var text = string.Format(this.NumberFormat ?? "{0}", index + this.NumberOffset);
                return text;
            };
        }

        public int NumberOffset { get; set; } = 1;

        public string NumberFormat { get; set; } = "{0:#,##0}.";
    }
}
