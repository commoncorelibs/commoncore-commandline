﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Runtime.Serialization;

namespace CommonCore.CommandLine.Exceptions
{
    [System.Runtime.InteropServices.ComVisible(true)]
    [Serializable]
    public class ArgumentDefaultException : ArgumentNullException
    {
        //public static void ThrowIf(object paramValue, string paramName)
        //{ if (paramValue is null) { throw new ArgumentNullException(paramName); } }

        //public static void ThrowIf(ValueType paramValue, string paramName)
        //{ if (paramValue == default) { throw new ArgumentDefaultException(paramName); } }

        public ArgumentDefaultException()
             : base("Value cannot be default.") { }

        public ArgumentDefaultException(string paramName)
            : base($"Value cannot be default. (Parameter '{paramName}')", paramName) { }

        public ArgumentDefaultException(string message, Exception innerException)
            : base(message, innerException) { }

        public ArgumentDefaultException(string paramName, string message)
            : base(message, paramName) { }

        [System.Security.SecurityCritical]
        protected ArgumentDefaultException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
