﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace CommonCore.CommandLine.AutoBuild.Factories
{
    public abstract class AServiceProvider<TService> : IServiceProvider<TService>
    {
        protected AServiceProvider() { }

        protected AServiceProvider(IDictionary<Type, TService> dict)
            => this.Dict = dict;

        protected IDictionary<Type, TService> Dict { get; }

        public TService this[Type key] { get => this.Get(key); set => this.Set(key, value); }

        public TService this[int index] => this.Get(this.Dict.Keys.ElementAt(index));

        public int Count => this.Dict.Count;

        public IReadOnlyCollection<Type> Keys => (IReadOnlyCollection<Type>)this.Dict.Keys;

        public IReadOnlyCollection<TService> Values => (IReadOnlyCollection<TService>)this.Dict.Values;

        public bool ContainsKey<TKey>() => this.ContainsKey(typeof(TKey));

        public bool ContainsKey(Type key) => this.Dict.ContainsKey(key);

        public TService Get<TKey>() => this.Get(typeof(TKey));

        public TService Get(Type key) => this.Dict[key];

        public void Remove<TKey>() => this.Remove(typeof(TKey));

        public void Remove(Type key) => this.Dict.Remove(key);

        public void Set<TKey>(TService service) => this.Set(typeof(TKey), service);

        public void Set(Type key, TService service) => this.Dict[key] = service;

        public bool TryGetValue<TKey>(out TService service) => this.TryGetValue(typeof(TKey), out service);

        public bool TryGetValue(Type key, out TService service) => this.Dict.TryGetValue(key, out service);

        public IEnumerator<Type> GetEnumerator() => this.Keys.GetEnumerator();

        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)this.Keys).GetEnumerator();

        [ExcludeFromCodeCoverage]
        object IServiceProvider.GetService(Type key) => this.Get(key);
    }
}
