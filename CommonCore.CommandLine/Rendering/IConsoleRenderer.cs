﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.CommandLine;
using CommonCore.CommandLine.IO;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering
{
    public interface IConsoleRenderer
    {
        Region Region { get; }

        ERenderMode RenderMode { get; }

        TextSpanFormatter Formatter { get; }

        IConsole Console { get; }

        ITerminal Terminal { get; }

        void Render(ISpan span, Region region = default);

        Size Measure(ISpan renderer, Size maxSize);
    }
}
