﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.AutoBuild.Extensions;
using CommonCore.CommandLine.Exceptions;

namespace CommonCore.CommandLine.AutoBuild
{
    public class AutoBuilder
    {
        public static Parser CreateParser(Assembly assembly)
            => CreateParser(AutoBuilder.FindEntryMethod(assembly));

        public static Parser CreateParser(MethodInfo method)
        {
            var info = new CommandInfo(method, RootCommand.ExecutableName, false, false);
            var command = info.ToRootCommand();
            var builder = Commander.Create(command)
                .ApplyDefaults()
                .InitDefaults()
                .RegisterDefaults()
                ;

            return builder.Build();
        }

        public static async Task<int> ExecuteAsync(Assembly assembly, string[] args)
        {
            Thrower.If.IsNull(assembly, nameof(assembly)).Throw();
            return await CreateParser(assembly).InvokeAsync(args ?? Array.Empty<string>(), null);
        }

        public static int Execute(Assembly assembly, string[] args)
        {
            Thrower.If.IsNull(assembly, nameof(assembly)).Throw();
            return CreateParser(assembly).Invoke(args ?? Array.Empty<string>(), null);
        }

        public static MethodInfo[] FindEntryMethods(Assembly assembly)
            => (from type in assembly.DefinedTypes
                where type.IsClass
                where !type.HasCustomAttribute<CompilerGeneratedAttribute>()
                from method in type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                where method.HasCustomAttribute<CommandEntryPointAttribute>()
                select method)
                .ToArray();

        public static MethodInfo FindEntryMethod(Assembly assembly)
        {
            var methods = FindEntryMethods(assembly);

            if (methods.Length == 0)
            { throw new MethodAccessException($"No entry point command method found. Ensure that one command method has the CommandEntryPoint attribute."); }
            else if (methods.Length > 1)
            { throw new MethodAccessException($"Multiple entry point command methods found. Ensure that only one command method has the CommandEntryPoint attribute."); }
            else if (!methods[0].IsStatic)
            { throw new MethodAccessException($"Entry point command method '{methods[0].Name}' on type '{methods[0].DeclaringType}' must be static."); }
            else if (methods[0].ReturnType != typeof(void)
                && methods[0].ReturnType != typeof(int)
                && methods[0].ReturnType != typeof(Task)
                && methods[0].ReturnType != typeof(Task<int>))
            { throw new MethodAccessException($"Entry point command method '{methods[0].Name}' on type '{methods[0].DeclaringType}' return type '{methods[0].ReturnType}' MUST be declared 'void', 'int', 'Task', or 'Task<int>'."); }
            else { return methods[0]; }
        }
    }
}
