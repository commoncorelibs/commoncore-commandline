﻿# Command Validators

Validators allow argument and option values to be analyzed after parsing, but before the command method is invoked. With them, values can be rejected and an error message provided to the user explaining the problem. Validators do not allow changing of the value, only implicitly accepting it by not returning an error message or explicitly rejecting it by providing a message.

Validators are declared using the `ValidatorReference` attribute, which takes a type and a method name pointing to the static method that should be called. If the type is not specified, then the declaring type of the marked command method will be used. Multiple validator references can be declared and they will be called in declaration order. Validator methods must be `static`, return `string`, and accept exactly one parameter of type `CommandResult`.

```csharp
/// <summary>
/// This is an example application showing of some features of <c>CommonCore.CommandLine</c>.
/// </summary>
/// <param name="console">Magic parameter. Ignored by help generation.</param>
/// <param name="word">What is the word? By default, bird is the word.</param>
/// <param name="numbers">Space-separated list of integer numbers.</param>
/// <param name="output">Write output to the given path instead of stdout.</param>
/// <param name="verbose">Print additional information.</param>
[CommandEntryPoint]
[ValidatorReference(nameof(ValidateWord)]
static void Main(IConsole console,
    [Argument] string word = "bird", [Argument] int[] numbers = null,
    FileInfo output = null, bool verbose = false)
{
    console.Out.WriteLine("Hello World!!");
    console.Out.WriteLine($"{nameof(word)}:    {word ?? "null"}");
    console.Out.WriteLine($"{nameof(numbers)}: {(numbers is null ? "null" : string.Join(", ", numbers))}");
    console.Out.WriteLine($"{nameof(output)}: {output?.FullName ?? "null"}");
    console.Out.WriteLine($"{nameof(verbose)}: {verbose}");
}

static string ValidateWord(CommandResult result)
{
    var word = result.GetArgumentValueOrDefault<string>("word");
    return word == "bird" ? null : $"Invalid input: Have you not heard? The word is 'bird', not '{word}'.";
}
```
