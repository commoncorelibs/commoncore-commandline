﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.ComponentModel;
using System.Linq;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Exceptions;
using CommonCore.CommandLine.Rendering;
using CommonCore.CommandLine.Rendering.Screens;
using CommonCore.CommandLine.Rendering.Views;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Demo.Test
{
    [EnableDefaultExceptionReporting]
    [EnableDefaultErrorReporting]
    [EnableDefaultGlobalOptionHelp]
    [EnableDefaultLocalOptionLicense]
    [EnableDefaultLocalOptionVersion]
    [AppMITLicense]
    static class Program
    {
        /// <summary>
        /// This is a <c>demo</c> of a <b>dotnet</b> console <b>tool</b> that uses <i>CommonCore.CommandLine</i> wrapper around <em>System.CommandLine</em>.
        /// <para>
        /// The <c>CommonCore.CommandLine</c> library was used to generate this application from the types, parameters, and documentation comments of the <c>Program.Main()</c> method.
        /// </para>
        /// <code>
        /// Some code with
        ///     indented lines
        ///     and <c>xml</c>.
        /// </code>
        /// </summary>
        /// <remarks>Here is where I say some additional things about the program.</remarks>
        /// <returns>The output of the program.</returns>
        [CommandReference(nameof(Help))]
        [CommandReference(nameof(License))]
        [CommandReference(nameof(Version))]
        [CommandEntryPoint]
        static void Main(IConsole console)
        {
            ////ErrorScreen.Instance.Show(result);
            ////HelpScreen.Instance.Show(result);
            ////VersionScreen.Instance.Show();
            //console.Out.WriteLine("Goodbye World!!");
            //HelpScreen.Instance.Show(result);


            //var diagram = "[ demo-auto ![ --things <blah> ] [ word <bird> ] [ numbers <> ] *[ --output <> ] *[ --verbose <False> ] ]";
            //var pad = "0123456789";
            //int padCount = diagram.Length / 10;
            //console.Out.Write(string.Concat(Enumerable.Repeat(pad, padCount)) + pad.Substring(0, diagram.Length % pad.Length));
            //console.Out.WriteLine(diagram);

            //var display = new string('_', diagram.Length).ToCharArray().AsSpan();
            //foreach (var index in list) { display[index] = '^'; }
            //Console.WriteLine(display.ToString());




            //var result = Thrower.If.IsNull(null, "test").Throw();





            //var texts = Viewer.First
            //    .Add("first")
            //    .If(true)
            //        .Add("second")
            //    .End
            //    .Add("third")
            //    .Build();

            //Console.WriteLine(string.Join('\n', texts));

            //var emp = EmployeeBuilderDirector.NewEmployee
            //    .SetName("Maks")
            //    .AtPosition("Software Developer")
            //    .WithSalary(3500)
            //    .Build();

            //Console.WriteLine(emp);

            //for (int i = -1; i < 2; i++)
            //{
            //    var texts = B.First
            //        .Add("first")
            //        .If(i == 0)
            //            .Add($"second: {i} zero")
            //        .Else().If(i < 0)
            //            .Add($"second: {i} neg")
            //        .Else()
            //            .Add($"second: {i} pos")
            //        .End()
            //        .Add("third")
            //        .Build();

            //    Console.WriteLine(string.Join('\n', texts));
            //}
        }


        /// <summary>
        /// Print the program help information.
        /// </summary>
        static void Help(ParseResult result) => HelpScreen.Instance.Show(result, result.RootCommandResult.Command);

        /// <summary>
        /// Print the program license information.
        /// </summary>
        static void License() => LicenseScreen.Instance.Show();

        /// <summary>
        /// Print the program version information.
        /// </summary>
        static void Version() => VersionScreen.Instance.Show();
    }

    //public interface IBuilder
    //{
    //    IEnumerable<string> Build();
    //}

    //public interface IBasicBuilder<TBuilder>
    //{
    //    TBuilder Add(string text);

    //    IIfBuilder<TBuilder> If(bool condition);
    //}

    //public interface IIfBuilder<TBuilder> : IBasicBuilder<TBuilder>
    //{
    //    IElseBuilder<TBuilder> Else();

    //    TBuilder End();
    //}

    //public interface IElseBuilder<TBuilder> : IBasicBuilder<TBuilder>
    //{
    //    TBuilder End();
    //}

    //public abstract class AB
    //{
    //    public abstract IEnumerable<string> Build();
    //}

    //public abstract class AB<TB> : AB where TB : AB<TB>
    //{
    //}

    //public class B : AB<B>
    //{
    //    public static B First => new B();

    //    protected readonly List<string> _texts = new List<string>();

    //    public B Add(string text) { this._texts.Add(text); return this; }

    //    public CB<B> If(bool condition) => new CB<B>(this, condition);

    //    public override IEnumerable<string> Build() => this._texts;
    //}

    //public class SB<TP> : B
    //    where TP : AB<TP>
    //{
    //    public SB(TP parent)
    //    {
    //        this._parent = parent;
    //    }

    //    private TP _parent;

    //    public override IEnumerable<string> Build()
    //    {
    //        foreach (var text in this._parent.Build())
    //        { yield return text; }
    //        foreach (var text in this._texts)
    //        { yield return text; }
    //    }
    //}

    //public class CB<TP> : AB<CB<TP>>
    //    where TP : AB<TP>
    //{
    //    public CB(TP parent, bool condition)
    //    {
    //        this._parent = parent;
    //        this._condition = condition;
    //    }

    //    private TP _parent;

    //    private bool _condition = false;

    //    private readonly List<string> _texts = new List<string>();

    //    public CB<TP> Add(string text) { this._texts.Add(text); return this; }

    //    public CB<CB<TP>> If(bool condition) => new CB<CB<TP>>(this, condition);

    //    public CB<CB<TP>> ElseIf(bool condition) => new CB<CB<TP>>(this, !this._condition && condition);

    //    public CB<CB<TP>> Else() => new CB<CB<TP>>(this, !this._condition);

    //    public SB<CB<TP>> End() => new SB<CB<TP>>(this);

    //    public override IEnumerable<string> Build()
    //    {
    //        foreach (var text in this._parent.Build())
    //        { yield return text; }
    //        if (this._condition)
    //        {
    //            foreach (var text in this._texts)
    //            { yield return text; }
    //        }
    //    }
    //}

    //public class Employee
    //{
    //    public string Name { get; set; }
    //    public string Position { get; set; }
    //    public double Salary { get; set; }

    //    public override string ToString() => $"Name: {Name}, Position: {Position}, Salary: {Salary}";
    //}

    //public class EmployeeBuilderDirector : EmployeeSalaryBuilder<EmployeeBuilderDirector>
    //{
    //    public static EmployeeBuilderDirector NewEmployee => new EmployeeBuilderDirector();
    //}

    //public abstract class EmployeeBuilder
    //{
    //    protected Employee employee;

    //    public EmployeeBuilder()
    //    {
    //        employee = new Employee();
    //    }

    //    public Employee Build() => employee;
    //}

    //public class EmployeeInfoBuilder<T> : EmployeeBuilder where T : EmployeeInfoBuilder<T>
    //{
    //    public T SetName(string name)
    //    {
    //        employee.Name = name;
    //        return (T)this;
    //    }
    //}

    //public class EmployeePositionBuilder<T> : EmployeeInfoBuilder<EmployeePositionBuilder<T>> where T : EmployeePositionBuilder<T>
    //{
    //    public T AtPosition(string position)
    //    {
    //        employee.Position = position;
    //        return (T)this;
    //    }
    //}

    //public class EmployeeSalaryBuilder<T> : EmployeePositionBuilder<EmployeeSalaryBuilder<T>> where T : EmployeeSalaryBuilder<T>
    //{
    //    public T WithSalary(double salary)
    //    {
    //        employee.Salary = salary;
    //        return (T)this;
    //    }
    //}

    //public abstract class ABuilder<TBuilder>
    //    where TBuilder : ABuilder<TBuilder>
    //{
    //    public abstract TBuilder Add(string text);

    //    public abstract IEnumerable<string> Build();

    //    public ISpan Create() => new ContentSpan(string.Concat(this.Build()));
    //}

    //public class Viewer : ABuilder<Viewer>
    //{
    //    public static Viewer First => new Viewer();

    //    protected List<string> Texts { get; } = new List<string>();

    //    public override Viewer Add(string text) { this.Texts.Add(text); return this; }

    //    public virtual ConditionalViewer If(bool condition)
    //    {
    //        return new ConditionalViewer(this, condition);
    //    }

    //    public override IEnumerable<string> Build() { return this.Texts; }
    //}

    //public class ConditionalViewer : ABuilder<ConditionalViewer>
    //{
    //    public ConditionalViewer(Viewer parent, bool condition)
    //    {
    //        this.End = parent;
    //        this.Condition = condition;
    //    }

    //    protected List<string> Texts { get; } = new List<string>();

    //    public Viewer Parent { get; }

    //    public Viewer End { get; }

    //    protected bool Condition { get; }

    //    public override ConditionalViewer Add(string text) { this.Texts.Add(text); return this; }

    //    public override IEnumerable<string> Build()
    //    {
    //        foreach (var text in this.End.Build()) { yield return text; }
    //        if (this.Condition)
    //        { foreach (var text in this.Texts) { yield return text; } }
    //    }
    //}

    //public class Spanner : ABuilder<ISpan, Spanner>
    //{
    //}




    ///// <summary>
    /////
    ///// <para>Example Bullet Definition List with Header:</para>
    ///// <list type="bullet">
    /////     <listheader>
    /////         <term>Terms</term>
    /////         <description>Descriptions</description>
    /////     </listheader>
    /////     <item><term>Term 0</term><description>Description 0</description></item>
    /////     <item><term>Term 1</term><description>Description 1</description></item>
    ///// </list>
    ///// <para>Example Bullet Definition List:</para>
    ///// <list type="bullet">
    /////     <item><term>Term 0</term><description>Description 0</description></item>
    /////     <item><term>Term 1</term><description>Description 1</description></item>
    ///// </list>
    /////
    ///// <para>Example Number Definition List with Header:</para>
    ///// <list type="number">
    /////     <listheader>
    /////         <term>Terms</term>
    /////         <description>Descriptions</description>
    /////     </listheader>
    /////     <item><term>Term 0</term><description>Description 0</description></item>
    /////     <item><term>Term 1</term><description>Description 1</description></item>
    ///// </list>
    ///// <para>Example Number Definition List:</para>
    ///// <list type="number">
    /////     <item><term>Term 0</term><description>Description 0</description></item>
    /////     <item><term>Term 1</term><description>Description 1</description></item>
    ///// </list>
    /////
    ///// <para>Example Bullet List with Header:</para>
    ///// <list type="bullet">
    /////     <listheader>
    /////         <term>Bullet Term</term>
    /////         <description>Bullet Description</description>
    /////     </listheader>
    /////     <item><description>Item 0</description></item>
    /////     <item><description>Item 1</description></item>
    ///// </list>
    ///// <para>Example Bullet List:</para>
    ///// <list type="bullet">
    /////     <item><description>Item 0</description></item>
    /////     <item><description>Item 1</description></item>
    ///// </list>
    /////
    ///// <para>Example Number List with Header:</para>
    ///// <list type="number">
    /////     <listheader>
    /////         <term>Number Term</term>
    /////         <description>Number Description</description>
    /////     </listheader>
    /////     <item><description>Item 0</description></item>
    /////     <item><description>Item 1</description></item>
    ///// </list>
    ///// <para>Example Number List:</para>
    ///// <list type="number">
    /////     <item><description>Item 0</description></item>
    /////     <item><description>Item 1</description></item>
    ///// </list>
    /////
    ///// <para>Example Table List:</para>
    ///// <list type="table">
    /////     <listheader>
    /////         <term>Col 0</term>
    /////         <term>Col 1</term>
    /////         <term>Col 2</term>
    /////     </listheader>
    /////     <item>
    /////         <description>Col 0, Row 0</description>
    /////         <description>Col 1, Row 0</description>
    /////         <description>Col 2, Row 0</description>
    /////     </item>
    /////     <item>
    /////         <description>Col 0, Row 1</description>
    /////         <description>Col 1, Row 1</description>
    /////         <description>Col 2, Row 1</description>
    /////     </item>
    ///// </list>
    ///// </summary>
    ///// <param name="command"></param>
}
