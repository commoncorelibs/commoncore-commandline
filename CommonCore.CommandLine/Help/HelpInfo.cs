﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using CommonCore.CommandLine.Rendering.Views;

namespace CommonCore.CommandLine.Help
{
    public class HelpInfo
    {
        public HelpInfo(string name = null) => this.Name = name;

        public string Name { get; set; }

        public List<IView> SummaryViews { get; set; }

        public List<IView> RemarksViews { get; set; }

        public List<IView> ReturnsViews { get; set; }

        public List<(string Name, List<IView> Views)> ParamViewPairs { get; set; }

        public List<(string Name, List<IView> Views)> TypeParamViewPairs { get; set; }

        public List<(string Name, List<IView> Views)> SeeAlsoViewPairs { get; set; }

        public List<(string Name, List<IView> Views)> ExceptionViewPairs { get; set; }

        public List<(string Name, List<IView> Views)> PermissionViewPairs { get; set; }

        //public Dictionary<string, string> ParameterDescriptions { get; } = new Dictionary<string, string>();
    }
}
