# Release v1.0.0-pre.6

* License and version are no longer global options #21
* Default title now aware of AppTitleAttribute.Company #14
* Fixed some typos #19, #15
