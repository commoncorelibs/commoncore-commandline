﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Binding;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.Linq;
using System.Reflection;
using System.Threading;
using CommonCore.CommandLine.AutoBuild.Attributes;

namespace CommonCore.CommandLine
{
    public static class CCUtils
    {
        public static bool HasAttribute<TAttribute>(ParameterInfo self)
            where TAttribute : Attribute
            => self.GetCustomAttribute<TAttribute>() != null;

        public static object GetDefaultValue(ParameterInfo self)
            => self.DefaultValue is DBNull ? GetDefaultValue(self.ParameterType) : self.DefaultValue;

        public static object GetDefaultValue(Type type)
            => type.IsValueType ? Activator.CreateInstance(type) : null;

        public static string[] BuildOptionAliases(ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            var name = parameter.Name.ToKebabCase();
            return name.Length == 1
                ? (new[] { $"-{char.ToLowerInvariant(name[0])}" })
                : (new[] { $"--{name}", $"-{char.ToLowerInvariant(name[0])}" });
        }

        public static Argument BuildArgument(ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            var name = parameter.Name.ToKebabCase();
            var argument = new Argument()
            {
                ArgumentType = parameter.ParameterType,
                Name = name,
                IsHidden = IsHidden(parameter)
            };
            argument.AddAlias(name);
            if (parameter.HasDefaultValue) { argument.SetDefaultValueFactory(() => GetDefaultValue(parameter)); }
            if (parameter.ParameterType.IsEnum)
            { argument.FromAmong(Enum.GetNames(parameter.ParameterType)); }
            return argument;
        }

        public static Option BuildOption(ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            var argument = new Argument() { ArgumentType = parameter.ParameterType };
            var option = new Option(BuildOptionAliases(parameter))
            { Name = parameter.Name.ToKebabCase(), Argument = argument, Required = IsRequired(parameter), IsHidden = IsHidden(parameter) };
            if (parameter.HasDefaultValue) { argument.SetDefaultValueFactory(() => GetDefaultValue(parameter)); }
            if (parameter.ParameterType.IsEnum)
            { option.FromAmong(Enum.GetNames(parameter.ParameterType)); }
            return option;
        }

        public static bool IsRequired(ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            return !parameter.HasDefaultValue || HasAttribute<RequiredAttribute>(parameter);
        }

        public static bool IsHidden(ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            return HasAttribute<HiddenAttribute>(parameter);
        }

        public static bool IsArgument(ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            return HasAttribute<ArgumentAttribute>(parameter);
        }

        public static bool IsOption(ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            return !HasAttribute<ArgumentAttribute>(parameter);
        }

        public static List<Type> OmittedTypes { get; } = new List<Type>()
        {
            typeof(IConsole),
            typeof(InvocationContext),
            typeof(BindingContext),
            typeof(ParseResult),
            typeof(CancellationToken),
            typeof(System.CommandLine.Help.IHelpBuilder),
            typeof(Commander),
            typeof(Services),
        };
        public static IEnumerable<ParameterInfo> FilterAutoParameters(MethodInfo method)
        {
            if (method is null) { throw new ArgumentNullException(nameof(method)); }
            return method.GetParameters()
                           // TODO: Should probably exclude types derived from those
                           // registered with services.
                           .Where(p => !CCUtils.OmittedTypes.Contains(p.ParameterType))
                           .Where(p => !Services.Instance.Keys.Contains(p.ParameterType));
        }

        public static readonly string[] OptionPrefixStrings = { "--", "-", "/" };
        public static (string prefix, string alias) SplitPrefix(string rawAlias)
        {
            for (var index = 0; index < CCUtils.OptionPrefixStrings.Length; index++)
            {
                var prefix = CCUtils.OptionPrefixStrings[index];
                if (rawAlias.StartsWith(prefix))
                { return (prefix, rawAlias.Substring(prefix.Length)); }
            }
            return (null, rawAlias);
        }

        public static string RemovePrefix(this string rawAlias)
        {
            for (var i = 0; i < CCUtils.OptionPrefixStrings.Length; i++)
            {
                var prefix = CCUtils.OptionPrefixStrings[i];
                if (rawAlias.StartsWith(prefix))
                { return rawAlias.Substring(prefix.Length); }
            }
            return rawAlias;
        }
    }
}
