﻿# Enable Default Output

The library is quiet by default and doesn't output anything to the console. This is unlike most other command line libraries that seem to take great pains to integrate help output with the rest of their libraries. This decision was made for two primary reasons. One, basic separation of concerns says that displaying help is not the responsibility of a parsing engine. Two, not everyone wants the default mechanisms; maybe they don't need any help or other display, or maybe they simply don't like the format used to display the information. Regardless, the reason doesn't matter, the library doesn't force use of its features.

To enable the default output mechanisms, apply the appropriate attributes to the command entry point method's declaring class.

The `EnableDefaultExceptionReporting` attribute indicates any encountered exceptions should be output to the console. This is useful in release code to avoid the application exiting ungracefully. By catching and displaying the exceptions, the application is able to gracefully exit.

The `EnableDefaultErrorReporting` attribute indicates that the default error reporting screen should be used to display any parsing errors to the user. This screen is very similar to the default help screen, but with encountered parsing errors listed.

The `EnableDefaultGlobalOptionHelp` attribute indicates that the **--help** global option should be added to the system. The default help screen will be used for display. Optionally, the default global option can be hidden from inclusion in default screens by specifying `true` as the attribute's only parameter. The option only accepts the default double-dash long form alias and there is currently no mechanism to control this behavior.

The `EnableDefaultLocalOptionLicense` attribute indicates that the **--license** local option should be added to the root command. The default license screen will be used for display. Optionally, the default local option can be hidden from inclusion in default screens by specifying `true` as the attribute's only parameter. The option only accepts the default double-dash long form alias and there is currently no mechanism to control this behavior.

The `EnableDefaultLocalOptionVersion` attribute indicates that the **--version** local option should be added to the root command. The default license screen will be used for display. Optionally, the default local option can be hidden from inclusion in default screens by specifying `true` as the attribute's only parameter. The option only accepts the default double-dash long form alias and there is currently no mechanism to control this behavior.

It is strongly recommended that all five default features are enabled.

```csharp
[EnableDefaultExceptionReporting]
[EnableDefaultErrorReporting]
[EnableDefaultGlobalOptionHelp]
[EnableDefaultLocalOptionLicense]
[EnableDefaultLocalOptionVersion]
[AppMITLicense]
static class Program
{
    /// <summary>
    /// This is an example Hello World application
    /// using the <c>CommonCore.CommandLine</c> library.
    /// </summary>
    [CommandEntryPoint]
    static void Main()
    {
        Console.WriteLine("Hello World!");
    }
}
```
