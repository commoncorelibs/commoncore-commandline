﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerDisplay("{Width},{Height}")]
    public readonly struct Size : IEquatable<Size>
    {
        public static Size Zero { get; } = default;

        public static Size Max { get; } = new Size(int.MaxValue, int.MaxValue);

        public Size(int width, int height)
        {
            if (width < 0) { throw new ArgumentOutOfRangeException(nameof(width)); }
            if (height < 0) { throw new ArgumentOutOfRangeException(nameof(height)); }

            this.Width = width;
            this.Height = height;
        }

        public readonly int Width { get; }

        public readonly int Height { get; }

        public readonly override string ToString()
            => $"[{this.Width}, {this.Height}]";

        public readonly override int GetHashCode()
            => HashCode.Combine(this.Width, this.Height);

        public readonly override bool Equals(object obj)
            => obj is Size size && this.Equals(size);

        public readonly bool Equals(Size other)
            => this.Width == other.Width && this.Height == other.Height;

        public static bool operator ==(Size left, Size right) => left.Equals(right);

        public static bool operator !=(Size left, Size right) => !left.Equals(right);

        public static Point operator +(Size left, Size right) => new Point(left.Width + right.Width, left.Height + right.Height);

        public static Point operator -(Size left, Size right) => new Point(left.Width - right.Width, left.Height - right.Height);
    }
}
