﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine.Parsing;
using System.Reflection;
using CommonCore.CommandLine.Exceptions;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="ParseAttribute"/> <c>class</c>
    /// provides support for directly parsing the input of arguments (but not options)
    /// into their output form.
    /// 
    /// <para>
    /// The attribute takes a type and a method name that points to the method
    /// representing the initializer. If the type is not specified, then the declaring
    /// type of the marked method will be used.
    /// </para>
    /// 
    /// <para>
    /// Parse methods must be <c>static</c>, have the same return type as the parameter,
    /// and accept exactly one parameter of type <see cref="ArgumentResult"/>.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class ParseAttribute : Attribute
    {
        public ParseAttribute(string methodName)
            : this(null, methodName) { }

        public ParseAttribute(Type declaringType, string methodName)
        {
            Thrower.If.IsEmpty(methodName, nameof(methodName)).Throw();

            this.DeclaringType = declaringType;
            this.MethodName = methodName;
        }

        /// <summary>
        /// Gets the type containing <see cref="MethodName"/> or <c>null</c>.
        /// </summary>
        public Type DeclaringType { get; }

        /// <summary>
        /// Gets the name of the referenced method.
        /// </summary>
        public string MethodName { get; }

        /// <summary>
        /// Gets the <see cref="MethodInfo"/> from the <see cref="DeclaringType"/> and <see cref="MethodName"/>
        /// members. If <see cref="DeclaringType"/> is <c>null</c>, then <paramref name="fallbackType"/> is
        /// used instead; if both are null, then an exception is thrown.
        /// </summary>
        /// <param name="returnType">The <see cref="Type"/> that must be returned by the method.</param>
        /// <param name="fallbackType">The <see cref="Type"/> to use if <see cref="DeclaringType"/> is <c>null</c>.</param>
        /// <returns>The <see cref="MethodInfo"/> represented by the attribute instance.</returns>
        /// <exception cref="ArgumentNullException">If <see cref="DeclaringType"/> and <paramref name="fallbackType"/> are both <c>null</c>.</exception>
        public MethodInfo GetMethod(Type returnType, Type fallbackType)
        {
            var type = this.DeclaringType ?? fallbackType ?? throw new ArgumentNullException(nameof(fallbackType));
            var name = this.MethodName;
            var method = ReflectionHelper.GetMethodInfo(type, name);

            if (method is null)
            { throw new MethodAccessException($"Parse method '{name}' on type '{type}' could not be found."); }

            if (!method.IsStatic)
            { throw new MethodAccessException($"Parse method '{name}' on type '{type}' MUST be static."); }

            if (method.ReturnType != returnType)
            { throw new MethodAccessException($"Parse method '{name}' on type '{type}' MUST have a return type of '{returnType}'."); }

            var parameters = method.GetParameters();
            if (parameters.Length != 1 || parameters[0].ParameterType != typeof(ArgumentResult))
            { throw new MethodAccessException($"Parse method '{name}' on type '{type}' MUST accept only one parameter of type {typeof(ArgumentResult)}."); }
            return method;
        }
    }
}
