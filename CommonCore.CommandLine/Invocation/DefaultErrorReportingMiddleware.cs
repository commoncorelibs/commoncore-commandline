﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine.Invocation;
using System.Threading.Tasks;
using CommonCore.CommandLine.Rendering.Screens;

namespace CommonCore.CommandLine.Invocation
{
    public class DefaultErrorReportingMiddleware : Middleware
    {
        public DefaultErrorReportingMiddleware()
            : base(EMiddlewareOrder.ErrorReporting, Middleware_ErrorReporting) { }

        private static async Task Middleware_ErrorReporting(InvocationContext context,
            Func<InvocationContext, Task> next)
        {
            if (context.ParseResult.Errors.Count == 0) { await next(context); return; }
            ErrorScreen.Instance.Show(context.ParseResult);
            context.ResultCode = 1;
        }
    }
}
