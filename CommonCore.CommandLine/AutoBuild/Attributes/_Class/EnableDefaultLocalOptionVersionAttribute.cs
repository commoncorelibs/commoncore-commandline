﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using CommonCore.CommandLine.Rendering.Screens;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="EnableDefaultLocalOptionVersionAttribute"/> <c>class</c>
    /// provides a way to signal the system to enable the default local option,
    /// <c>--version</c> on the root command.
    /// The option will use the default <see cref="VersionScreen"/> for display.
    /// Optionally, the default global option can be hidden from inclusion in default
    /// screens by specifying <c>true</c> as the attributes only parameter.
    /// 
    /// <para>
    /// The option only accepts the default double-dash long form alias and
    /// there is currently no mechanism to control this behavior.
    /// </para>
    /// 
    /// <para>
    /// The attribute should be applied to the implementing console application's main <c>class</c>,
    /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> <c>method</c>.
    /// This is generally the <c>Program</c> class.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class EnableDefaultLocalOptionVersionAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnableDefaultLocalOptionVersionAttribute"/> <c>class</c>.
        /// </summary>
        /// <param name="hidden">Indicates whether the option should be hidden when displaying information about commands.</param>
        public EnableDefaultLocalOptionVersionAttribute(bool hidden = false)
            => this.Hidden = hidden;

        /// <summary>
        /// Gets a value indicating whether the options should be hidden when displaying information about commands.
        /// </summary>
        public bool Hidden { get; }
    }
}
