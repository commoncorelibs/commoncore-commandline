﻿# Command Initializers

Initializers allow application specific code to be run on each command after it and any subcommands are created. Initializer methods are called from the bottom of the command tree to the top. Meaning subcommand initializers are called before the initializer of their parent commands. If multiple initializer references are provided, then they are called in declaration order. Initializers are provided to address edge cases that can't be accomplished with other means, such as adding a global option unique to the application. In most cases, initializers shouldn't be needed.

Initializers are declared using the `InitializerReference` attribute, which takes a type and a method name pointing to the static method that should be called. If the type is not specified, then the declaring type of the marked command method will be used. Initializer methods must be `static`, return `void`, and accept exactly one parameter of type `CoreCommand`.

```csharp
/// <summary>
/// This is an example application showing of some features of <c>CommonCore.CommandLine</c>.
/// </summary>
/// <param name="console">Magic parameter. Ignored by help generation.</param>
/// <param name="word">What is the word? By default, bird is the word.</param>
/// <param name="numbers">Space-separated list of integer numbers.</param>
/// <param name="output">Write output to the given path instead of stdout.</param>
/// <param name="verbose">Print additional information.</param>
[CommandEntryPoint]
[InitializerReference(nameof(Initialize)]
static void Main(IConsole console,
    [Argument] string word = "bird", [Argument] int[] numbers = null,
    FileInfo output = null, bool verbose = false)
{
    console.Out.WriteLine("Hello World!!");
    console.Out.WriteLine($"{nameof(word)}:    {word ?? "null"}");
    console.Out.WriteLine($"{nameof(numbers)}: {(numbers is null ? "null" : string.Join(", ", numbers))}");
    console.Out.WriteLine($"{nameof(output)}: {output?.FullName ?? "null"}");
    console.Out.WriteLine($"{nameof(verbose)}: {verbose}");
}

static void Initialize(CoreCommand command)
{
    // Do something with the command.
}
```
