﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Help;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.Linq;
using System.Reflection;
using System.Text;
using CommonCore.CommandLine.Help;
using CommonCore.CommandLine.Reflection;
using CommonCore.CommandLine.Symbols;

namespace CommonCore.CommandLine.Extensions
{
    public static class CommandExtensions
    {
        public static void AddArgument(this Command command, params Argument[] arguments)
        { foreach (var argument in arguments) { command.AddArgument(argument); } }

        public static void AddArgument(this Command command, IEnumerable<Argument> arguments)
        { foreach (var argument in arguments) { command.AddArgument(argument); } }

        public static void AddOption(this Command command, params Option[] options)
        { foreach (var option in options) { command.AddOption(option); } }

        public static void AddOption(this Command command, IEnumerable<Option> options)
        { foreach (var option in options) { command.AddOption(option); } }

        public static void AddCommand(this Command self, params Command[] commands)
        { foreach (var command in commands) { self.AddCommand(command); } }

        public static void AddCommand(this Command self, IEnumerable<Command> commands)
        { foreach (var command in commands) { self.AddCommand(command); } }

        public static void AddValidator(this Command self, params ValidateSymbol<CommandResult>[] validators)
        { foreach (var validator in validators) { self.AddValidator(r => validator(r)); } }

        public static void AddValidator(this Command self, IEnumerable<ValidateSymbol<CommandResult>> validators)
        { foreach (var validator in validators) { self.AddValidator(r => validator(r)); } }

        //public static void AddExclusiveOption(this CoreCommand command, Option option, EMiddlewareOrder order = EMiddlewareOrder.LateExclusiveOption, int orderOffset = 0)
        //{
        //    command.AddOption(option);
        //    command.Middlewares.Add(new Middleware(order, orderOffset,
        //        async (context, next) =>
        //        {
        //            if (!context.ParseResult.HasOption(option)) { await next(context); return; }
        //            //HelpScreen.Instance.Show(context.ParseResult);
        //        }));
        //}

        public static string ToUsage(this ICommand command)
        {
            var usage = new List<string>();
            foreach (var ancestor in command.GetAncestry<ICommand>().Reverse())
            {
                usage.Add(ancestor.Name);
                if (ancestor != command)
                { usage.Add(FormatArgumentUsage(ancestor.Arguments)); }
            }

            var hasOption = command.Options.Any(o => !o.IsHidden);
            if (hasOption) { usage.Add(DefaultHelpText.Usage.Options); }

            usage.Add(FormatArgumentUsage(command.Arguments));

            var hasCommand = command.Children.OfType<ICommand>().Any(c => !c.IsHidden);
            if (hasCommand) { usage.Add(DefaultHelpText.Usage.Command); }

            if (!command.TreatUnmatchedTokensAsErrors)
            { usage.Add(DefaultHelpText.Usage.AdditionalArguments); }

            return string.Join(" ", usage.Where(u => !string.IsNullOrWhiteSpace(u)));
        }

        public static IEnumerable<string> ToUsages(this ICommand self)
        {
            string usage = self.ToUsage();
            if (usage != null) { yield return usage; }
            if (self is CoreCommand command)
            { foreach (var subusage in command.AdditonalUsages.Select(sub => sub.ToUsage())) { yield return subusage; } }
            //var subs = self.Children.OfType<ICommand>().Where(c => !c.IsHidden);
            //if (subs.Any())
            //{
            //    foreach (var sub in subs)
            //    {
            //        usage = self.ToUsage();
            //        if (usage != null) { yield return usage; }
            //    }
            //}
            //var usages = this.AppInfo.Usages;
            //if (usages?.Any(u => u.Blurbs != null && u.Blurbs.Length > 0) ?? false)
            //{
            //    this.Add("----------");
            //    var assembly = AssemblyHelper.GetAssembly().GetShortName();
            //    foreach (var usage in usages)
            //    {
            //        if (usage.Blurbs == null || usage.Blurbs.Length == 0) { continue; }

            //        this.Add(usage.Title != null, usage.Title);
            //        var alias = usage.Alias ?? assembly;
            //        this.Add((IView)new ListView<string>(usage.Blurbs.Select(b => alias + " " + b), s => s) { Indenting = 2 });
            //    }
            //}
        }

        public static string ToHelpInvocation(this ICommand command)
        {
            var invocation = command.Name;
            var descriptor = command.ToDescriptor();
            return descriptor is null ? invocation : $"{invocation} {descriptor}";
        }

        public static string ToInvocation(this ICommand command) => command.Name;

        public static string ToDescriptor(this ICommand command)
        {
            //return FormatArgumentUsage(command.Arguments);
            var items = new List<string>();

            var options = command.Options.Where(o => !o.IsHidden && o.Parents.Count <= 1);
            var optionCount = options.Count();
            if (optionCount > 0) { items.Add("opts(" + optionCount + ")"); }

            var arguments = command.Arguments.Where(a => !a.IsHidden);
            var argumentCount = arguments.Count();
            if (argumentCount > 0) { items.Add("args(" + argumentCount + ")"); }

            var commands = command.Children.OfType<ICommand>().Where(c => !c.IsHidden);
            var commandCount = commands.Count();
            if (commandCount > 0) { items.Add("cmds(" + commandCount + ")"); }

            return items.Count == 0 ? string.Empty : "[" + string.Join(", ", items) + "]";
        }

        public static string FormatArgumentUsage(IEnumerable<IArgument> arguments)
        {
            var args = arguments?.Where(a => !a.IsHidden).ToArray();
            if (args is null || !args.Any()) { return null; }

            var builder = new StringBuilder();
            var suffixes = new Stack<string>();

            for (var index = 0; index < args.Length; index++)
            {
                var arg = args[index];
                if (__isOptional(arg)) { builder.Append('['); suffixes.Push("]"); }

                var arityIndicator = arg.Arity.MaximumNumberOfValues > 1 ? " ..." : "";
                builder.Append($"<{arg.Name}>{arityIndicator}");
                if (index < args.Length - 1) { builder.Append(" "); }
            }

            while (suffixes.Count > 0) { builder.Append(suffixes.Pop()); }

            return builder.ToString();

            static bool __isOptional(IArgument argument) => argument.Parents.Count > 1 || argument.Arity.MinimumNumberOfValues == 0;
        }

        private static Dictionary<ICommand, HelpInfo> _helps = new Dictionary<ICommand, HelpInfo>();

        public static void SetHelpInfo(this ICommand self, HelpInfo help)
        {
            if (self is CoreCommand cc) { cc.HelpInfo = help; }
            else { _helps[self] = help; }
        }

        public static HelpInfo GetHelpInfo(this ICommand self)
            => self is CoreCommand cc ? cc.HelpInfo : _helps.TryGetValue(self, out HelpInfo help) ? help : null;
    }
}
