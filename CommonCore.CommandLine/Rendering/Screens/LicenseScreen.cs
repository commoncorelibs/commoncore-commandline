﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.CommandLine.Rendering.Screens
{
    /// <summary>
    /// The <see cref="LicenseScreen"/> <c>convienence</c> <c>class</c> provides an
    /// implementation of <see cref="Screen"/> populated with license information
    /// for the implementing console application.
    /// </summary>
    public class LicenseScreen : Screen
    {
        /// <summary>
        /// Gets the <c>singleton</c> <c>instance</c> of the <see cref="ErrorScreen"/> <c>class</c>.
        /// </summary>
        public static LicenseScreen Instance { get; set; } = new LicenseScreen();

        /// <summary>
        /// Build and render the contents of the screen.
        /// </summary>
        public void Show()
            => this
            .AddHeader()
            .AddCopyright()
            .AddLicenseSummary()
            .Add()
            .AddLicenseText(out bool written)
            .Add(written)
            .Render()
            ;
    }
}
