﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="EnableDefaultGlobalOptionsAttribute"/> <c>class</c>
    /// provides a way to signal the system to enable the default global options.
    /// The system currently supports three default global options:
    /// <c>--help</c>, <c>--license</c>, and <c>--version</c>.
    /// There is currently no way to enable or disable individual options,
    /// either all three are enabled or all three are disabled.
    /// Each option will use the default related screen for display.
    /// Optionally, the default global options can be hidden from inclusion in default
    /// screens by specifying <c>true</c> as the attributes only parameter.
    /// The options only accept the default double-dash long form alias and
    /// there is currently no mechanism to control this behavior.
    /// 
    /// <para>
    /// The attribute should be applied to the implementing console application's main <c>class</c>,
    /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> <c>method</c>.
    /// This is generally the <c>Program</c> class.
    /// </para>
    /// </summary>
    [Obsolete("Use 'EnableDefaultGlobalOptionHelpAttribute', 'EnableDefaultLocalOptionLicenseAttribute' and , 'EnableDefaultLocalOptionVersionAttribute' instead.", true)]
    [AttributeUsage(AttributeTargets.Class)]
    public class EnableDefaultGlobalOptionsAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnableDefaultGlobalOptionsAttribute"/> <c>class</c>.
        /// </summary>
        /// <param name="hidden">Indicates whether the options should be hidden when displaying information about commands.</param>
        public EnableDefaultGlobalOptionsAttribute(bool hidden = false)
            => this.Hidden = hidden;

        /// <summary>
        /// Gets a value indicating whether the options should be hidden when displaying information about commands.
        /// </summary>
        public bool Hidden { get; }
    }
}
