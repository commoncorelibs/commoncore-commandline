﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerDisplay("{X},{Y}")]
    public readonly struct Point : IEquatable<Point>
    {
        public static Point Zero { get; } = default;

        public Point(int x, int y)
        {
            if (x < 0) { throw new ArgumentOutOfRangeException(nameof(x)); }
            if (y < 0) { throw new ArgumentOutOfRangeException(nameof(y)); }

            this.X = x;
            this.Y = y;
        }

        public readonly int X { get; }

        public readonly int Y { get; }

        public readonly override string ToString()
            => $"[{this.X}, {this.Y}]";

        public readonly override int GetHashCode()
            => HashCode.Combine(this.X, this.Y);

        public readonly override bool Equals(object obj)
            => obj is Point point && this.Equals(point);

        public readonly bool Equals(Point other)
            => this.X == other.X && this.Y == other.Y;

        public static bool operator ==(Point left, Point right) => left.Equals(right);

        public static bool operator !=(Point left, Point right) => !left.Equals(right);

        public static Point operator +(Point left, Point right) => new Point(left.X + right.X, left.Y + right.Y);

        public static Point operator -(Point left, Point right) => new Point(left.X - right.X, left.Y - right.Y);

        public static Point operator +(Point left, Size right) => new Point(left.X + right.Width, left.Y + right.Height);

        public static Point operator -(Point left, Size right) => new Point(left.X - right.Width, left.Y - right.Height);
    }
}
