﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine.Invocation;
using System.Reflection;
using System.Threading.Tasks;
using CommonCore.CommandLine.Exceptions;
using CommonCore.CommandLine.Invocation;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="MiddlewareReferenceAttribute"/> <c>class</c>
    /// provides support for middleware.
    /// 
    /// <para>
    /// Middleware allows application specific code to be injected into the parsing
    /// pipeline. This is one of the most advanced parts of the system and requires
    /// a moderate amount of knowledge on how the system works.
    /// Multiple middleware references can be declared.
    /// In most cases, middleware should not be needed.
    /// </para>
    /// 
    /// <para>
    /// The attribute takes a type and a method name that points to the method
    /// representing the initializer. If the type is not specified, then the declaring
    /// type of the marked method will be used.
    /// </para>
    /// 
    /// <para>
    /// Middleware methods must be <c>static</c>, have a return type of <see cref="Task"/>,
    /// and accept exactly two parameters of types <see cref="InvocationContext"/>
    /// and <see cref="Func{InvocationContext, Task}"/>.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class MiddlewareReferenceAttribute : Attribute
    {
        public MiddlewareReferenceAttribute(EMiddlewareOrder order, string methodName)
            : this(order, 0, null, methodName) { }

        public MiddlewareReferenceAttribute(int order, string methodName)
            : this(0, order, null, methodName) { }

        public MiddlewareReferenceAttribute(EMiddlewareOrder orderBase, int orderOffset, string methodName)
            : this(orderBase, orderOffset, null, methodName) { }

        public MiddlewareReferenceAttribute(EMiddlewareOrder order, Type declaringType, string methodName)
            : this(order, 0, declaringType, methodName) { }

        public MiddlewareReferenceAttribute(int order, Type declaringType, string methodName)
            : this(0, order, declaringType, methodName) { }

        public MiddlewareReferenceAttribute(EMiddlewareOrder orderBase, int orderOffset, Type declaringType, string methodName)
        {
            Thrower.If.IsEmpty(methodName, nameof(methodName)).Throw();
            this.OrderBase = orderBase;
            this.OrderOffset = orderOffset;
            this.DeclaringType = declaringType;
            this.MethodName = methodName;
        }

        public EMiddlewareOrder OrderBase { get; }

        public int OrderOffset { get; }

        /// <summary>
        /// Gets the type containing <see cref="MethodName"/> or <c>null</c>.
        /// </summary>
        public Type DeclaringType { get; }

        /// <summary>
        /// Gets the name of the referenced method.
        /// </summary>
        public string MethodName { get; }

        public EMiddlewareOrder Order => this.OrderBase + this.OrderOffset;

        //public delegate Task InvocationMiddleware(InvocationContext context, Func<InvocationContext, Task> next);
        public Middleware GetMiddleware(Type fallbackType)
        {
            return new Middleware(this.OrderBase, this.OrderOffset, (InvocationMiddleware)this.GetMethod(fallbackType).CreateDelegate(typeof(InvocationMiddleware)));
        }

        /// <summary>
        /// Gets the <see cref="MethodInfo"/> from the <see cref="DeclaringType"/> and <see cref="MethodName"/>
        /// members. If <see cref="DeclaringType"/> is <c>null</c>, then <paramref name="fallbackType"/> is
        /// used instead; if both are null, then an exception is thrown.
        /// </summary>
        /// <param name="fallbackType">The <see cref="Type"/> to use if <see cref="DeclaringType"/> is <c>null</c>.</param>
        /// <returns>The <see cref="MethodInfo"/> represented by the attribute instance.</returns>
        /// <exception cref="ArgumentNullException">If <see cref="DeclaringType"/> and <paramref name="fallbackType"/> are both <c>null</c>.</exception>
        public MethodInfo GetMethod(Type fallbackType)
        {
            var type = this.DeclaringType ?? fallbackType ?? throw new ArgumentNullException(nameof(fallbackType));
            var name = this.MethodName;
            var method = ReflectionHelper.GetMethodInfo(type, name);

            if (method is null)
            { throw new MethodAccessException($"Validator reference method '{name}' on type '{type}' could not be found."); }

            if (!method.IsStatic)
            { throw new MethodAccessException($"Validator reference method '{name}' on type '{type}' MUST be static."); }

            if (method.ReturnType != typeof(Task))
            { throw new MethodAccessException($"Validator reference method '{name}' on type '{type}' MUST have a return type of '{typeof(Task)}'."); }

            var parameters = method.GetParameters();
            if (parameters.Length != 2
                || parameters[0].ParameterType != typeof(InvocationContext)
                || parameters[1].ParameterType != typeof(Func<InvocationContext, Task>))
            { throw new MethodAccessException($"Validator reference method '{name}' on type '{type}' MUST accept only two parameters of types {typeof(InvocationContext)} and {typeof(Func<InvocationContext, Task>)}."); }

            return method;
        }
    }
}
