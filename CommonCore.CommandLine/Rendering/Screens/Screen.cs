﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.IO;
using System.Linq;
using CommonCore.CommandLine.Extensions;
using CommonCore.CommandLine.Rendering.Views;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering
{
    /// <summary>
    /// The <see cref="Screen"/> <c>class</c> provides a simple interface for
    /// constructing a batch of content to be displayed by the implementing
    /// console application.
    /// </summary>
    public class Screen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Screen"/> <c>class</c>.
        /// </summary>
        public Screen()
        {
            this.AppInfo = Services.Instance.Get<AppInfo>();
            this.Stack = new StackView();
            this.View = new ViewHost(this.Stack);
        }

        /// <summary>
        /// Gets the <see cref="AppInfo"/> related to the current implementing console application.
        /// </summary>
        public AppInfo AppInfo { get; protected set; }

        /// <summary>
        /// Gets the <see cref="ViewHost"/> used to render the screen.
        /// </summary>
        public ViewHost View { get; protected set; }

        /// <summary>
        /// Gets the <see cref="StackView"/> representing the screen content.
        /// </summary>
        public StackView Stack { get; protected set; }

        /// <summary>
        /// Gets the <see cref="IConsoleRenderer"/> used to render the screen.
        /// </summary>
        public IConsoleRenderer Renderer => this.View.Renderer;

        /// <summary>
        /// Fluent method to display the screen contents.
        /// </summary>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Render() { this.View.Render(); return this; }



        /// <summary>
        /// Fluent method to add a blank line to the content.
        /// </summary>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add() => this.Add(new TextView("\n"));

        /// <summary>
        /// Fluent method to add a span to the content.
        /// </summary>
        /// <param name="span">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(ASpan span) { this.Stack.Add(new TextView(span)); return this; }

        /// <summary>
        /// Fluent method to add a view to the content.
        /// </summary>
        /// <param name="view">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(IView view) { this.Stack.Add(view); return this; }

        /// <summary>
        /// Fluent method to add multiple views to the content.
        /// </summary>
        /// <param name="views">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(params IView[] views) { foreach (var view in views) { this.Add(view); } return this; }

        /// <summary>
        /// Fluent method to add multiple views to the content.
        /// </summary>
        /// <param name="views">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(IEnumerable<IView> views) { foreach (var view in views) { this.Add(view); } return this; }

        /// <summary>
        /// Fluent method to add text to the content.
        /// </summary>
        /// <param name="text">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(string text) => this.Add(new TextView(text));

        /// <summary>
        /// Fluent method to add formatted text to the content.
        /// </summary>
        /// <param name="format">The format of the content.</param>
        /// <param name="args">The objects to format.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(string format, params object[] args) => this.Add(new TextView(string.Format(format, args)));

        /// <summary>
        /// Fluent method to add multiple texts to the content.
        /// </summary>
        /// <param name="texts">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(params string[] texts) { foreach (var text in texts) { this.Add(text); } return this; }

        /// <summary>
        /// Fluent method to add multiple texts to the content.
        /// </summary>
        /// <param name="texts">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(IEnumerable<string> texts) { foreach (var text in texts) { this.Add(text); } return this; }




        /// <summary>
        /// Fluent method to add text to the content.
        /// </summary>
        /// <param name="indent">The number of levels to indent the content.</param>
        /// <param name="text">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(int indent, string text) => this.Add(new TextView(new ContentSpan(new string(' ', indent * 2)), new ContentSpan(text)));

        /// <summary>
        /// Fluent method to add formatted text to the content.
        /// </summary>
        /// <param name="indent">The number of levels to indent the content.</param>
        /// <param name="format">The format of the content.</param>
        /// <param name="args">The objects to format.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(int indent, string format, params object[] args) => this.Add(indent, string.Format(format, args));

        /// <summary>
        /// Fluent method to add multiple texts to the content.
        /// </summary>
        /// <param name="indent">The number of levels to indent the content.</param>
        /// <param name="texts">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(int indent, params string[] texts) { foreach (var text in texts) { this.Add(indent, text); } return this; }

        /// <summary>
        /// Fluent method to add multiple texts to the content.
        /// </summary>
        /// <param name="indent">The number of levels to indent the content.</param>
        /// <param name="texts">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(int indent, IEnumerable<string> texts) { foreach (var text in texts) { this.Add(indent, text); } return this; }




        /// <summary>
        /// Fluent method to add a blank line to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check) => check ? this.Add() : this;

        /// <summary>
        /// Fluent method to add a view to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="view">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, IView view) => check ? this.Add(view) : this;

        /// <summary>
        /// Fluent method to add text to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="text">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, string text) => check ? this.Add(text) : this;

        /// <summary>
        /// Fluent method to add formatted text to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="format">The format of the content.</param>
        /// <param name="args">The objects to format.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, string format, params object[] args) => check ? this.Add(format, args) : this;

        /// <summary>
        /// Fluent method to add multiple texts to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="texts">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, params string[] texts) => check ? this.Add(texts) : this;

        /// <summary>
        /// Fluent method to add multiple texts to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="texts">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, IEnumerable<string> texts) => check ? this.Add(texts) : this;




        /// <summary>
        /// Fluent method to add text to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="indent">The number of levels to indent the content.</param>
        /// <param name="text">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, int indent, string text) => check ? this.Add(indent, text) : this;

        /// <summary>
        /// Fluent method to add formatted text to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="indent">The number of levels to indent the content.</param>
        /// <param name="format">The format of the content.</param>
        /// <param name="args">The objects to format.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, int indent, string format, params object[] args) => check ? this.Add(indent, format, args) : this;

        /// <summary>
        /// Fluent method to add multiple texts to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="indent">The number of levels to indent the content.</param>
        /// <param name="texts">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, int indent, params string[] texts) => check ? this.Add(indent, texts) : this;

        /// <summary>
        /// Fluent method to add multiple texts to the content.
        /// </summary>
        /// <param name="check">Indicate if the content should be added.</param>
        /// <param name="indent">The number of levels to indent the content.</param>
        /// <param name="texts">The content to add.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen Add(bool check, int indent, IEnumerable<string> texts) => check ? this.Add(indent, texts) : this;




        public Screen AddTable<TItem, TValue>(IEnumerable<TItem> items, params Func<TItem, TValue>[] cellValues)
        {
            var table = new TableView<TItem>() { Items = items.ToArray() };
            foreach (var cellValue in cellValues) { table.AddColumn(cellValue); }
            return this;
        }

        public Screen AddTable<TItem, TValue>(IEnumerable<TItem> items, params (string Header, Func<TItem, TValue> CellValue)[] datas)
        {
            var table = new TableView<TItem>() { Items = items.ToArray() };
            foreach (var data in datas) { table.AddColumn(data.CellValue, data.Header); }
            return this;
        }

        public Screen AddTable<TItem, TValue>(IEnumerable<TItem> items, params (IView Header, Func<TItem, TValue> CellValue)[] datas)
        {
            var table = new TableView<TItem>() { Items = items.ToArray() };
            foreach (var data in datas) { table.AddColumn(data.CellValue, data.Header); }
            return this;
        }




        public Screen AddTable<TItem, TValue>(bool check, IEnumerable<TItem> items, params Func<TItem, TValue>[] datas)
            => check ? this.AddTable<TItem, TValue>(items, datas) : this;

        public Screen AddTable<TItem, TValue>(bool check, IEnumerable<TItem> items, params (string Header, Func<TItem, TValue> CellValue)[] datas)
            => check ? this.AddTable<TItem, TValue>(items, datas) : this;

        public Screen AddTable<TItem, TValue>(bool check, IEnumerable<TItem> items, params (IView Header, Func<TItem, TValue> CellValue)[] datas)
            => check ? this.AddTable<TItem, TValue>(items, datas) : this;




        /// <summary>
        /// Fluent method to add the default application header.
        /// </summary>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddHeader() => this.Add(new TextView(ForeColorSpan.Green(), new ContentSpan(string.Format("{0} {1}{2}", this.AppInfo.Title, this.AppInfo.Version, (this.AppInfo.DebugMode ? " (DEBUG)" : ""))), ForeColorSpan.Reset()));

        /// <summary>
        /// Fluent method to add the default application copyright notice.
        /// </summary>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddCopyright() => this.Add(this.AppInfo.Copyright);

        /// <summary>
        /// Fluent method to add the default application license summary.
        /// </summary>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddLicenseSummary() => this.Add(this.AppInfo.LicenseSummary);

        /// <summary>
        /// Fluent method to add the default application license text, if it exists.
        /// </summary>
        /// <param name="written">Reports whether the license text was added.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddLicenseText(out bool written)
        {
            var text = this.AppInfo.LicenseText;
            written = !string.IsNullOrEmpty(this.AppInfo.LicenseText);
            return written ? this.Add(this.AppInfo.LicenseText) : this;
        }

        //public (Screen True, Screen False) If(bool value) => value ? (this, (Screen)null) : ((Screen)null, this);




        /// <summary>
        /// Fluent method to add the default command summary section, if it exists.
        /// </summary>
        /// <param name="command">The command to build content from.</param>
        /// <param name="shortForm">Indicates whether the short content or full content should be added.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddSummary(ICommand command, bool shortForm)
        {
            bool isSubComand = command.Parents.OfType<ICommand>().Any();
            if (isSubComand) { this.Add($"COMMAND: {command.Name}").Add(); }

            var help = command.GetHelpInfo();
            if (help is null || help.SummaryViews is null || help.SummaryViews.Count == 0)
            { return this; }

            if (shortForm) { this.Add(help.SummaryViews[0]).Add(); }
            else { this.Add(help.SummaryViews); }
            return this;
        }

        /// <summary>
        /// Fluent method to add the default command remarks section, if it exists.
        /// </summary>
        /// <param name="command">The command to build content from.</param>
        /// <param name="shortForm">Indicates whether the short content or full content should be added.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddRemarks(ICommand command, bool shortForm)
        {
            var help = command.GetHelpInfo();
            if (help is null || help.RemarksViews is null || help.RemarksViews.Count == 0)
            { return this; }

            this.Add("REMARKS:");
            if (shortForm) { this.Add(help.RemarksViews[0]).Add(); }
            else { this.Add(help.RemarksViews); }
            return this;
        }

        /// <summary>
        /// Fluent method to add the default command returns section, if it exists.
        /// </summary>
        /// <param name="command">The command to build content from.</param>
        /// <param name="shortForm">Indicates whether the short content or full content should be added.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddReturns(ICommand command, bool shortForm)
        {
            var help = command.GetHelpInfo();
            if (help is null || help.ReturnsViews is null || help.ReturnsViews.Count == 0)
            { return this; }

            this.Add("RETURNS:");
            if (shortForm) { this.Add(help.ReturnsViews[0]).Add(); }
            else { this.Add(help.ReturnsViews); }
            return this;
        }

        /// <summary>
        /// Fluent method to add the default errors section, if it exists.
        /// </summary>
        /// <param name="result">The <see cref="ParseResult"/> to build content from.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddErrors(ParseResult result)
        {
            if (result.Errors.Count > 0)
            {
                //static int __findMatch(string text, int start, char open, char close)
                //{
                //    int level = 0;
                //    for (int index = start; index < text.Length; index++)
                //    {
                //        if (text[index] == open) { level++; }
                //        else if (text[index] == close)
                //        { if (level > 1) { level--; } else { return index; } }
                //    }
                //    return -1;
                //}

                //static string __consume(string text, int start)
                //{
                //    int end = -1;
                //    int[] ends = new int[3];
                //    ends[0] = text.IndexOf(" [", start);
                //    ends[1] = text.IndexOf("![", start);
                //    ends[2] = text.IndexOf("*[", start);
                //    if (ends.Any(i => i > -1)) { end = ends.Where(i => i > -1).Min(); }
                //    if (end == -1) { return null; }
                //    var result = text[start..end];
                //    return result.Length == 0 ? null : result;
                //}

                //static string __consumeBlock(string text, int start)
                //{
                //    int end = __findMatch(text, start, '[', ']');
                //    if (end == -1) { return null; }
                //    var result = text[start..(end + 1)];
                //    return result.Length == 0 ? null : result;
                //}

                //var list = new List<ISpan>();
                //var diagram = result.Diagram();
                //int index = 0;
                //while (index < diagram.Length)
                //{
                //    var text = __consume(diagram, index);
                //    if (!(text is null || text.Length == 0))
                //    {
                //        list.Add(new ContentSpan(text));
                //        index += text.Length;
                //    }
                //    text = __consumeBlock(diagram, index);
                //    if (!(text is null || text.Length == 0))
                //    {
                //        if (text.StartsWith("![")) { list.Add(ForeColorSpan.Red()); }
                //        else if (text.StartsWith("*[")) { list.Add(ForeColorSpan.Cyan()); }
                //        list.Add(new ContentSpan(text));
                //        list.Add(ForeColorSpan.Reset());
                //        index += text.Length;
                //    }
                //}

                this//.Add(new TextView(list))
                    .Add("ERRORS:")
                    .Add(new ListView<ParseError, TextView>(result.Errors, e => B.Open().Red().Text(e.Message).Close()) { Indenting = 2 })
                    .Add();
            }
            return this;
        }

        /// <summary>
        /// Fluent method to add the default command usages section, if it exists.
        /// </summary>
        /// <param name="command">The command to build content from.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddUsages(ICommand command)
        {
            var usages = command.ToUsages();
            if (usages.Any())
            {
                this.Add("USAGES:")
                    .Add((IView)new ListView(usages) { Indenting = 2 })
                    .Add();
            }
            return this;
        }

        /// <summary>
        /// Fluent method to add the default command arguments section, if it exists.
        /// </summary>
        /// <param name="command">The command to build content from.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddArguments(ICommand command)
        {
            var commands = new List<ICommand>() { command };
            if (command.Parents.FirstOrDefault() is ICommand parent) { commands.Add(parent); }

            var arguments = commands.SelectMany(c => c.Arguments.Where(a => !a.IsHidden));
            if (arguments.Any())
            {
                this.Add("ARGUMENTS:");

                var table = new TableView<IArgument>(arguments) { Indenting = 2 };
                table.AddColumn(d => d.ToHelpInvocation());
                table.AddColumn(d => d.ToHelpDescription());
                this.Add(table)
                    .Add();
            }
            return this;
        }

        /// <summary>
        /// Fluent method to add the default command options section, if it exists.
        /// </summary>
        /// <param name="command">The command to build content from.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddOptions(ICommand command)
        {
            var options = command.Options.Where(o => !o.IsHidden).ToArray();
            if (options.Length > 0)
            {
                this.Add("OPTIONS:");

                var table = new TableView<IOption>(options) { Indenting = 2 };
                table.AddColumn(option => option.ToHelpInvocation());
                table.AddColumn(option => option.ToHelpDescription());
                this.Add(table)
                    .Add();
            }
            return this;
        }

        /// <summary>
        /// Fluent method to add the default command subcommands section, if it exists.
        /// </summary>
        /// <param name="command">The command to build content from.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddSubcommands(ICommand command)
        {
            var subcommands = command.Children.OfType<ICommand>().Where(c => !c.IsHidden).ToArray();
            if (subcommands.Length > 0)
            {
                this.Add("COMMANDS:");

                int max = subcommands.Max(c => c.ToInvocation().Length);
                var table = new TableView<ICommand>(subcommands) { Indenting = 2 };
                table.AddColumn(c => c.ToInvocation());
                table.AddColumn(c => c.ToDescriptor());
                table.AddColumn(c => c.ToSummary());
                this.Add(table)
                    .Add();
            }
            return this;
        }

        /// <summary>
        /// Fluent method to add the default command additional arguments section, if it exists.
        /// </summary>
        /// <param name="command">The command to build content from.</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        public Screen AddAdditionalArguments(ICommand command)
        {
            if (!command.TreatUnmatchedTokensAsErrors)
            {
                this.Add("ADDITIONAL ARGUMENTS:")
                    .Add(1, "Arguments passed to the application that is being run.")
                    .Add();
            }
            return this;
        }




        /// <summary>
        /// Don't use this method!! Unneeded one-for-one wrapper around <see cref="File.WriteAllText(string, string)"/>.
        /// </summary>
        /// <param name="path">Don't use this method!!</param>
        /// <param name="content">Don't use this method!!</param>
        /// <returns>Fluent reference to <c>this</c> screen.</returns>
        [Obsolete("Don't use this method!!", true)]
        public Screen WriteAllText(string path, string content) { File.WriteAllText(path, content); return this; }
    }
}
