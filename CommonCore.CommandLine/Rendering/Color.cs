﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerDisplay("{R},{G},{B}")]
    public readonly struct Color : IEquatable<Color>
    {
        //public static Color None { get; } = default;

        public Color(byte r, byte g, byte b)
        {
            this.R = r;
            this.G = g;
            this.B = b;
        }

        public readonly byte R { get; }

        public readonly byte G { get; }

        public readonly byte B { get; }

        public readonly override string ToString()
            => $"[{this.R}, {this.G}, {this.B}]";

        public readonly override int GetHashCode()
            => HashCode.Combine(this.R, this.G, this.B);

        public readonly override bool Equals(object obj)
            => obj is Color color && this.Equals(color);

        public readonly bool Equals(Color other)
            => this.R == other.R && this.G == other.G && this.B == other.B;

        public static bool operator ==(Color left, Color right) => left.Equals(right);

        public static bool operator !=(Color left, Color right) => !left.Equals(right);
    }
}
