﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.Threading.Tasks;

namespace CommonCore.CommandLine.Invocation
{
    public class DefaultExceptionHandlerMiddleware : Middleware
    {
        public DefaultExceptionHandlerMiddleware()
            : base(EMiddlewareOrder.SetupExceptionHandler, Middleware_ErrorReporting) { }

        private static async Task Middleware_ErrorReporting(InvocationContext context,
            Func<InvocationContext, Task> next)
        {
            try { await next(context); }
            catch (Exception exception) { __default(exception, context); }

            void __default(Exception exception, InvocationContext context)
            {
                context.Console.Error.WriteLine("Unhandled exception: ");
                context.Console.Error.WriteLine(exception.ToString());
                context.ResultCode = 1;
            }
        }
    }
}
