﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.Linq;
using CommonCore.CommandLine.Extensions;
using CommonCore.CommandLine.Rendering.Views;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering
{
    public class B
    {
        public static B Open() => new B();

        protected IEnumerable<string> _texts { get; set; }

        protected int _indentLevel { get; set; }

        public ForeColorSpan _foreSpan { get; set; }

        protected B Fore(ForeColorSpan span) { this._foreSpan = span; return this; }

        public B Black() => this.Fore(ForeColorSpan.Black());

        public B Red() => this.Fore(ForeColorSpan.Red());

        public B Green() => this.Fore(ForeColorSpan.Green());

        public B Yellow() => this.Fore(ForeColorSpan.Yellow());

        public B Blue() => this.Fore(ForeColorSpan.Blue());

        public B Magenta() => this.Fore(ForeColorSpan.Magenta());

        public B Cyan() => this.Fore(ForeColorSpan.Cyan());

        public B White() => this.Fore(ForeColorSpan.White());

        public B DarkGray() => this.Fore(ForeColorSpan.BrightBlack());

        public B LightRed() => this.Fore(ForeColorSpan.BrightRed());

        public B LightGreen() => this.Fore(ForeColorSpan.BrightGreen());

        public B LightYellow() => this.Fore(ForeColorSpan.BrightYellow());

        public B LightBlue() => this.Fore(ForeColorSpan.BrightBlue());

        public B LightMagenta() => this.Fore(ForeColorSpan.BrightMagenta());

        public B LightCyan() => this.Fore(ForeColorSpan.BrightCyan());

        public B LightGray() => this.Fore(ForeColorSpan.BrightWhite());

        public B Rgb(byte r, byte g, byte b) => this.Fore(ForeColorSpan.Rgb(r, g, b));

        public B Text(string text) { this._texts = text.Yield(); return this; }

        public B Text(params string[] texts) { this._texts = texts; return this; }

        public B Text(IEnumerable<string> texts) { this._texts = texts; return this; }

        public B Indent(int level) { this._indentLevel = level; return this; }

        public TextView Close()
        {
            var view = new TextView(this.BuildSpans());
            this._texts = null;
            this._foreSpan = null;
            this._indentLevel = 0;
            return view;
        }

        protected IEnumerable<ISpan> BuildSpans()
        {
            if (this._indentLevel > 0) { yield return new ContentSpan(new string(' ', this._indentLevel * 2)); }
            if (this._foreSpan != null) { yield return this._foreSpan; }
            if (this._texts != null) { foreach (var text in this._texts) { yield return new ContentSpan(text); } }
            if (this._foreSpan != default) { yield return ForeColorSpan.Reset(); }
        }
    }
}
