﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine.Parsing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using CommonCore.CommandLine.Rendering.Views;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Help
{
    public class HelpDocReader
    {
        private HelpDocReader(XDocument document)
        {
            this.Document = document;
            this.Members = document.Descendants("members");
        }

        public XDocument Document { get; protected set; }

        public IEnumerable<XElement> Members { get; protected set; }

        public List<string> BlockElementNames { get; } = new List<string>() { "para", "code", "list" };

        public List<string> InlineElementNames { get; } = new List<string>() { "b", "i", "c", "em", "see", "paramref", "typeparamref" };

        // TODO: This smells, shouldn't just eat the exception.
        public static HelpDocReader Load(string filePath)
        { try { return Load(File.OpenText(filePath)); } catch { return null; } }

        // TODO: This smells, shouldn't just eat the exception.
        public static HelpDocReader Load(TextReader reader)
        { try { return new HelpDocReader(XDocument.Load(reader, LoadOptions.PreserveWhitespace)); } catch { return null; } }

        public HelpInfo ParseHelp(MethodInfo method)
        {
            if (method is null) { throw new ArgumentNullException(nameof(method)); }

            var element = this.GetMemberElement(method);
            if (element is null) { return null; }

            var help = new HelpInfo();
            this.ParseMemberElement(element, help);
            return help;
        }

        public bool TryParseHelp(MethodInfo method, HelpInfo help)
        {
            if (method is null) { throw new ArgumentNullException(nameof(method)); }
            if (help is null) { throw new ArgumentNullException(nameof(help)); }

            var element = this.GetMemberElement(method);
            if (element == null) { return false; }

            this.ParseMemberElement(element, help);
            return true;
        }

        public bool TryParseHelp(Type type, HelpInfo help)
        {
            if (type is null) { throw new ArgumentNullException(nameof(type)); }
            if (help is null) { throw new ArgumentNullException(nameof(help)); }

            var element = this.GetMemberElement(type);
            if (element == null) { return false; }

            this.ParseMemberElement(element, help);
            return true;
        }

        /// <summary>
        /// Parses the specified <paramref name="member"/> element for the root tags:
        /// <c>summary</c>, <c>remarks</c>, <c>returns</c>, <c>param</c>, <c>typeparam</c>, <c>exception</c>, <c>permission</c>, and <c>seealso</c>.
        /// </summary>
        /// <param name="member">The member element containing the root tags.</param>
        /// <param name="help">The help info object to populate.</param>
        public void ParseMemberElement(XElement member, HelpInfo help)
        {
            if (member is null) { throw new ArgumentNullException(nameof(member)); }
            if (help is null) { throw new ArgumentNullException(nameof(help)); }

            {
                XElement element;

                element = member.Element("summary");
                if (element != null) { help.SummaryViews = this.ParseRootElement(element); }

                element = member.Element("remarks");
                if (element != null) { help.RemarksViews = this.ParseRootElement(element); }

                element = member.Element("returns");
                if (element != null) { help.ReturnsViews = this.ParseRootElement(element); }
            }

            {
                IEnumerable<XElement> elements;

                elements = member.Elements("param");
                if (elements != null && elements.Any())
                {
                    var pairs = from element in elements
                                select (element.Attribute("name")?.Value.ToKebabCase(), this.ParseRootElement(element));
                    if (pairs.Any()) { help.ParamViewPairs = pairs.ToList(); }
                }

                elements = member.Elements("typeparam");
                if (elements != null && elements.Any())
                {
                    var pairs = from element in elements
                                select (element.Attribute("name")?.Value.ToKebabCase(), this.ParseRootElement(element));
                    if (pairs.Any()) { help.TypeParamViewPairs = pairs.ToList(); }
                }

                elements = member.Elements("exception");
                if (elements != null && elements.Any())
                {
                    var pairs = from element in elements
                                select (element.Attribute("cref")?.Value, this.ParseRootElement(element));
                    if (pairs.Any()) { help.ExceptionViewPairs = pairs.ToList(); }
                }

                elements = member.Elements("permission");
                if (elements != null && elements.Any())
                {
                    var pairs = from element in elements
                                select (element.Attribute("cref")?.Value, this.ParseRootElement(element));
                    if (pairs.Any()) { help.PermissionViewPairs = pairs.ToList(); }
                }

                elements = member.Elements("seealso");
                if (elements != null && elements.Any())
                {
                    var pairs = from element in elements
                                select (element.Attribute("cref")?.Value, this.ParseRootElement(element));
                    if (pairs.Any()) { help.SeeAlsoViewPairs = pairs.ToList(); }
                }
            }

            // <param name="name">description</param>
            // <typeparam name="name">description</typeparam>
            // <seealso cref="member" />
            // <exception cref="member">description</exception>
            // <permission cref="member">description</permission>
            // <include file='filename' path='tagpath[@name="id"]' />
            // - https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/include
            // <inheritdoc cref="member" />
            // - https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/inheritdoc
        }

        /// <summary>
        /// Parses a root element and returns a list of <see cref="IView"/> objects representing the parsed data.
        /// </summary>
        /// <param name="element">The root element containing the primary tags.</param>
        /// <returns>A list of views representing the parsed data.</returns>
        public List<IView> ParseRootElement(XElement element, bool allowRecursion = true)
        {
            var views = new List<IView>();
            var spans = new List<ISpan>();

            foreach (var node in element.Nodes())
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Text:
                    {
                        var e = node as XText;
                        if (spans.Count == 0 && string.IsNullOrWhiteSpace(e.Value)) { break; }
                        else { spans.Add(this.ParseText(e, spans.Count == 0)); }
                        break;
                    }
                    case XmlNodeType.Element:
                    {
                        var e = node as XElement;
                        var name = e.Name.LocalName;
                        if (allowRecursion && name == "example")
                        {
                            __pushSpansToView();
                            foreach (var view in this.ParseRootElement(e, false))
                            { __pushRawView(view); }
                        }
                        else if (this.BlockElementNames.Contains(name))
                        {
                            __pushSpansToView();
                            __pushRawView(this.ParseBlockElement(e));
                        }
                        else// if (this.InlineElementNames.Contains(name))
                        { spans.Add(this.ParseInlineElement(e)); }
                        break;
                    }
                    case XmlNodeType.SignificantWhitespace:
                    case XmlNodeType.Whitespace:
                    {
                        break;
                    }
                }
            }
            __pushSpansToView();
            return views;

            void __pushBlankView()
            {
                if (spans.Count > 0) { throw new InvalidOperationException("Can't push a blank view when spans are buffered."); }
                views.Add(new TextView());
            }

            void __pushRawView(IView view)
            {
                if (spans.Count > 0) { throw new InvalidOperationException("Can't push a raw view when spans are buffered."); }
                views.Add(view ?? throw new ArgumentNullException(nameof(view)));
                __pushBlankView();
            }

            void __pushSpansToView()
            {
                if (spans.Count == 0) { return; }
                else if (spans.Count == 1) { views.Add(new TextView(spans[0])); }
                else { views.Add(new TextView(spans)); }
                spans.Clear();
                __pushBlankView();
            }
        }

        /// <summary>
        /// Parses a text node and returns a <see cref="NormalizedTextSpan"/> containing the
        /// content, optionally with any leading whitespace trimmed.
        /// </summary>
        /// <param name="node">The text node to parse.</param>
        /// <param name="trimStart">Whether leading whitespace should be trimmed.</param>
        /// <returns>
        /// A <see cref="NormalizedTextSpan"/> containing the
        /// content, optionally with any leading whitespace trimmed.
        /// </returns>
        public ISpan ParseText(XText node, bool trimStart)
            => new NormalizedTextSpan(!trimStart ? node.Value : node.Value.TrimStart());

        /// <summary>
        /// Parses the specified <paramref name="element"/> as a block element
        /// and returns a <see cref="IView"/> representing the parsed content. The recognized
        /// block elements are <c>code</c>, <c>list</c>, and <c>para</c>. Any
        /// other element will be treated as a <c>para</c> element.
        /// </summary>
        /// <param name="element">The element to parse.</param>
        /// <returns>A view representing the parsed content.</returns>
        public IView ParseBlockElement(XElement element)
            => element.Name.LocalName switch
            {
                "code" => this.ParseBlockElementCode(element),
                "list" => element.Attribute("type")?.Value switch
                {
                    "number" => this.ParseBlockElementListNumbered(element),
                    "table" => this.ParseBlockElementListTable(element),
                    _ => this.ParseBlockElementListBulleted(element)
                },
                _ => this.ParseBlockElementPara(element)
            };

        /// <summary>
        /// Parses the specified <paramref name="element"/> as an inline element
        /// and returns a <see cref="ISpan"/> representing the parsed content.
        /// The recognized inline elements are <c>b</c>, <c>c</c>, <c>i</c>, <c>em</c>,
        /// <c>see</c>, <c>paramref</c>, and <c>typeparamref</c>.
        /// Any other element will be ignored and its contents treated as a text node.
        /// </summary>
        /// <param name="element">The element to parse.</param>
        /// <returns>A <see cref="ISpan"/> representing the parsed content.</returns>
        public ISpan ParseInlineElement(XElement element)
            => element.Name.LocalName switch
            {
                "b" => new ContainerSpan(ForeColorSpan.BrightBlack(), new ContentSpan(element.Value), ForeColorSpan.Reset()),
                "i" => new ContainerSpan(StyleSpan.UnderlineOn(), new ContentSpan(element.Value), StyleSpan.UnderlineOff()),
                "c" => new ContainerSpan(ForeColorSpan.Cyan(), new ContentSpan(element.Value), ForeColorSpan.Reset()),
                "em" => new ContainerSpan(ForeColorSpan.BrightBlack(), new ContentSpan(element.Value), ForeColorSpan.Reset()),
                "see" => new ContainerSpan(ForeColorSpan.Cyan(), new ContentSpan(element.Attribute("cref").Value), ForeColorSpan.Reset()),
                "paramref" => new ContainerSpan(ForeColorSpan.Cyan(), new ContentSpan(element.Attribute("name").Value), ForeColorSpan.Reset()),
                "typeparamref" => new ContainerSpan(ForeColorSpan.Cyan(), new ContentSpan(element.Attribute("name").Value), ForeColorSpan.Reset()),
                _ => new NormalizedTextSpan(element.Value)
            };

        // TODO: Code blocks should preserve leading whitespace.
        // TODO: Code blocks should preserve internal xml elements.
        /// <summary>
        /// Parses a <c>code</c> element and returns a <see cref="IView"/> representing the parsed content.
        /// </summary>
        /// <param name="element">The element to parse.</param>
        /// <returns>A <see cref="IView"/> representing the parsed content.</returns>
        public IView ParseBlockElementCode(XElement element)
        {
            var view = new ListView<IView, IView>() { Symbol = ">", Delimiter = " " };
            view.ContentValueFunc = v => v;

            var value = element.ToString()[6..^7];
            var lines = value.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (string.IsNullOrWhiteSpace(lines[0])) { lines = lines[1..^0]; }
            if (string.IsNullOrWhiteSpace(lines[^1])) { lines = lines[0..^1]; }
            int min = int.MaxValue;
            foreach (var line in lines)
            {
                int count = line.TakeWhile(c => char.IsWhiteSpace(c)).Count();
                if (count < min) { min = count; }
            }

            view.Items = (from line in lines
                          select new TextView(ForeColorSpan.BrightBlack(), new ContentSpan(line.Substring(min)), ForeColorSpan.Reset()))
                         .ToArray();
            return view;
        }

        /// <summary>
        /// Parses a <c>para</c> element and returns a <see cref="IView"/> representing the parsed content.
        /// Only text nodes and inline elements are parsed inside a <c>para</c> block.
        /// </summary>
        /// <param name="element">The element to parse.</param>
        /// <returns>A <see cref="IView"/> representing the parsed content.</returns>
        public IView ParseBlockElementPara(XElement element)
        {
            var spans = new List<ISpan>();
            foreach (var node in element.Nodes())
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Text: { spans.Add(this.ParseText((XText)node, spans.Count == 0)); break; }
                    case XmlNodeType.Element: { spans.Add(this.ParseInlineElement((XElement)node)); break; }
                    case XmlNodeType.SignificantWhitespace:
                    case XmlNodeType.Whitespace: { break; }
                }
            }
            return new TextView(spans);
        }

        public IView ParseBlockElementListBulleted(XElement element)
            => new BulletedListView<IView, IView>(
                from item in element.Elements("item") select this.JoinListItemValue(item),
                v => v);

        public IView ParseBlockElementListNumbered(XElement element)
            => new NumberedListView<IView, IView>(
                from item in element.Elements("item") select this.JoinListItemValue(item),
                v => v);

        public IView ParseBlockElementListTable(XElement element)
        {
            var items = element.Elements("item");
            var head = element.Element("listheader");
            var cols = head.Elements("term").Select(e => this.ParseBlockElementPara(e)).ToArray();

            var view = new TableView<IView[]>();
            for (int icol = 0; icol < cols.Length; icol++)
            {
                // Note: Store current icol in a variable so it
                // doesn't change before the delegate can use it.
                var index = icol;
                view.AddColumn(row => row[index], cols[icol]);
            }

            var rows = new IView[items.Count()][];
            int irow = 0;
            foreach (var item in items)
            {
                var descs = item.Elements("description").Select(e => this.ParseBlockElementPara(e)).ToArray();
                var row = new IView[cols.Length];
                // Note: Use ElementAtOrDefault() in case descs is shorter than row.
                for (int icol = 0; icol < cols.Length; icol++)
                { row[icol] = descs.ElementAtOrDefault(icol); }
                rows[irow] = row;
                irow++;
            }
            view.Items = rows;
            return view;
        }

        public XElement GetMemberElement(Type info)
        {
            string name = $"T:{info.FullName}";
            return this.Members.Elements("member").FirstOrDefault(m => string.Equals(m.Attribute("name")?.Value, name));
        }

        public XElement GetMemberElement(MethodInfo info)
        {
            string name = $"M:{BuildTypeName(info.DeclaringType)}.{info.Name}";
            var parameters = info.GetParameters();
            if (parameters.Length > 0)
            { name += $"({string.Join(',', parameters.Select(p => BuildTypeName(p.ParameterType)))})"; }
            return this.Members.Elements("member").FirstOrDefault(m => name.Equals(m.Attribute("name")?.Value));
        }

        public IView JoinListItemValue(XElement item)
        {
            var term = item.Element("term");
            var desc = item.Element("description");
            var termValue = term?.Value?.Trim();
            var descValue = desc?.Value?.Trim();
            bool hasTerm = !string.IsNullOrWhiteSpace(termValue);
            bool hasDesc = !string.IsNullOrWhiteSpace(descValue);
            if (hasTerm && hasDesc)
            {
                var view = new StackView(EOrientation.Horizontal);
                view.Add(this.ParseBlockElementPara(term));
                view.Add(this.ParseBlockElementPara(desc));
                return view;
            }
            return new TextView(hasTerm ? termValue : hasDesc ? descValue : string.Empty);
        }

        public static string BuildTypeName(Type type)
        {
            if (type.IsNested) { return BuildTypeName(type.DeclaringType) + "." + type.Name; }
            else if (!type.IsGenericType) { return type.FullName; }
            else
            {
                var def = type.GetGenericTypeDefinition().FullName;
                return def.Substring(0, def.IndexOf("`")) + "{" + string.Concat(type.GetGenericArguments().Select(t => BuildTypeName(t))) + "}";
            }
        }
    }
}
