﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.CommandLine.Parsing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.AutoBuild.Extensions;
using CommonCore.CommandLine.Invocation;

namespace CommonCore.CommandLine.AutoBuild
{
    public readonly struct CommandInfo : IEquatable<CommandInfo>
    {
        public static CommandInfo None { get; } = default;

        public CommandInfo(MethodInfo info, string commandName = null, bool includeInParentUsage = false, bool useKebabCase = true)
        {
            this.Info = info ?? throw new ArgumentNullException(nameof(info));
            this.IncludeInParentUsage = includeInParentUsage;
            this.Parameters = info.FilterAutoParameters().ToArray();

            var commands = info.GetCustomAttributes<CommandReferenceAttribute>();
            this.Commands = commands.Select(a => new CommandInfo(a.GetMethod(info.DeclaringType), a.CommandName, a.IncludeUsage, useKebabCase:true)).ToArray();

            var initializers = info.GetCustomAttributes<InitializerReferenceAttribute>();
            this.Initializers = initializers.Select(a => a.GetMethod(info.DeclaringType)).ToArray();

            var middlewares = info.GetCustomAttributes<MiddlewareReferenceAttribute>();
            this.Middlewares = middlewares.Select(a => (a.OrderBase, a.OrderOffset, a.GetMethod(info.DeclaringType))).ToArray();

            var validators = info.GetCustomAttributes<ValidatorReferenceAttribute>();
            this.Validators = validators.Select(a => a.GetMethod(info.DeclaringType)).ToArray();

            this.CommandName = commandName
                ?? info.GetCustomAttribute<CommandNameAttribute>()?.Name
                ?? info.Name;
            if (useKebabCase) { this.CommandName = this.CommandName.ToKebabCase(); }
        }

        public readonly MethodInfo Info { get; }

        public readonly bool IncludeInParentUsage { get; }

        public readonly string CommandName { get; }

        public readonly IReadOnlyList<CommandInfo> Commands { get; }

        public readonly IReadOnlyList<MethodInfo> Initializers { get; }

        public readonly IReadOnlyList<(EMiddlewareOrder OrderBase, int OrderOffset, MethodInfo Method)> Middlewares { get; }

        public readonly IReadOnlyList<MethodInfo> Validators { get; }

        public readonly IReadOnlyList<ParameterInfo> Parameters { get; }

        public readonly override string ToString()
        {
            var builder = new StringBuilder();
            using var stringWriter = new StringWriter(builder);
            using var writer = new IndentedTextWriter(stringWriter);
            this.Write(writer);
            return builder.ToString();
        }

        public readonly void Write(IndentedTextWriter writer)
        {
            writer.WriteLine($"{this.Info.Name} ({this.CommandName}): {this.Info.DeclaringType}");
            writer.Indent++;

            writer.WriteLine($"Parameters: {this.Parameters.Count}");
            writer.Indent++;
            foreach (var parameter in this.Parameters)
            {
                writer.Write($"{parameter.ParameterType} {parameter.Name}");
                if (parameter.HasDefaultValue)
                {
                    writer.Write(" = ");
                    writer.Write(parameter.DefaultValue ?? "null");
                }
                writer.WriteLine();
            }
            writer.Indent--;

            writer.WriteLine($"Initializers: {this.Initializers.Count}");
            writer.Indent++;
            foreach (var initializer in this.Initializers)
            {
                writer.WriteLine($"{initializer.Name}: {initializer.DeclaringType}");
            }
            writer.Indent--;

            writer.WriteLine($"Middlewares: {this.Middlewares.Count}");
            writer.Indent++;
            foreach (var middleware in this.Middlewares)
            {
                writer.WriteLine($"{middleware.Method.Name}: {middleware.Method.DeclaringType}");
            }
            writer.Indent--;

            writer.WriteLine($"Validators: {this.Validators.Count}");
            writer.Indent++;
            foreach (var validator in this.Validators)
            {
                writer.WriteLine($"{validator.Name}: {validator.DeclaringType}");
            }
            writer.Indent--;

            foreach (var command in this.Commands) { command.Write(writer); }

            writer.Indent--;
        }

        public readonly override int GetHashCode() => HashCode.Combine(this.Info);

        public readonly override bool Equals(object obj) => obj is CommandInfo info && this.Equals(info);

        public readonly bool Equals(CommandInfo other) => EqualityComparer<MethodInfo>.Default.Equals(this.Info, other.Info);

        public static bool operator ==(CommandInfo left, CommandInfo right) => left.Equals(right);

        public static bool operator !=(CommandInfo left, CommandInfo right) => !left.Equals(right);
    }
}
