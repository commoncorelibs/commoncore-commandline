﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonCore.CommandLine.Rendering.Views
{
    public class TableView<TItem> : AItemsView<TItem>
    {
        public TableView() => this.Grid.Updated += this.OnGridUpdated;

        public TableView(IEnumerable<TItem> items) : this() => this.Items = items.ToArray();

        protected bool GridInitialized { get; set; }

        protected GridView Grid { get; set; } = new GridView();

        public int Indenting { get => this.Grid.Indenting; set => this.Grid.Indenting = value; }

        public string Delimiter { get => this.Grid.Delimiter; set => this.Grid.Delimiter = value; }

        public string Prefix { get => this.Grid.Prefix; set => this.Grid.Prefix = value; }

        public string Suffix { get => this.Grid.Suffix; set => this.Grid.Suffix = value; }

        public IReadOnlyList<ITableViewColumn<TItem>> Columns => this._columns;
        private List<ITableViewColumn<TItem>> _columns = new List<ITableViewColumn<TItem>>();

        public void AddColumn<TValue>(Func<TItem, TValue> cellValue, ColumnDefinition column = null)
            => this.AddColumn(cellValue, (IView)null, column);

        public void AddColumn<TValue>(Func<TItem, TValue> cellValue, string header, ColumnDefinition column = null)
            => this.AddColumn(cellValue, new TextView(header), column);

        public void AddColumn<TValue>(Func<TItem, TValue> cellValue, IView header, ColumnDefinition column = null)
            => this.AddColumn(new TableViewColumn<TItem, TValue>(cellValue, header, column ?? new ColumnDefinition()));

        public void AddColumn(ITableViewColumn<TItem> column)
        {
            this._columns.Add(column);
            this.GridInitialized = false;

            this.OnUpdated();
        }

        protected void OnGridUpdated(object sender, EventArgs e) => this.OnUpdated();

        protected override void WhenItemsSet(IReadOnlyList<TItem> oldItems, IReadOnlyList<TItem> newItems)
        { this.GridInitialized = false; base.WhenItemsSet(oldItems, newItems); }

        public override void Render(IConsoleRenderer renderer, Region region)
        {
            this.EnsureInitialized(renderer);
            this.Grid.Render(renderer, region);
        }

        public override Size Measure(IConsoleRenderer renderer, Size maxSize)
        {
            this.EnsureInitialized(renderer);
            return this.Grid.Measure(renderer, maxSize);
        }

        private void EnsureInitialized(IConsoleRenderer renderer)
        {
            if (this.GridInitialized) { return; }

            bool hasHeader = this.Columns.Any(c => c.Header != null);
            var rowCount = this.Items.Count;
            if (hasHeader) { rowCount++; }

            this.Grid.SetColumns(this.Columns.Select(x => x.ColumnDefinition).ToArray());
            this.Grid.SetRows(Enumerable.Repeat(RowDefinition.Auto(), rowCount).ToArray());

            if (hasHeader)
            {
                for (int columnIndex = 0; columnIndex < this.Columns.Count; columnIndex++)
                {
                    //if (this.Columns[columnIndex].Header is IView header)
                    //{ this.Grid.SetChild(header, columnIndex, 0); }
                    var header = this.Columns[columnIndex].Header ?? new TextView();
                    this.Grid.SetChild(header, columnIndex, 0);
                }
            }

            for (int itemIndex = 0; itemIndex < this.Items.Count; itemIndex++)
            {
                TItem item = this.Items[itemIndex];
                for (int columnIndex = 0; columnIndex < this.Columns.Count; columnIndex++)
                {
                    if (this.Columns[columnIndex].GetCell(item, renderer.Formatter) is IView cell)
                    {
                        this.Grid.SetChild(cell, columnIndex, itemIndex + (hasHeader ? 1 : 0));
                    }
                }
            }

            this.GridInitialized = true;
        }
    }
}
