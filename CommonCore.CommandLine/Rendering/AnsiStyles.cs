﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    // TODO: This list is missing a bunch of sequences:
    // https://en.wikipedia.org/wiki/ANSI_escape_code
    // https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences?redirectedfrom=MSDN
    [DebuggerStepThrough]
    public static class AnsiStyles
    {
        public static Ansi Reset { get; } = Ansi.From('m', 0);

        public static AnsiPair Bright { get; } = AnsiPair.From('m', 1, 22);

        public static AnsiPair Faint { get; } = AnsiPair.From('m', 2, 22);

        //public static AnsiPair Italic { get; } = AnsiPair.From('m', 3, 23);

        public static AnsiPair Underline { get; } = AnsiPair.From('m', 4, 24);

        //public static AnsiPair SlowBlink { get; } = AnsiPair.From('m', 5, 25);

        //public static AnsiPair RapidBlink { get; } = AnsiPair.From('m', 6, 25);

        public static AnsiPair Reverse { get; } = AnsiPair.From('m', 7, 27);

        //public static AnsiPair Hidden { get; } = AnsiPair.From('m', 8, 28);

        //public static AnsiPair Strike { get; } = AnsiPair.From('m', 9, 29);
    }
}
