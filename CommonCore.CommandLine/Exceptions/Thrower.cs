﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CommonCore.CommandLine.Exceptions
{
    public sealed class Thrower
    {
        private static readonly object _lock = new object();

        public static Thrower If => null;

        public Thrower()
        {
            this._exceptions = new List<Exception>(1);
            this.Exceptions = this._exceptions;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public IReadOnlyList<Exception> Exceptions { get; }
        private List<Exception> _exceptions = null;

        [EditorBrowsable(EditorBrowsableState.Never)]
        public Thrower AddException(Exception exception)
        {
            lock (_lock) { this._exceptions.Add(exception); }
            return this;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public Thrower ThrowExceptions()
        { throw this.Exceptions.Count == 1 ? this.Exceptions[0] : new AggregateException(this.Exceptions); }
    }
}
