﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public class ChoicesAttribute : Attribute
    {
        public ChoicesAttribute(object[] choices)
        {
            this.Choices = choices;
        }

        public object[] Choices { get; }
    }
}
