﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Reflection;
using System.Threading.Tasks;
using CommonCore.CommandLine.Exceptions;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="CommandReferenceAttribute"/> <c>class</c>
    /// provides a way to support verbs, aka subcommands.
    /// 
    /// <para>
    /// The attribute takes a type and a method name that points to the method
    /// representing the verb. If the type is not specified, then the declaring
    /// type of the marked method will be used.
    /// In this way, an arbitrarily nested hierarchy of explicitly related commands
    /// is simple to create and trivial for the library to parse.
    /// </para>
    /// 
    /// <para>
    /// Command methods must be <c>static</c> and have a return type of
    /// <see cref="void"/>, <see cref="int"/>, <see cref="Task"/>,
    /// or <see cref="Task{T}"/> with generic type of <see cref="int"/>.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class CommandReferenceAttribute : Attribute
    {
        public CommandReferenceAttribute(string methodName, bool includeUsage = false, string commandName = null)
            : this(null, methodName, includeUsage, commandName) { }

        public CommandReferenceAttribute(Type declaringType, string methodName, bool includeUsage = false, string commandName = null)
        {
            Thrower.If.IsEmpty(methodName, nameof(methodName)).Throw();

            this.DeclaringType = declaringType;
            this.MethodName = methodName;
            this.IncludeUsage = includeUsage;
            this.CommandName = commandName;
        }

        /// <summary>
        /// Gets the type containing <see cref="MethodName"/> or <c>null</c>.
        /// </summary>
        public Type DeclaringType { get; }

        /// <summary>
        /// Gets the name of the referenced method.
        /// </summary>
        public string MethodName { get; }

        public string CommandName { get; }

        public bool IncludeUsage { get; }

        /// <summary>
        /// Gets the <see cref="MethodInfo"/> from the <see cref="DeclaringType"/> and <see cref="MethodName"/>
        /// members. If <see cref="DeclaringType"/> is <c>null</c>, then <paramref name="fallbackType"/> is
        /// used instead; if both are null, then an exception is thrown.
        /// </summary>
        /// <param name="fallbackType">The <see cref="Type"/> to use if <see cref="DeclaringType"/> is <c>null</c>.</param>
        /// <returns>The <see cref="MethodInfo"/> represented by the attribute instance.</returns>
        /// <exception cref="ArgumentNullException">If <see cref="DeclaringType"/> and <paramref name="fallbackType"/> are both <c>null</c>.</exception>
        public MethodInfo GetMethod(Type fallbackType)
        {
            var type = this.DeclaringType ?? fallbackType ?? throw new ArgumentNullException(nameof(fallbackType));
            var name = this.MethodName;
            var method = ReflectionHelper.GetMethodInfo(type, name);

            if (method is null)
            { throw new MethodAccessException($"Command reference method '{name}' on type '{type}' could not be found."); }

            if (!method.IsStatic)
            { throw new MethodAccessException($"Command reference method '{name}' on type '{type}' must be static."); }

            if (method.ReturnType != typeof(void))
            { throw new MethodAccessException($"Command reference method '{name}' on type '{type}' return type MUST be declared 'void'."); }

            if (method.ReturnType != typeof(void)
                && method.ReturnType != typeof(int)
                && method.ReturnType != typeof(Task)
                && method.ReturnType != typeof(Task<int>))
            { throw new MethodAccessException($"Command reference method '{name}' on type '{type}' return type '{method.ReturnType}' MUST be declared 'void', 'int', 'Task', or 'Task<int>'."); }

            return method;
        }
    }
}
