﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerStepThrough]
    public static class AnsiScrollCursors
    {
        public static Ansi UpOne { get; } = Ansi.FromBracketless('A');

        public static Ansi DownOne { get; } = Ansi.FromBracketless('B');

        public static Ansi RightOne { get; } = Ansi.FromBracketless('C');

        public static Ansi LeftOne { get; } = Ansi.FromBracketless('D');
    }
}
