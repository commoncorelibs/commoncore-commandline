# CommonCore.CommandLine Introduction

The library revolves around static methods and attributes; commands are defined by static methods and features are activated with attributes. A command's [arguments](command_arguments.md) and [options](command_options.md) are defined by the parameters of the command's method and parameter attributes allow control over how those arguments and/or options are defined. [Verbs (ie subcommands)](command_verbs.md), [initializers](command_initializers.md), [validators](command_validators.md), and [middleware](command_middleware.md) are all defined by static methods and applied to commands through attributes on that command's method.

The library has four main usability goals:
* Minimum required markup to define a command.
* Maximum control over command definition.
* Structure is defined by static methods.
* Relationships must be explicitly defined.

There are also several design goals:
* Be quite by default; features that output must be opt-in.
* No reflection fishing; feature targets should declare themselves.
* Utilize immutability where possible; readonly structs are your friend.

With [auto-build](auto_build.md) enabled, the following is a basic Hello World example.

```csharp
[EnableDefaultExceptionReporting]
[EnableDefaultErrorReporting]
[EnableDefaultGlobalOptionHelp]
[EnableDefaultLocalOptionLicense]
[EnableDefaultLocalOptionVersion]
[AppMITLicense]
static class Program
{
    /// <summary>
    /// This is an example Hello World application
    /// using the <c>CommonCore.CommandLine</c> library.
    /// </summary>
    [CommandEntryPoint]
    static void Main()
    {
        Console.WriteLine("Hello World!");
    }
}
```
