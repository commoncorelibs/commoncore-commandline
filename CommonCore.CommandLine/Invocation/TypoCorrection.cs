﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.IO;
using System.CommandLine.Parsing;
using System.Linq;

namespace CommonCore.CommandLine.Invocation
{
    public class TypoCorrection
    {
        public TypoCorrection(int maxLevenshteinDistance)
        {
            if (maxLevenshteinDistance <= 0) { throw new ArgumentOutOfRangeException(nameof(maxLevenshteinDistance)); }

            this._maxLevenshteinDistance = maxLevenshteinDistance;
        }

        private readonly int _maxLevenshteinDistance;

        public void ProvideSuggestions(ParseResult result, IConsole console)
        {
            foreach (var token in result.UnmatchedTokens)
            {
                string suggestions = string.Join(", or ", this.GetPossibleTokens(result.CommandResult.Command, token).Select(x => $"'{x}'"));

                if (suggestions.Any())
                {
                    console.Out.WriteLine($"'{token}' was not matched. Did you mean {suggestions}?");
                }
            }
        }

        private IEnumerable<string> GetPossibleTokens(ISymbol targetSymbol, string token)
        {
            IEnumerable<string> possibleMatches = targetSymbol.Children
                .Where(x => !x.IsHidden)
                .Where(x => x.RawAliases.Count > 0)
                .Select(symbol =>
                    symbol.RawAliases
                        .Union(symbol.Aliases)
                        .OrderBy(x => GetDistance(token, x))
                        .ThenByDescending(x => GetStartsWithDistance(token, x))
                        .First()
                );

            int? bestDistance = null;
            return possibleMatches
                .Select(possibleMatch => (possibleMatch, distance: GetDistance(token, possibleMatch)))
                .Where(tuple => tuple.distance <= this._maxLevenshteinDistance)
                .OrderBy(tuple => tuple.distance)
                .ThenByDescending(tuple => GetStartsWithDistance(token, tuple.possibleMatch))
                .TakeWhile(tuple =>
                {
                    var (_, distance) = tuple;
                    if (bestDistance is null) { bestDistance = distance; }
                    return distance == bestDistance;
                })
                .Select(tuple => tuple.possibleMatch);
        }

        private static int GetStartsWithDistance(string first, string second)
        { int i; for (i = 0; i < first.Length && i < second.Length && first[i] == second[i]; i++) { } return i; }

        //Based on https://blogs.msdn.microsoft.com/toub/2006/05/05/generic-levenshtein-edit-distance-with-c/
        private static int GetDistance(string first, string second)
        {
            // Validate parameters
            if (first is null) { throw new ArgumentNullException(nameof(first)); }
            if (second is null) { throw new ArgumentNullException(nameof(second)); }

            // Get the length of both.  If either is 0, return
            // the length of the other, since that number of insertions
            // would be required.

            int n = first.Length, m = second.Length;
            if (n == 0) { return m; }
            if (m == 0) { return n; }


            // Rather than maintain an entire matrix (which would require O(n*m) space),
            // just store the current row and the next row, each of which has a length m+1,
            // so just O(m) space. Initialize the current row.

            int curRow = 0, nextRow = 1;
            int[][] rows = { new int[m + 1], new int[m + 1] };

            for (int j = 0; j <= m; ++j) { rows[curRow][j] = j; }

            // For each virtual row (since we only have physical storage for two)
            for (int i = 1; i <= n; ++i)
            {
                // Fill in the values in the row
                rows[nextRow][0] = i;
                for (int j = 1; j <= m; ++j)
                {
                    int dist1 = rows[curRow][j] + 1;
                    int dist2 = rows[nextRow][j - 1] + 1;
                    int dist3 = rows[curRow][j - 1] + (first[i - 1].Equals(second[j - 1]) ? 0 : 1);

                    rows[nextRow][j] = Math.Min(dist1, Math.Min(dist2, dist3));
                }

                // Swap the current and next rows
                if (curRow == 0) { curRow = 1; nextRow = 0; } else { curRow = 0; nextRow = 1; }
            }

            // Return the computed edit distance
            return rows[curRow][m];
        }
    }
}
