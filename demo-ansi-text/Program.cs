﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using CommonCore.CommandLine.Rendering;
using CommonCore.CommandLine.Rendering.Views;

namespace CommonCore.CommandLine.Demo.AnsiText
{
    static class Program
    {
        static void Main()
        {
            var screen = new Screen();

            screen.Add("ANSI STYLES:")
                .Add()
                .Add(new TextView("Supported styles are '",
                AnsiStyles.Bright.Open.Code, nameof(AnsiStyles.Bright), AnsiStyles.Bright.Close.Code,
                "', '",
                AnsiStyles.Faint.Open.Code, nameof(AnsiStyles.Faint), AnsiStyles.Faint.Close.Code,
                "', '",
                AnsiStyles.Underline.Open.Code, nameof(AnsiStyles.Underline), AnsiStyles.Underline.Close.Code,
                "', '",
                AnsiStyles.Reverse.Open.Code, nameof(AnsiStyles.Reverse), AnsiStyles.Reverse.Close.Code,
                "'."))
                .Add();

            //// "j k l m n q t u v w x"
            //var br = $"{Ansi.Escape}(0j{Ansi.Escape}(B";
            //var tr = $"{Ansi.Escape}(0k{Ansi.Escape}(B";
            //var tl = $"{Ansi.Escape}(0l{Ansi.Escape}(B";
            //var bl = $"{Ansi.Escape}(0m{Ansi.Escape}(B";
            //var cm = $"{Ansi.Escape}(0n{Ansi.Escape}(B";
            //var h = $"{Ansi.Escape}(0q{Ansi.Escape}(B";
            //var cl = $"{Ansi.Escape}(0t{Ansi.Escape}(B";
            //var cr = $"{Ansi.Escape}(0u{Ansi.Escape}(B";
            //var cb = $"{Ansi.Escape}(0v{Ansi.Escape}(B";
            //var ct = $"{Ansi.Escape}(0w{Ansi.Escape}(B";
            //var v = $"{Ansi.Escape}(0x{Ansi.Escape}(B";

            //screen.Add("ANSI LINE BORDERS:")
            //    .Add()
            //    .Add(new TextView($"{Ansi.Escape}(0", "j k l m n q t u v w x", $"{Ansi.Escape}(B"))
            //    .Add()
            //    .Add(new TextView(tl, h, ct, h, tr))
            //    .Add(new TextView(v, "A", v, "B", v))
            //    .Add(new TextView(v, h, cm, h, v))
            //    .Add(new TextView(v, "C", v, "D", v))
            //    .Add(new TextView(bl, h, cb, h, br))
            //    .Add()
            //    ;

            screen.Add("ANSI FOREGROUND COLORS:")
                .Add();

            var foreTable = new TableView<KeyValuePair<string, Ansi>>(ForeColorDict)
            { Prefix = "| ", Delimiter = " | ", Suffix = " |" };
            foreTable.AddColumn(pair => new TextView(pair.Value, pair.Key, AnsiForeColors.Default), "Color");
            foreTable.AddColumn(pair => new TextView(pair.Value, AnsiStyles.Bright.Open, pair.Key, AnsiStyles.Bright.Close, AnsiForeColors.Default), "Bright");
            foreTable.AddColumn(pair => new TextView(pair.Value, AnsiStyles.Faint.Open, pair.Key, AnsiStyles.Faint.Close, AnsiForeColors.Default), "Faint");
            foreTable.AddColumn(pair => new TextView(pair.Value, AnsiStyles.Underline.Open, pair.Key, AnsiStyles.Underline.Close, AnsiForeColors.Default), "Underline");
            foreTable.AddColumn(pair => new TextView(pair.Value, AnsiStyles.Reverse.Open, pair.Key, AnsiStyles.Reverse.Close, AnsiForeColors.Default), "Reverse");
            screen.Add(foreTable)
                .Add();

            screen.Add("ANSI BACKGROUND COLORS:")
                .Add();

            var backTable = new TableView<KeyValuePair<string, Ansi>>(BackColorDict)
            { Prefix = "| ", Delimiter = " | ", Suffix = " |" };
            backTable.AddColumn(pair => new TextView(pair.Value, pair.Key, AnsiBackColors.Default), "Color");
            backTable.AddColumn(pair => new TextView(pair.Value, AnsiStyles.Bright.Open, pair.Key, AnsiStyles.Bright.Close, AnsiBackColors.Default), "Bright");
            backTable.AddColumn(pair => new TextView(pair.Value, AnsiStyles.Faint.Open, pair.Key, AnsiStyles.Faint.Close, AnsiBackColors.Default), "Faint");
            backTable.AddColumn(pair => new TextView(pair.Value, AnsiStyles.Underline.Open, pair.Key, AnsiStyles.Underline.Close, AnsiBackColors.Default), "Underline");
            backTable.AddColumn(pair => new TextView(pair.Value, AnsiStyles.Reverse.Open, pair.Key, AnsiStyles.Reverse.Close, AnsiBackColors.Default), "Reverse");
            screen.Add(backTable)
                .Add();

            screen.Render();
        }

        private static Dictionary<string, Ansi> BackColorDict { get; }
            = new Dictionary<string, Ansi>()
            {
                { nameof(AnsiBackColors.Black), AnsiBackColors.Black },
                { nameof(AnsiBackColors.Red), AnsiBackColors.Red },
                { nameof(AnsiBackColors.Green), AnsiBackColors.Green },
                { nameof(AnsiBackColors.Yellow), AnsiBackColors.Yellow },
                { nameof(AnsiBackColors.Blue), AnsiBackColors.Blue },
                { nameof(AnsiBackColors.Magenta), AnsiBackColors.Magenta },
                { nameof(AnsiBackColors.Cyan), AnsiBackColors.Cyan },
                { nameof(AnsiBackColors.White), AnsiBackColors.White },

                { nameof(AnsiBackColors.BrightBlack), AnsiBackColors.BrightBlack },
                { nameof(AnsiBackColors.BrightRed), AnsiBackColors.BrightRed },
                { nameof(AnsiBackColors.BrightGreen), AnsiBackColors.BrightGreen },
                { nameof(AnsiBackColors.BrightYellow), AnsiBackColors.BrightYellow },
                { nameof(AnsiBackColors.BrightBlue), AnsiBackColors.BrightBlue },
                { nameof(AnsiBackColors.BrightMagenta), AnsiBackColors.BrightMagenta },
                { nameof(AnsiBackColors.BrightCyan), AnsiBackColors.BrightCyan },
                { nameof(AnsiBackColors.BrightWhite), AnsiBackColors.BrightWhite },
            };

        private static Dictionary<string, Ansi> ForeColorDict { get; }
            = new Dictionary<string, Ansi>()
            {
                { nameof(AnsiForeColors.Black), AnsiForeColors.Black },
                { nameof(AnsiForeColors.Red), AnsiForeColors.Red },
                { nameof(AnsiForeColors.Green), AnsiForeColors.Green },
                { nameof(AnsiForeColors.Yellow), AnsiForeColors.Yellow },
                { nameof(AnsiForeColors.Blue), AnsiForeColors.Blue },
                { nameof(AnsiForeColors.Magenta), AnsiForeColors.Magenta },
                { nameof(AnsiForeColors.Cyan), AnsiForeColors.Cyan },
                { nameof(AnsiForeColors.White), AnsiForeColors.White },

                { nameof(AnsiForeColors.BrightBlack), AnsiForeColors.BrightBlack },
                { nameof(AnsiForeColors.BrightRed), AnsiForeColors.BrightRed },
                { nameof(AnsiForeColors.BrightGreen), AnsiForeColors.BrightGreen },
                { nameof(AnsiForeColors.BrightYellow), AnsiForeColors.BrightYellow },
                { nameof(AnsiForeColors.BrightBlue), AnsiForeColors.BrightBlue },
                { nameof(AnsiForeColors.BrightMagenta), AnsiForeColors.BrightMagenta },
                { nameof(AnsiForeColors.BrightCyan), AnsiForeColors.BrightCyan },
                { nameof(AnsiForeColors.BrightWhite), AnsiForeColors.BrightWhite },
            };
    }
}
