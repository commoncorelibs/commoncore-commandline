﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CommonCore.CommandLine.Rendering.Views.Spans
{
    public class ContainerSpan : ASpan, IReadOnlyList<ISpan>
    {
        public ContainerSpan(params ISpan[] spans)
        {
            if (spans is null) { throw new ArgumentNullException(nameof(spans)); }

            this._spans = new List<ISpan>(spans);
            this.RecalculateChildPositions();
        }

        public ContainerSpan(IEnumerable<ISpan> spans)
        {
            if (spans is null) { throw new ArgumentNullException(nameof(spans)); }

            this._spans = new List<ISpan>(spans);
            this.RecalculateChildPositions();
        }

        private readonly List<ISpan> _spans;

        public override int ContentLength => this._spans.Sum(span => span.ContentLength);

        public int Count => this._spans.Count;

        public ISpan this[int index] => this._spans[index];

        public IEnumerator<ISpan> GetEnumerator() => ((IEnumerable<ISpan>)this._spans).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)this._spans).GetEnumerator();

        public override void RecalculatePositions(ISpan parent, int start)
        {
            base.RecalculatePositions(parent, start);

            this.RecalculateChildPositions();
        }

        private void RecalculateChildPositions()
        {
            var start = this.Start;
            for (var i = 0; i < this._spans.Count; i++)
            {
                var span = this._spans[i];
                span.RecalculatePositions(this, start);
                start += span.ContentLength;
            }
        }

        public override void WriteTo(TextWriter writer, ERenderMode outputMode)
        {
            for (var index = 0; index < this._spans.Count; index++)
            { this._spans[index].WriteTo(writer, outputMode); }
        }

        public void Add(ISpan span)
        {
            if (span is null) { throw new ArgumentNullException(nameof(span)); }

            this._spans.Add(span);
            this.RecalculateChildPositions();
        }

        public void Add(string text)
        {
            this._spans.Add(new ContentSpan(text));
            this.RecalculateChildPositions();
        }
    }
}
