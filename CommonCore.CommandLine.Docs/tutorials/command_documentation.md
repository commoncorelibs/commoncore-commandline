﻿# Command Documentation

Along with generating command options and arguments from a method, the library can also generate organized help information for them from the method's standard [documentation comments](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/documentation-comments). This is why the `GenerateDocumentationFile` element should be included in the **csproj** file, the library needs the generated documentation file to parse for the application's help information.

All the standard comment doc tags are understood; the root elements: `summary`, `remarks`, `returns`, `param`, `typeparam`, `exception`, `permission`, and `seealso`; the block elements: `code`, `list` (bulleted, numbered, and table), and `para`; and the inline elements: `b`, `c`, `i`, `em`, `see`, `paramref`, and `typeparamref`. Root elements can contain text, block elements, and inline elements. Block elements are each handled slightly differently; `para` elements can contain text and inline elements; `list` elements come in various forms, but all contain `term` and/or `description` elements which are treated the same as `para` elements; `code` contents are given the bare minimum of processing, preserving any leading space and leaving any contained xml intact. Inline elements can contain only text.

There is one unique special case. The `example` element can be nested within any root tag and is treated as a root tag for purposes of what it can contain, but it cannot be declared as a root tag, nor can one `example` be nested within another `example`. This handling is clunky and will hopefully be addressed in a future version.

```csharp
/// <summary>
/// This is an example application showing of some features of <c>CommonCore.CommandLine</c>.
/// </summary>
/// <param name="console">Magic parameter. Ignored by help generation.</param>
/// <param name="word">What is the word? By default, bird is the word.</param>
/// <param name="numbers">Space-separated list of integer numbers.</param>
/// <param name="output">Write output to the given path instead of stdout.</param>
/// <param name="verbose">Print additional information.</param>
[CommandEntryPoint]
static void Main(IConsole console,
    [Argument] string word = "bird", [Argument] int[] numbers = null,
    FileInfo output = null, bool verbose = false)
{
    console.Out.WriteLine("Hello World!!");
    console.Out.WriteLine($"{nameof(word)}:    {word ?? "null"}");
    console.Out.WriteLine($"{nameof(numbers)}: {(numbers is null ? "null" : string.Join(", ", numbers))}");
    console.Out.WriteLine($"{nameof(output)}: {output?.FullName ?? "null"}");
    console.Out.WriteLine($"{nameof(verbose)}: {verbose}");
}
```
