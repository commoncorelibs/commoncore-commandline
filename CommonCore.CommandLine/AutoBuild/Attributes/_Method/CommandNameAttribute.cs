﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using CommonCore.CommandLine.Exceptions;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="CommandNameAttribute"/> <c>class</c>
    /// provides a way to manually override a command's auto-generated name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class CommandNameAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandNameAttribute"/> <c>class</c>.
        /// </summary>
        /// <param name="name">The desired name of the command.</param>
        public CommandNameAttribute(string name)
        {
            Thrower.If.IsEmpty(name, nameof(name)).Throw();

            this.Name = name;
        }

        /// <summary>
        /// Gets the desired name of the command.
        /// </summary>
        public string Name { get; }
    }
}
