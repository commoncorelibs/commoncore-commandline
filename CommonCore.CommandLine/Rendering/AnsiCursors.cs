﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerStepThrough]
    public static class AnsiCursors
    {
        public static AnsiPair Blink { get; } = AnsiPair.FromPrivate('h', 12, 'l', 12);

        public static AnsiPair Visible { get; } = AnsiPair.FromPrivate('h', 25, 'l', 25);

        public static Ansi SavePosition { get; } = Ansi.FromBracketless('s');

        public static Ansi RestorePosition { get; } = Ansi.FromBracketless('u');
    }
}
