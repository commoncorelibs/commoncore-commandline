﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;

namespace CommonCore.CommandLine.Extensions
{
    public static class OptionExtensions
    {
        public static Option SetType(this Option option, Type type)
        {
            option.Argument = new Argument() { ArgumentType = type };
            return option;
        }

        public static string ToHelpInvocation(this IOption option)
        {
            var invocation = option.ToInvocation();
            var descriptor = option.ToDescriptor();
            return descriptor is null ? invocation : $"{invocation} <{descriptor}>";
        }

        public static string ToHelpDescription(this IOption option)
        {
            var desc = option.ToSummary();
            var meta = option.ToMeta();
            if (desc is null && meta is null) { return null; }
            return desc is null ? meta : meta is null ? desc : $"[{meta}] {desc}";
        }

        public static string ToInvocation(this IOption option)
            => string.Join(", ", option.RawAliases.Select(r => CCUtils.SplitPrefix(r))
                .OrderBy(t => t.prefix, StringComparer.OrdinalIgnoreCase)
                .ThenBy(t => t.alias, StringComparer.OrdinalIgnoreCase)
                .GroupBy(t => t.alias)
                .Select(g => g.First())
                .Select(t => $"{t.prefix}{t.alias}"));

        public static string ToDescriptor(this IOption option)
            => option.Argument.ToDescriptor();

        public static string ToMeta(this IOption option)
        {
            var hint = option.ToDefaultHint();
            var list = new List<string>();
            if (option.Required) { list.Add("REQUIRED"); }
            //if (option.IsExclusive()) { list.Add("EXCLUSIVE"); }
            if (hint != null) { list.Add($"default: {hint}"); }
            return list.Count == 0 ? null : string.Join(", ", list);
        }

        public static string ToDefaultHint(this IOption option)
            => option.Argument.ToDefaultHint();
    }
}
