﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.CommandLine.Binding;
using System.CommandLine.Suggestions;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using CommonCore.CommandLine.AutoBuild.Factories;
using CommonCore.CommandLine.Help;
using CommonCore.CommandLine.Reflection;
using CommonCore.CommandLine.Suggestions;

namespace CommonCore.CommandLine
{
    public class Services : IServiceProvider, IEnumerable<Type>
    {
        static Services()
        {
            Services.Suggestions.Set<bool>(SuggestSource.Create(new[] { bool.TrueString.ToLower(), bool.FalseString.ToLower() }));
            Services.Suggestions.Set<bool?>(SuggestSource.Create(new[] { bool.TrueString.ToLower(), bool.FalseString.ToLower() }));
            Services.Suggestions.Set<FileInfo>(SuggestSource.Create(new[] { "file" }));
            Services.Suggestions.Set<DirectoryInfo>(SuggestSource.Create(new[] { "directory" }));

            HelpDocReader = HelpDocReader.Load(Path.ChangeExtension(AssemblyHelper.GetAssembly().Location, "xml"));
        }

        public static Services Instance { get; set; } = new Services();

        public static TypeSuggestionProvider Suggestions => TypeSuggestionProvider.Instance;

        public static TypeConversionProvider Converters => TypeConversionProvider.Instance;

        public static TypeParseProvider Parsers => TypeParseProvider.Instance;

        public static HelpDocReader HelpDocReader { get; set; }

        public Services()
        {
            this.Dict = new Dictionary<Type, Func<IServiceProvider, object>>();
            this.Set(this);
        }

        public Services(BindingContext binding)
        {
            if (binding is null) { throw new ArgumentNullException(nameof(binding)); }
            var provider = ReflectionHelper.GetPropertyValue<IServiceProvider>(binding, "ServiceProvider");
            var dict = ReflectionHelper.GetFieldValue<Dictionary<Type, Func<IServiceProvider, object>>>(provider, "_services");
            this.Dict = dict ?? throw new InvalidOperationException("BindingContext.ServiceProvider._services field not found.");
        }

        protected Dictionary<Type, Func<IServiceProvider, object>> Dict { get; }

        public int Count => this.Keys.Count;

        public IReadOnlyCollection<Type> Keys => this.Dict.Keys;

        public object this[Type key] => this.Get(key);

        public object this[int index] => this.Get(this.Keys.ElementAt(index));

        public bool ContainsKey<TKey>() => this.Dict.ContainsKey(typeof(TKey));

        public bool ContainsKey(Type key) => this.Dict.ContainsKey(key);

        public bool TryGetValue<TKey>(out TKey value)
        {
            bool result = this.Dict.TryGetValue(typeof(TKey), out Func<IServiceProvider, object> func);
            value = result ? (TKey)func(this) : default;
            return result;
        }

        public bool TryGetValue(Type key, out object value)
        {
            bool result = this.Dict.TryGetValue(key, out Func<IServiceProvider, object> func);
            value = result ? func(this) : default;
            return result;
        }

        public TKey Get<TKey>() => (TKey)(this.Dict.TryGetValue(typeof(TKey), out var factory) ? factory(this) : null);

        public object Get(Type key) => this.Dict.TryGetValue(key, out var factory) ? factory(this) : null;

        public void Set<TKey>(TKey service) => this.Dict[typeof(TKey)] = provider => service;

        public void Set<TKey>(object service) => this.Dict[typeof(TKey)] = provider => service;

        public void Set(Type key, object service) => this.Dict[key] = provider => service;

        public void Set<TKey>(Func<IServiceProvider, object> factory) => this.Dict[typeof(TKey)] = factory;

        public void Set(Type key, Func<IServiceProvider, object> factory) => this.Dict[key] = factory;

        public void Remove<TKey>() => this.Dict.Remove(typeof(TKey));

        public void Remove(Type key) => this.Dict.Remove(key);

        public void CopyTo(Services other)
        { foreach (var pair in this.Dict) { other.Set(pair.Key, pair.Value); } }

        object IServiceProvider.GetService(Type key) => this.Get(key);

        public IEnumerator<Type> GetEnumerator() => this.Keys.GetEnumerator();

        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)this.Keys).GetEnumerator();
    }

    public static class ServicesExtensions
    {
        private static Dictionary<BindingContext, Services> _providers = new Dictionary<BindingContext, Services>();
        public static Services GetServices(this BindingContext self)
            => _providers.TryGetValue(self, out var provider) ? provider : (_providers[self] = new Services(self));
    }
}
