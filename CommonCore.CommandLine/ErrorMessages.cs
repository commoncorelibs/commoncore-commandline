﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.IO;
using System.Linq;
using CommonCore.CommandLine.Extensions;

namespace CommonCore.CommandLine
{
    public class ErrorMessages : ValidationMessages
    {
        public static ErrorMessages Instance { get; set; } = new ErrorMessages();

        public override string UnrecognizedArgument(string unrecognizedArg, IReadOnlyCollection<string> allowedValues)
        {
            return $"Argument '{unrecognizedArg}' not recognized. Must be one of: {string.Join(",", allowedValues.Select(value => $"'{value}'"))}";
        }
    }
}
