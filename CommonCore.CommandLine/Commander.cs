﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Binding;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.CommandLine.Parsing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommonCore.CommandLine.Invocation;
using CommonCore.CommandLine.IO;
using CommonCore.CommandLine.Reflection;
using CommonCore.CommandLine.Rendering;
using CommonCore.CommandLine.Symbols;
using SCLProcess = System.CommandLine.Invocation.Process;

namespace CommonCore.CommandLine
{
    public class Commander
    {
        public static int InstanceCount { get; private set; }

        public static Commander Create(CoreCommand command = null)
            => new Commander(command);

        protected Commander(CoreCommand command = null)
        {
            this.InstanceId = InstanceCount++;
            this.Command = command ?? new CoreCommand(RootCommand.ExecutableName);
            this.Services = Services.Instance;
            this.Services.Set(this);
            this.Middlewares.Add(new Middleware(EMiddlewareOrder.Initialize, (context) => this.Services.CopyTo(context.BindingContext.GetServices())));
        }

        public int InstanceId { get; }

        protected List<Middleware> Middlewares { get; } = new List<Middleware>();

        public CoreCommand Command { get; }

        public Services Services { get; set; }

        public ERenderMode OutputMode { get; set; }

        public int OutputWidth { get; set; }

        public bool EnableDirectives { get; set; }

        public bool EnablePosixBundling { get; set; }

        public ResponseFileHandling ResponseFileHandling { get; set; } = ResponseFileHandling.Disabled;

        internal ValidationMessages ValidationMessages { get; set; }
        //= ValidationMessages.Instance;
        = ErrorMessages.Instance;

        public Parser Build()
        {
            var rootCommand = this.Command;

            var wares = Enumerable.Empty<Middleware>()
                .Concat(this.Middlewares)
                .Concat(rootCommand.Middlewares)
                .Concat(rootCommand.Children.OfType<CoreCommand>().SelectMany(c => c.Middlewares))
                .OrderBy(w => w.Order)
                .Select(w => w.Handler)
                .ToArray()
                ;

            var parser = new Parser(
            new CommandLineConfiguration(
                new[] { rootCommand },
                enablePosixBundling: this.EnablePosixBundling,
                enableDirectives: this.EnableDirectives,
                validationMessages: this.ValidationMessages,
                responseFileHandling: this.ResponseFileHandling,
                middlewarePipeline: wares,
                helpBuilderFactory: null));

            //this.Command.ImplicitParser = parser;
            ReflectionHelper.SetPropertyValue(this.Command, "ImplicitParser", parser);

            return parser;
        }

        public Commander AddMiddleware(EMiddlewareOrder order, InvocationMiddleware handler)
        {
            this.Middlewares.Add(new Middleware(order, 0, handler));
            return this;
        }
    }

    public static class CommanderExtensions
    {
        public static Commander AddArgument(this Commander commander, Argument argument)
        { commander.Command.AddArgument(argument); return commander; }

        public static Commander AddArgument(this Commander commander, params Argument[] arguments)
        { foreach (var argument in arguments) { commander.Command.AddArgument(argument); } return commander; }

        public static Commander AddArgument(this Commander commander, IEnumerable<Argument> arguments)
        { foreach (var argument in arguments) { commander.Command.AddArgument(argument); } return commander; }

        public static Commander AddOption(this Commander commander, Option option)
        { commander.Command.AddOption(option); return commander; }

        public static Commander AddOption(this Commander commander, params Option[] options)
        { foreach (var option in options) { commander.Command.AddOption(option); } return commander; }

        public static Commander AddOption(this Commander commander, IEnumerable<Option> options)
        { foreach (var option in options) { commander.Command.AddOption(option); } return commander; }

        public static Commander AddCommand(this Commander commander, Command command)
        { commander.Command.AddCommand(command); return commander; }

        public static Commander AddCommand(this Commander commander, params Command[] commands)
        { foreach (var command in commands) { commander.Command.AddCommand(command); } return commander; }

        public static Commander AddCommand(this Commander commander, IEnumerable<Command> commands)
        { foreach (var command in commands) { commander.Command.AddCommand(command); } return commander; }

        //public static Commander AddCommand<TDynamicCommand>(this Commander commander)
        //    where TDynamicCommand : ADynamicCommand
        //{ Activator.CreateInstance(typeof(TDynamicCommand), commander.Command); return commander; }

        public static Commander AddGlobalOption(this Commander commander, Option option)
        { commander.Command.AddGlobalOption(option); return commander; }



        public static Commander InitService<TKey>(this Commander commander, object service)
        { commander.Services.Set<TKey>(service); return commander; }

        public static Commander InitService<TKey>(this Commander commander, TKey service)
        { commander.Services.Set<TKey>(service); return commander; }

        public static Commander InitService<TKey>(this Commander commander, Func<IServiceProvider, object> factory)
        { commander.Services.Set<TKey>(factory); return commander; }

        public static Commander InitService(this Commander commander, Type key, Func<IServiceProvider, object> factory)
        { commander.Services.Set(key, factory); return commander; }



        public static Commander EnableDirectives(this Commander commander, bool value)
        { commander.EnableDirectives = value; return commander; }

        public static Commander EnablePosixBundling(this Commander commander, bool value)
        { commander.EnablePosixBundling = value; return commander; }

        public static Commander Set(this Commander commander, ERenderMode outputMode)
        { commander.OutputMode = outputMode; return commander; }

        public static Commander Set(this Commander commander, int outputWidth)
        { commander.OutputWidth = outputWidth > 0 ? outputWidth : 0; return commander; }

        public static Commander Set(this Commander commander, ResponseFileHandling responseFileHandling)
        { commander.ResponseFileHandling = responseFileHandling; return commander; }

        public static Commander Set(this Commander commander, ValidationMessages validationMessages)
        { commander.ValidationMessages = validationMessages; return commander; }

        public static Commander ApplyDefaults(this Commander commander, ERenderMode mode = ERenderMode.Ansi, int width = 100)
            => commander
            .EnableDirectives(true)
            .EnablePosixBundling(true)
            .Set(mode)
            .Set(width)
            .Set(ResponseFileHandling.Disabled)
            ;

        public static Commander InitDefaults(this Commander commander)
            => commander
            .InitService<IConsole>(new SystemConsoleTerminal(new SystemConsole()) { OutputMode = commander.OutputMode })
            .InitService<IConsoleRenderer>(new CommanderRenderer(commander.OutputMode, commander.OutputWidth, true))
            ;

        public static Commander RegisterDefaults(this Commander commander)
            => commander
            .RegisterConsoleFactory(commander.Services.Get<IConsole>())
            .RegisterDebugDirective()
            .RegisterParseDirective()
            .RegisterSuggestDirective()
            .RegisterTypoCorrections()
            .RegisterWithDotnetSuggest()
            .RegisterCancelOnProcessTermination()
            //.RegisterPreferVirtualTerminal()
            ;

        public static Commander RegisterConsoleFactory(this Commander commander, IConsole console)
            => commander.RegisterConsoleFactory(_ => console);

        public static Commander RegisterConsoleFactory(this Commander commander, Func<BindingContext, IConsole> createConsole)
        {
            commander.AddMiddleware(EMiddlewareOrder.SetupConsole, async(context, next) =>
            {
                // Note: Not only is ConsoleFactory prop internalized,
                // but so is the IConsoleFactory interface! So, can't
                // derive around this and have to reflect it all.
                //context.BindingContext.ConsoleFactory = new AnonymousConsoleFactory(createConsole);
                var type = context.GetType().Assembly.GetType("System.CommandLine.Binding.AnonymousConsoleFactory");
                ReflectionHelper.SetPropertyValue(context.BindingContext, "ConsoleFactory", Activator.CreateInstance(type, createConsole));

                await next(context);
            });

            return commander;
        }

        public static Commander RegisterDebugDirective(this Commander commander)
        {
            if (!commander.EnableDirectives) { throw new InvalidOperationException("Register directive 'debug' failed because directives are not enabled."); }

            commander.AddMiddleware(EMiddlewareOrder.DebugDirective, async(context, next) =>
            {
                if (context.ParseResult.Directives.Contains("debug"))
                {
                    var process = System.Diagnostics.Process.GetCurrentProcess();
                    context.Console.Out.WriteLine($"Attach your debugger to process {process.Id} ({process.ProcessName}).");
                    while (!System.Diagnostics.Debugger.IsAttached) { await Task.Delay(500); }
                }
                // Note: Should be able to remove this, since next is called after each ware exits.
                await next(context);
            });
            return commander;
        }

        public static Commander RegisterParseDirective(this Commander commander)
        {
            if (!commander.EnableDirectives) { throw new InvalidOperationException("Register directive 'parse' failed because directives are not enabled."); }

            commander.AddMiddleware(EMiddlewareOrder.ParseDirective, async(context, next) =>
            {
                if (!context.ParseResult.Directives.Contains("parse")) { await next(context); }
                else { context.InvocationResult = new ParseDirectiveInvocationResult(); }
            });
            return commander;
        }

        public static Commander RegisterSuggestDirective(this Commander commander)
        {
            if (!commander.EnableDirectives) { throw new InvalidOperationException("Register directive 'suggest' failed because directives are not enabled."); }

            commander.AddMiddleware(EMiddlewareOrder.SuggestDirective, async(context, next) =>
            {
                if (!context.ParseResult.Directives.TryGetValues("suggest", out var values))
                { await next(context); }
                else
                {
                    var value = values.FirstOrDefault();
                    int position;
                    if (value != null) { position = int.Parse(value); }
                    else { position = ReflectionHelper.GetPropertyValue<string>(context.ParseResult, "RawInput")?.Length ?? 0; }
                    context.InvocationResult = new SuggestDirectiveInvocationResult(position);
                }
            });
            return commander;
        }

        public static Commander RegisterTypoCorrections(this Commander commander, int maxLevenshteinDistance = 3)
        {
            commander.AddMiddleware(EMiddlewareOrder.SetupTypoCorrection, async(context, next) =>
            {
                if (context.ParseResult.UnmatchedTokens.Any()
                    && context.ParseResult.CommandResult.Command.TreatUnmatchedTokensAsErrors)
                {
                    var typoCorrection = new TypoCorrection(maxLevenshteinDistance);
                    //var type = context.GetType().Assembly.GetType("System.CommandLine.Invocation.TypoCorrection");
                    //var typoCorrection = Activator.CreateInstance(type, maxLevenshteinDistance);

                    typoCorrection.ProvideSuggestions(context.ParseResult, context.Console);
                    //ReflectionHelper.CallMethodInfo(typoCorrection, "ProvideSuggestions", context.ParseResult, context.Console);
                }
                await next(context);
            });

            return commander;
        }

        public static Commander RegisterWithDotnetSuggest(this Commander commander)
        {
            commander.AddMiddleware(EMiddlewareOrder.SetupDotnetSuggest, async(context, next) =>
            {
                var feature = new FeatureRegistration("dotnet-suggest-registration");

                await feature.EnsureRegistered(async () =>
                {
                    try
                    {
                        var currentProcessFullPath = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                        var currentProcessFileNameWithoutExtension = Path.GetFileNameWithoutExtension(currentProcessFullPath);

                        var stdOut = new StringBuilder();
                        var stdErr = new StringBuilder();

                        var dotnetSuggestProcess = SCLProcess.StartProcess(
                            command: "dotnet-suggest",
                            args: $"register --command-path \"{currentProcessFullPath}\" --suggestion-command \"{currentProcessFileNameWithoutExtension}\"",
                            stdOut: value => stdOut.Append(value),
                            stdErr: value => stdOut.Append(value));

                        await dotnetSuggestProcess.CompleteAsync();

                        return $"{dotnetSuggestProcess.StartInfo.FileName} exited with code {dotnetSuggestProcess.ExitCode}{Environment.NewLine}OUT:{Environment.NewLine}{stdOut}{Environment.NewLine}ERR:{Environment.NewLine}{stdErr}";
                    }
                    catch (Exception exception)
                    {
                        return $"Exception during dotnet-suggest registration:{Environment.NewLine}{exception}";
                    }
                });

                await next(context);
            });
            return commander;
        }

        public static Commander RegisterCancelOnProcessTermination(this Commander commander)
        {
            commander.AddMiddleware(EMiddlewareOrder.SetupCancellation, async(context, next) =>
            {
                bool cancellationHandlingAdded = false;
                ManualResetEventSlim blockProcessExit = null;
                ConsoleCancelEventHandler consoleHandler = null;
                EventHandler processExitHandler = null;

                var info = context.GetType().GetEvent("CancellationHandlingAdded");
                if (info != null)
                {
                    Action<CancellationTokenSource> handler = (CancellationTokenSource cts) =>
                    {
                        blockProcessExit = new ManualResetEventSlim(initialState: false);
                        cancellationHandlingAdded = true;
                        consoleHandler = (_, args) =>
                        {
                            cts.Cancel();
                        // Stop the process from terminating.
                        // Since the context was cancelled, the invocation should
                        // finish and Main will return.
                        args.Cancel = true;
                        };
                        processExitHandler = (_1, _2) =>
                        {
                            cts.Cancel();
                        // The process exits as soon as the event handler returns.
                        // We provide a return value using Environment.ExitCode
                        // because Main will not finish executing.
                        // Wait for the invocation to finish.
                        blockProcessExit.Wait();
                            Environment.ExitCode = context.ResultCode;
                        };
                        Console.CancelKeyPress += consoleHandler;
                        AppDomain.CurrentDomain.ProcessExit += processExitHandler;
                    };
                    info.AddEventHandler(context, handler);
                }

                try { await next(context); }
                finally
                {
                    if (cancellationHandlingAdded)
                    {
                        Console.CancelKeyPress -= consoleHandler;
                        AppDomain.CurrentDomain.ProcessExit -= processExitHandler;
                        blockProcessExit!.Set();
                    }
                }
            });
            return commander;
        }

        //public static Commander RegisterPreferVirtualTerminal(this Commander commander)
        //{
        //    commander.RegisterConsoleFactory(context
        //        => context.Console.GetTerminal(__preferVirtualTerminal(context), commander.OutputMode)
        //        ?? new SystemConsoleTerminal(new SystemConsole()) { OutputMode = commander.OutputMode });
        //    return commander;

        //    static bool __preferVirtualTerminal(BindingContext context)
        //    {
        //        if (context.ParseResult.Directives.TryGetValues("enable-vt", out var trueOrFalse))
        //        { if (bool.TryParse(trueOrFalse.FirstOrDefault(), out var pvt)) { return pvt; } }
        //        return true;
        //    }
        //}

        //public static Commander LoadDescription(this Commander commander, MethodInfo method, string xmlDocsFilePath = null)
        //{
        //    if (method is null) { throw new ArgumentNullException(nameof(method)); }
        //    //var reader = HelpDocReader.Load(xmlDocsFilePath ?? Path.ChangeExtension(method.DeclaringType.Assembly.Location, "xml"));
        //    var reader = Services.HelpDocReader;
        //    if (reader != null)
        //    {
        //        var help = new HelpInfo();
        //        if (reader.TryParseHelp(method, help))
        //        {
        //            var command = commander.Command;
        //            command.Description = help.SummaryViews?.FirstOrDefault()?.ToString();
        //            if (help.ParamViewPairs != null)
        //            {
        //                var options = command.Options.ToArray();
        //                var arguments = command.Arguments.ToArray();

        //                foreach (var pair in help.ParamViewPairs)
        //                {
        //                    Symbol symbol
        //                        = (Symbol)options.FirstOrDefault(o => pair.Name == o.Name || o.HasAlias(pair.Name))
        //                        ?? arguments.FirstOrDefault(a => pair.Name == a.Name || a.HasAlias(pair.Name));
        //                    if (symbol != null) { symbol.Description = pair.Views?[0]?.ToString(); }
        //                }
        //            }
        //        }
        //        commander.Command.SetHelpInfo(help);
        //    }
        //    return commander;
        //}
    }
}
