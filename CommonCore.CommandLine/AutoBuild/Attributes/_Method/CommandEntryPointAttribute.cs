﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="CommandEntryPointAttribute"/> <c>class</c> is used to explicitly mark a method
    /// as the entry point for the auto-build system. When active, the system will throw an exception
    /// if no methods are marked with the attribute or if more than one marked method is found. An
    /// exception will also be thrown if the marked method is not <c>static</c> or if it has a return type
    /// other than <see cref="void"/>, <see cref="int"/>, <see cref="System.Threading.Tasks.Task"/>,
    /// or <see cref="System.Threading.Tasks.Task{T}"/> with generic type of <see cref="int"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class CommandEntryPointAttribute : Attribute { }
}
