﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="AppMITLicense"/> <c>convienence</c> <c>class</c> provides an
    /// implementation of <see cref="AppLicenseAttribute"/> populated with data for
    /// the MIT License (MIT): https://opensource.org/licenses/MIT.
    /// 
    /// <para>
    /// The attribute should be applied to the implementing console application's main <c>class</c>,
    /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> <c>method</c>.
    /// This is generally the <c>Program</c> class.
    /// </para>
    /// </summary>
    public class AppMITLicense : AppLicenseAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppMITLicense"/> <c>class</c>
        /// populated with information related to the implementing console application.
        /// </summary>
        public AppMITLicense()
        {
            this.LicenseName = "MIT License";
            this.LicenseId = "MIT";
            this.LicenseUrl = "https://opensource.org/licenses/MIT";
            this.LicenseText =
@"The MIT License (MIT)

{Copyright}

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the ""Software""), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next paragraph) shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED ""AS IS"", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.";
        }
    }
}
