﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerDisplay("{X},{Y},{Width},{Height}")]
    public readonly struct Region : IEquatable<Region>
    {
        public static Region Zero { get; } = default;

        public Region(Point point, Size size)
            : this(point.X, point.Y, size.Width, size.Height) { }

        public Region(Point point, int? width, int? height)
            : this(point.X, point.Y, width, height) { }

        public Region(int x, int y, Size size)
            : this(x, y, size.Width, size.Height) { }

        public Region(int x, int y, int? width = null, int? height = null, bool isOverwrittenOnRender = true)
        {
            if (x < 0) { throw new ArgumentOutOfRangeException(nameof(x)); }
            if (y < 0) { throw new ArgumentOutOfRangeException(nameof(y)); }
            if (width < 0) { throw new ArgumentOutOfRangeException(nameof(width)); }
            if (height < 0) { throw new ArgumentOutOfRangeException(nameof(height)); }

            this.X = x;
            this.Y = y;
            this.Width = width ?? (Console.IsOutputRedirected ? 100 : Console.WindowWidth);
            this.Height = height ?? (Console.IsOutputRedirected ? 100 : Console.WindowHeight);

            this.IsOverwrittenOnRender = isOverwrittenOnRender;
        }

        public readonly int X { get; }

        public readonly int Y { get; }

        public readonly int Height { get; }

        public readonly int Width { get; }

        public readonly int Left => this.X;

        public readonly int Top => this.Y;

        public readonly int Right => this.X + this.Width - 1;

        public readonly int Bottom => this.Y + this.Height - 1;

        public readonly bool IsOverwrittenOnRender { get; }

        public readonly override string ToString()
            => $"[{this.X}, {this.Y}, {this.Width}, {this.Height}]";

        public readonly override int GetHashCode()
            => HashCode.Combine(this.X, this.Y, this.Height, this.Width, this.IsOverwrittenOnRender);

        public readonly override bool Equals(object obj)
            => obj is Region region && this.Equals(region);

        public readonly bool Equals(Region other)
            => this.X == other.X && this.Y == other.Y
            && this.Height == other.Height && this.Width == other.Width
            && this.IsOverwrittenOnRender == other.IsOverwrittenOnRender;

        public static bool operator ==(Region left, Region right) => left.Equals(right);

        public static bool operator !=(Region left, Region right) => !left.Equals(right);
    }
}
