﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Reflection;

namespace CommonCore.CommandLine.Reflection
{
    /// <summary>
    /// Helper class defining commonly used properties and methods for
    /// dealing with assembly information.
    /// </summary>
    public static class AssemblyHelper
    {
        /// <summary>
        /// Gets the process executable in the default application domain. In other application
        /// domains, this is the first executable that was executed by
        /// <see cref="System.AppDomain.ExecuteAssembly(string)"/>.
        /// If that fails, then the <see cref="System.Reflection.Assembly"/> of the method that
        /// invoked the currently executing method.
        /// </summary>
        /// <returns>Process executable assembly, first executed assembly, or calling assembly, in that order.</returns>
        public static Assembly GetAssembly()
            => Assembly.GetEntryAssembly() ?? Assembly.GetCallingAssembly();
    }
}
