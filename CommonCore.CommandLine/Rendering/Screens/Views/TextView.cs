﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering.Views
{
    public class TextView : AView
    {
        public TextView()
            : this(new ContentSpan(string.Empty)) { }

        public TextView(string content)
            : this(content.StartsWith(Ansi.Escape) ? (ISpan)new ControlSpan(new Ansi(content)) : new ContentSpan(content)) { }

        public TextView(params string[] contents)
            : this(contents.Select(content => content.StartsWith(Ansi.Escape) ? (ISpan)new ControlSpan(new Ansi(content)) : new ContentSpan(content))) { }

        public TextView(IEnumerable<string> contents)
            : this(contents.Select(content => new ContentSpan(content))) { }

        public TextView(ISpan span)
            => this.Span = span ?? throw new ArgumentNullException(nameof(span));

        public TextView(params ISpan[] spans)
            => this.Span = new ContainerSpan(spans ?? throw new ArgumentNullException(nameof(spans)));

        public TextView(IEnumerable<ISpan> spans)
            => this.Span = new ContainerSpan(spans ?? throw new ArgumentNullException(nameof(spans)));

        public ISpan Span { get; set; }

        public override string ToString() => this.Span?.ToString();

        public override void Render(IConsoleRenderer renderer, Region region = default)
        {
            if (renderer is null) { throw new ArgumentNullException(nameof(renderer)); }

            if (this.Span is null) { return; }
            renderer.Render(this.Span, region);
        }

        public override Size Measure(IConsoleRenderer renderer, Size maxSize)
        {
            if (renderer is null) { throw new ArgumentNullException(nameof(renderer)); }

            return this.Span is null ? new Size(0, 0) : renderer.Measure(this.Span, maxSize);
        }

        protected void Observe<T>(IObservable<T> observable, Func<T, FormattableString> formatProvider)
        {
            if (observable is null) { throw new ArgumentNullException(nameof(observable)); }
            if (formatProvider is null) { throw new ArgumentNullException(nameof(formatProvider)); }

            observable.Subscribe(new Observer<T>(this, formatProvider));
        }

        public static TextView FromObservable<T>(IObservable<T> observable, Func<T, FormattableString> formatProvider = null)
        {
            var view = new TextView();
            view.Observe(observable, formatProvider ?? (x => $"{x}"));
            return view;
        }

        // Note: Until we find a need for these, best to just comment them out.

        //internal static ContentView Create(object content, TextSpanFormatter formatter)
        //{
        //    if (content is null) { throw new ArgumentNullException(nameof(content)); }

        //    // Note: Why cast it as dynamic? Just call ToString().
        //    //return CreateView((dynamic)content, formatter);
        //    return CreateView(content.ToString(), formatter);
        //}

        //private static ContentView CreateView(string stringContent, TextSpanFormatter _)
        //    => new ContentView(stringContent);

        //private static ContentView CreateView(TextSpan span, TextSpanFormatter _)
        //    => new ContentView(span);

        //private static ContentView CreateView<T>(IObservable<T> observable, TextSpanFormatter _)
        //    => FromObservable(observable);

        //private static ContentView CreateView(object value, TextSpanFormatter formatter)
        //    => new ContentView(formatter.Format(value));

        // TODO: Look into observables. Haven't dealt much with
        // them and need to learn more before mucking about.
        protected class Observer<T> : IObserver<T>
        {
            private readonly TextView _contentView;
            private readonly Func<T, FormattableString> _formatProvider;
            private readonly TextSpanFormatter _textSpanFormatter = new TextSpanFormatter();

            public Observer(TextView contentView, Func<T, FormattableString> formatProvider)
            {
                this._contentView = contentView;
                this._formatProvider = formatProvider;
            }

            public void OnCompleted() { }

            public void OnError(Exception error) { }

            public void OnNext(T value)
            {
                this._contentView.Span = this._textSpanFormatter.ParseToSpan(this._formatProvider(value));
                this._contentView.OnUpdated();
            }
        }
    }
}
