﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="EnableDefaultErrorReportingAttribute"/> <c>class</c>
    /// provides a way to signal the system to enable the default error reporting
    /// and that the default error reporting screen should be used to display any
    /// parsing errors to the user. This screen is very similar to the default help
    /// screen, but with encountered parsing errors listed.
    /// 
    /// <para>
    /// The attribute should be applied to the implementing console application's main <c>class</c>,
    /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> <c>method</c>.
    /// This is generally the <c>Program</c> class.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class EnableDefaultErrorReportingAttribute : Attribute { }
}
