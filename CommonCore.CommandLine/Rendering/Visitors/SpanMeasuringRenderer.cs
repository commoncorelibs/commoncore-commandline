﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using CommonCore.CommandLine.Extensions;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering.Visitors
{
    public class SpanMeasuringRenderer : ASpanVisitor
    {
        public SpanMeasuringRenderer(Region region = default)
        {
            this.Region = region;
        }

        public bool LastSpanEndedWithWhitespace { get; protected set; }

        public int PositionOnLine
        {
            get => this._positionOnLine;
            protected set
            {
                if (value > this.Width) { this.Width = value; }
                this._positionOnLine = value;
            }
        }
        private int _positionOnLine;

        public int Width { get; protected set; }

        public int Height => this.LinesWritten;

        public int LinesWritten { get; protected set; }

        public Region Region { get; }

        public override void VisitControlSpan(ControlSpan span) { }

        public override void VisitForegroundColorSpan(ForeColorSpan span) { }

        public override void VisitBackgroundColorSpan(BackColorSpan span) { }

        public override void VisitStyleSpan(StyleSpan span) { }

        public override void VisitCursorControlSpan(CursorControlSpan span) { }

        // Note: Any changes to logic in this method must
        // be mirrored in ASpanRenderer.
        public override void VisitContentSpan(ContentSpan span)
        {
            var text = span.ToString().AsMemory();

            // if text from the previous line was not truncated because the word was separated by an ANSI code, it should be truncated
            var skipWordRemainderFromPreviousLine =
                !this.LastSpanEndedWithWhitespace
                && this.PositionOnLine == 0
                && this.LinesWritten > 0
                && !text.Span.StartsWithWhitespace();

            foreach (var word in text.SplitForWrapping().SplitForWidth(this.RemainingWidthInRegion))
            {
                if (skipWordRemainderFromPreviousLine)
                {
                    skipWordRemainderFromPreviousLine = false;
                    continue;
                }

                if (word.IsNewLine()) { if (this.TryStartNewLine()) { this.FlushLine(); } }
                else if (!this.TryAppendWord(word.Span)) { break; }
            }
            this.LastSpanEndedWithWhitespace = text.Span.EndsWithWhitespace();

            //if (this.PositionOnLine > 0)
            //{
            //    this.TryStartNewLine();
            //    this.FlushLine();
            //}
        }

        protected override void Stop(ISpan span)
        { if (this.PositionOnLine > 0 || span.ContentLength == 0) { this.FlushLine(); } }

        private bool FilledRegionHeight => this.LinesWritten >= this.Region.Height;

        private int RemainingWidthInRegion => this.Region.Width - this.PositionOnLine;

        private void FlushLine()
        {
            this.LinesWritten++;
            this.PositionOnLine = 0;
        }

        private bool TryAppendWord(ReadOnlySpan<char> value)
        {
            // omit whitespace if it's at the beginning of the line
            if (this.PositionOnLine == 0 && value.IsWhiteSpace() && this.LinesWritten > 0)
            { return true; }

            if (!this.TryStartNewLine()) { return false; }

            if (value.Length > this.RemainingWidthInRegion)
            {
                var endTrimmed = value.TrimEnd();
                if (endTrimmed.Length <= this.RemainingWidthInRegion)
                { value = endTrimmed; }
                else
                {
                    var chars = value;
                    while (chars.Length > this.RemainingWidthInRegion)
                    {
                        var text = chars.Slice(0, this.RemainingWidthInRegion);
                        chars = chars.Slice(this.RemainingWidthInRegion);

                        //this._buffer.Append(text);
                        this.PositionOnLine += text.Length;

                        if (this.RemainingWidthInRegion <= 0)
                        { this.FlushLine(); }
                    }
                    if (chars.Length > 0)
                    {
                        //this._buffer.Append(chars);
                        this.PositionOnLine += chars.Length;

                        if (this.RemainingWidthInRegion <= 0)
                        { this.FlushLine(); }
                    }
                    //this.FlushLine();
                }
            }
            else
            {
                //this._buffer.Append(value);
                this.PositionOnLine += value.Length;

                if (this.RemainingWidthInRegion <= 0)
                { this.FlushLine(); }
            }

            return !this.FilledRegionHeight;
        }

        private bool TryStartNewLine() => !this.FilledRegionHeight;
    }
}
