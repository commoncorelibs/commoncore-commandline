﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="OptionAttribute"/> <c>class</c>
    /// provides a way to signal the system that a command method parameter
    /// is an option. Any command method parameter not decorated with the
    /// <see cref="ArgumentAttribute"/> is treated as an option, so this
    /// attribute is not required. The attribute is provided for consistency,
    /// completeness, and the explicitly minded.
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class OptionAttribute : Attribute { }
}
