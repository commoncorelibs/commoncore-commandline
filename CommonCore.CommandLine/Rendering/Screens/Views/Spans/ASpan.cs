﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System.IO;

namespace CommonCore.CommandLine.Rendering.Views.Spans
{
    public abstract class ASpan : ISpan
    {
        protected ASpan() { }

        public abstract int ContentLength { get; }

        public ISpan Root { get => this._root ?? this; private set => this._root = value ?? this; }
        private ISpan _root;

        public ISpan Parent { get; private set; }

        public int Start { get; private set; }

        public int End => this.Start + this.ContentLength;

        public virtual void RecalculatePositions(ISpan parent, int start)
        {
            this.Parent = parent;
            this.Root = parent.Root;
            this.Start = start;
        }

        public override string ToString()
            => this.ToString(ERenderMode.PlainText);

        public virtual string ToString(ERenderMode outputMode)
        {
            using var writer = new StringWriter();
            this.WriteTo(writer, outputMode);
            return writer.ToString();
        }

        public virtual void WriteTo(TextWriter writer, ERenderMode _)
            => writer.Write(this.ToString());
    }
}
