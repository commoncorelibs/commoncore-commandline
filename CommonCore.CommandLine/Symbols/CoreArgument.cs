﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine;
using System.CommandLine.Parsing;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.Symbols
{
    public delegate object ConvertArgumentDelegate(ArgumentResult argumentResult);

    public class CoreArgument : Argument<object>
    {
        public CoreArgument(string name)
            : base(name) { }

        public CoreArgument(string name, ParseArgument<object> parseDelegate, bool isDefault = false)
            : base(name, parseDelegate, isDefault)
        {
            if (parseDelegate is null) { throw new ArgumentNullException(nameof(parseDelegate)); }
            if (!string.IsNullOrWhiteSpace(name)) { this.Name = name; }

            ////if (isDefault) { this.SetDefaultValueFactory(argumentResult => parse(argumentResult)); }
            //this.ParseDelegate = parseDelegate;
        }

        //public ConvertArgumentDelegate ParseDelegate
        //{
        //    get => this._parseDelegate;
        //    set
        //    {
        //        this._parseDelegate = value;
        //        ReflectionHelper.SetPropertyValue(this, "ConvertArguments", (ArgumentResult argumentResult, out object value) =>
        //        {
        //            var result = this._parseDelegate(argumentResult);

        //            if (string.IsNullOrEmpty(argumentResult.ErrorMessage))
        //            {
        //                value = result;
        //                return true;
        //            }
        //            else
        //            {
        //                value = this.ArgumentType.IsValueType ? Activator.CreateInstance(this.ArgumentType) : null;
        //                return false;
        //            }
        //        });
        //        //ReflectionHelper.SetPropertyValue(this, "ConvertArguments", del);
        //    }
        //}
        //protected ConvertArgumentDelegate _parseDelegate = null;

        protected delegate bool TryConvertArgumentDelegate(ArgumentResult argumentResult, out object value);
    }
}
