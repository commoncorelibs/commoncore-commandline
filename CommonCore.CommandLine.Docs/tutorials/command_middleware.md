﻿# Command Middleware

Middleware allows application specific code to be injected into the parsing pipeline. This is one of the most advanced parts of the system and requires a moderate amount of knowledge on how the system works. In most cases, middleware should not be needed.

Middleware is declared using the `MiddlewareReference` attribute, which takes a type and a method name pointing to the static method that should be called. If the type is not specified, then the declaring type of the marked command method will be used. Multiple middleware references can be declared. Middleware methods must be `static`, return `Task`, and accept exactly two parameters of types `InvocationContext` and `Func<InvocationContext, Task>`, respectively.

 The `MiddlewareReference` attribute also requires an order which can be specified with an `EMiddlewareOrder` enum value, an `int`, or both in which case the	the latter will be used as an offset to the former. This order determines when in the parsing pipeline the middleware method is called.

To use the example of a adding a global option, the global options themselves would be added to the command in an initializer, but the short-circuiting effect would be achieved with middleware, specifically with a `EMiddlewareOrder.EarlyExclusiveOption` or `EMiddlewareOrder.LateExclusiveOption` order, depending if the global option should preempt or not any enabled default global options.

Short-circuiting through middleware is accomplished by not awaiting a call to the specified `next` func parameter. To say it the other way, to avoid short-circuiting the rest of the parsing process, a middleware method **MUST** `await next(context);` before exiting.

As a demonstration, the including the **foo** option in the command line will short-circuit and print **FOO!!**, while the **bar** option will print **BAR!!** without short-circuiting and allow the command entry point method to print **Hello World!!**.

```csharp
/// <summary>
/// This is an example application showing of some features of <c>CommonCore.CommandLine</c>.
/// </summary>
[CommandEntryPoint]
[Initializer(nameof(InitializeTestGlobalOptions))]
[MiddlewareReference(EMiddlewareOrder.EarlyExclusiveOption, nameof(MiddlewareFoo)]
[MiddlewareReference(EMiddlewareOrder.EarlyExclusiveOption, nameof(MiddlewareBar)]
static void Main(IConsole console)
{
    console.Out.WriteLine("Hello World!!");
}

static void InitializeTestGlobalOptions(Command command)
{
    command.AddGlobalOption(new Option<bool>(new[] { "--foo" });
    command.AddGlobalOption(new Option<bool>(new[] { "--bar" });
}


static Task MiddlewareFoo(InvocationContext context, Func<InvocationContext, Task> next)
{
    if (!context.ParseResult.HasOption("--foo")) { await next(context); return; }
    console.Out.WriteLine("FOO!!");
}

static Task MiddlewareFoo(InvocationContext context, Func<InvocationContext, Task> next)
{
    if (context.ParseResult.HasOption("--bar"))
    {
        console.Out.WriteLine("BAR!!");
    }
    await next(context);
}
```

```shell
> example-app
Hello World!!
> example-app --foo
FOO!!
> example-app --bar
BAR!!
Hello World!!
```
