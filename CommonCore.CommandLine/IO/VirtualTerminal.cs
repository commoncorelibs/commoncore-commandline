﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.CommandLine;
using CommonCore.CommandLine.Rendering;

namespace CommonCore.CommandLine.IO
{
    public class VirtualTerminal : TerminalBase
    {
        private readonly VirtualTerminalMode _virtualTerminalMode;

        public VirtualTerminal(IConsole console, VirtualTerminalMode virtualTerminalMode)
            : base(console) => this._virtualTerminalMode = virtualTerminalMode;

        public override ConsoleColor BackgroundColor { get; set; }

        public override ConsoleColor ForegroundColor { get; set; }

        public override void ResetColor()
        {
            this.Console.Out.Write(AnsiBackColors.Default.Code);
            this.Console.Out.Write(AnsiForeColors.Default.Code);
        }

        public override void Clear()
            => this.Console.Out.Write(AnsiClears.EntireScreen.Code);

        //public override int CursorLeft
        //{
        //    get => System.Console.CursorLeft;
        //    set => this.Console.Out.Write(
        //        AnsiUtil.Cursor.Move.ToLocation(left: value + 1).Code);
        //}

        //public override int CursorTop
        //{
        //    get => System.Console.CursorTop;
        //    set => this.Console.Out.Write(AnsiUtil.Cursor.Move.ToLocation(top: value + 1).Code);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing) { this._virtualTerminalMode?.Dispose(); }
            base.Dispose(disposing);
        }

        //public override void SetCursorPosition(int left, int top)
        //    => this.Console.Out.Write(AnsiUtil.Cursor.Move.ToLocation(left: left + 1, top: top + 1).Code);

        public override void HideCursor()
            => this.Console.Out.Write(AnsiCursors.Visible.Close.Code);

        public override void ShowCursor()
            => this.Console.Out.Write(AnsiCursors.Visible.Open.Code);
    }
}
