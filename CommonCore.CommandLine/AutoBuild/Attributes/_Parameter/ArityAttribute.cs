﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    public class ArityZero : ArityAttribute { public ArityZero() : base(0, 0) { } }

    public class ArityOne : ArityAttribute { public ArityOne() : base(1, 1) { } }

    public class ArityZeroOrOne : ArityAttribute { public ArityZeroOrOne() : base(0, 1) { } }

    public class ArityZeroOrMore : ArityAttribute { public ArityZeroOrMore() : base(0, int.MaxValue) { } }

    public class ArityOneOrMore : ArityAttribute { public ArityOneOrMore() : base(1, int.MaxValue) { } }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class ArityAttribute : Attribute
    {
        public ArityAttribute(int maxValueCount)
            : this(0, maxValueCount) { }

        public ArityAttribute(int arityMin, int arityMax)
        {
            this.ArityMin = arityMin;
            this.ArityMax = arityMax;
        }

        public int ArityMin { get; }

        public int ArityMax { get; }

        public IArgumentArity Arity => new ArgumentArity(this.ArityMin, this.ArityMax);
    }
}
