﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.CommandLine;
using System.CommandLine.Binding;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.CommandLine.Parsing;
using System.IO;
using System.Threading;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering;
using CommonCore.CommandLine.Rendering.Views;

namespace CommonCore.CommandLine.Demo.Basics
{
    //[EnableDefaultExceptionReporting]
    [EnableDefaultErrorReporting]
    [EnableDefaultGlobalOptionHelp]
    [EnableDefaultLocalOptionLicense]
    [EnableDefaultLocalOptionVersion]
    [AppMITLicense]
    static class Program
    {
        /// <summary>
        /// This is a <c>demo</c> of a <b>dotnet</b> console <b>tool</b> that uses <i>CommonCore.CommandLine</i> wrapper around <em>System.CommandLine</em>.
        /// <para>
        /// The <c>CommonCore.CommandLine</c> library was used to generate this application from the types, parameters, and documentation comments of the <c>Program.Main()</c> method.
        /// </para>
        /// <code>
        /// Some code with
        ///     indented lines
        ///     and <c>xml</c>.
        /// </code>
        /// </summary>
        /// <remarks>Here is where I say some additional things about the program.</remarks>
        /// <returns>The output of the program.</returns>
        /// <param name="word">What is the word? By default, bird is the word.</param>
        /// <param name="numbers">Space-separated list of integer numbers.</param>
        /// <param name="things">Select enum value for things.</param>
        /// <param name="output">Write output to the given path instead of stdout.</param>
        /// <param name="verbose">Print additional information.</param>
        [CommandEntryPoint]
        [CommandReference(nameof(DoIt), true)]
        [ValidatorReference(nameof(ValidateWord))]
        static void Main(IConsole console,
            [Argument] string word = "bird", [Argument] int[] numbers = null,
            EThings things = EThings.All, FileInfo output = null, bool verbose = false)
        {
            console.Out.WriteLine($"{nameof(word)}    : {word ?? "null"}");
            console.Out.WriteLine($"{nameof(numbers)} : {(numbers is null ? "null" : string.Join(", ", numbers))}");
            console.Out.WriteLine($"{nameof(output)}  : {output?.FullName ?? "null"}");
            console.Out.WriteLine($"{nameof(verbose)} : {verbose}");
        }

        static string ValidateWord(CommandResult result)
        {
            var word = result.GetArgumentValueOrDefault<string>("word");
            return word == "bird" ? null : $"Invalid input: Have you not heard? The word is 'bird', not '{word}'.";
        }

        /// <summary>
        /// Test method with two required args.
        /// </summary>
        /// <param name="num">A required arg.</param>
        /// <param name="text">Another required arg.</param>
        static void DoIt(IConsole console, [Argument] int num, [Argument] string text)
        {
            console.Out.WriteLine("verb do-it");
            console.Out.WriteLine($"{nameof(num)}  : {num}");
            console.Out.WriteLine($"{nameof(text)} : {text ?? "null"}");
        }
    }

    public enum EThings : int
    {
        None = 0,
        One,
        Few,
        Some,
        Many,
        All
    }
}
