﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Reflection;
using CommonCore.CommandLine.Exceptions;
using CommonCore.CommandLine.Reflection;
using CommonCore.CommandLine.Symbols;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="InitializerReferenceAttribute"/> <c>class</c>
    /// provides support for command initializers.
    /// 
    /// <para>
    /// Initializers allow application specific code to be run on each command after it
    /// and any subcommands are created. Initializer methods are called from the bottom
    /// of the command tree to the top. Meaning subcommand initializers are called before
    /// the initializer of their parent commands. If multiple initializer references are
    /// provided, then they are called in declaration order. Initializers are provided to
    /// address edge cases that can't be accomplished with other means, such as adding a
    /// global option unique to the application.
    /// Multiple initializer references can be declared and they will be called in
    /// declaration order.
    /// In most cases, initializers shouldn't be needed.
    /// </para>
    /// 
    /// <para>
    /// The attribute takes a type and a method name that points to the method
    /// representing the initializer. If the type is not specified, then the declaring
    /// type of the marked method will be used.
    /// </para>
    /// 
    /// <para>
    /// Initializer methods must be <c>static</c>, have a return type of <see cref="void"/>,
    /// and accept exactly one parameter of type <see cref="CoreCommand"/>.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class InitializerReferenceAttribute : Attribute
    {
        public InitializerReferenceAttribute(string methodName)
            : this(null, methodName) { }

        public InitializerReferenceAttribute(Type declaringType, string methodName)
        {
            Thrower.If.IsEmpty(methodName, nameof(methodName)).Throw();

            this.DeclaringType = declaringType;
            this.MethodName = methodName;
        }

        /// <summary>
        /// Gets the type containing <see cref="MethodName"/> or <c>null</c>.
        /// </summary>
        public Type DeclaringType { get; }

        /// <summary>
        /// Gets the name of the referenced method.
        /// </summary>
        public string MethodName { get; }

        /// <summary>
        /// Gets the <see cref="MethodInfo"/> from the <see cref="DeclaringType"/> and <see cref="MethodName"/>
        /// members. If <see cref="DeclaringType"/> is <c>null</c>, then <paramref name="fallbackType"/> is
        /// used instead; if both are null, then an exception is thrown.
        /// </summary>
        /// <param name="fallbackType">The <see cref="Type"/> to use if <see cref="DeclaringType"/> is <c>null</c>.</param>
        /// <returns>The <see cref="MethodInfo"/> represented by the attribute instance.</returns>
        /// <exception cref="ArgumentNullException">If <see cref="DeclaringType"/> and <paramref name="fallbackType"/> are both <c>null</c>.</exception>
        public MethodInfo GetMethod(Type fallbackType)
        {
            var type = this.DeclaringType ?? fallbackType ?? throw new ArgumentNullException(nameof(fallbackType));
            var name = this.MethodName;
            var method = ReflectionHelper.GetMethodInfo(type, name);

            if (method is null)
            { throw new MethodAccessException($"Initializer reference method '{name}' on type '{type}' could not be found."); }

            if (!method.IsStatic)
            { throw new MethodAccessException($"Initializer reference method '{name}' on type '{type}' MUST be static."); }

            if (method.ReturnType != typeof(void))
            { throw new MethodAccessException($"Initializer reference method '{name}' on type '{type}' return type MUST be declared 'void'."); }

            var parameters = method.GetParameters();
            if (parameters.Length != 1 || parameters[0].ParameterType != typeof(CoreCommand))
            { throw new MethodAccessException($"Initializer reference method '{name}' on type '{type}' MUST accept only one parameter of type {typeof(CoreCommand)}."); }

            return method;
        }
    }
}
