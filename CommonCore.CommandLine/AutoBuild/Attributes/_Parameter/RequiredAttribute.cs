﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="RequiredAttribute"/> <c>class</c>
    /// provides a way to signal the system that a command method parameter
    /// is required despite any declared default value. The attribute can be
    /// applied to both argument and option parameters. Parameters declared
    /// without default values are treated as required and the attribute will
    /// be ignored in those cases.
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class RequiredAttribute : Attribute { }
}
