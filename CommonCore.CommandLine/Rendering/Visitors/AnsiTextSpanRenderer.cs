﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering.Visitors
{
    public class AnsiTextSpanRenderer : TextSpanRenderer
    {
        public AnsiTextSpanRenderer(IConsole console, Region region)
            : base(console.Out, region)
            => this.Console = console ?? throw new ArgumentNullException(nameof(console));

        private IConsole Console { get; }

        public override void VisitControlSpan(ControlSpan span)
            => this.Writer.Write(span.Ansi.Code);

        public override void VisitForegroundColorSpan(ForeColorSpan span)
            => this.Writer.Write(span.Ansi.Code);

        public override void VisitBackgroundColorSpan(BackColorSpan span)
            => this.Writer.Write(span.Ansi.Code);

        public override void VisitStyleSpan(StyleSpan span)
            => this.Writer.Write(span.Ansi.Code);

        //public override void VisitCursorControlSpan(CursorControlSpan span)
        //{
        //    //if (_styleControlCodeMappings.TryGetValue(span.Name, out var controlCode))
        //    //{ this.Writer.Write(controlCode.Code); }
        //    if (span.Ansi != default) { this.Writer.Write(span.Ansi.Code); }
        //}

        //private static Dictionary<string, Ansi> _styleControlCodeMappings { get; } =
        //    new Dictionary<string, Ansi>
        //    {
        //        [nameof(StyleSpan.AttributesOff)] = AnsiUtil.Text.AttributesOff,
        //        [nameof(StyleSpan.SlowBlinkOff)] = AnsiUtil.Text.SlowBlinkOff,
        //        [nameof(StyleSpan.SlowBlinkOn)] = AnsiUtil.Text.SlowBlinkOn,
        //        [nameof(StyleSpan.BrightOff)] = AnsiUtil.Text.BrightOff,
        //        [nameof(StyleSpan.BrightOn)] = AnsiUtil.Text.BrightOn,
        //        [nameof(StyleSpan.HiddenOn)] = AnsiUtil.Text.HiddenOn,
        //        [nameof(StyleSpan.ReverseOn)] = AnsiUtil.Text.ReverseOn,
        //        [nameof(StyleSpan.ReverseOff)] = AnsiUtil.Text.ReverseOff,
        //        [nameof(StyleSpan.ItalicOff)] = AnsiUtil.Text.ItalicOff,
        //        [nameof(StyleSpan.ItalicOn)] = AnsiUtil.Text.ItalicOn,
        //        [nameof(StyleSpan.UnderlinedOff)] = AnsiUtil.Text.UnderlinedOff,
        //        [nameof(StyleSpan.UnderlinedOn)] = AnsiUtil.Text.UnderlinedOn,
        //    };
    }
}
