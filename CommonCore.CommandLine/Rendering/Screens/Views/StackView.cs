﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;

namespace CommonCore.CommandLine.Rendering.Views
{
    public class StackView : AContainerView
    {
        public StackView()
            : this(EOrientation.Vertical) { }

        public StackView(EOrientation orientation)
            => this.Orientation = orientation;

        public EOrientation Orientation { get; }

        public override void Render(IConsoleRenderer renderer, Region region = default)
        {
            switch (this.Orientation)
            {
                case EOrientation.Vertical: { this.RenderVertical(renderer, region); break; }
                case EOrientation.Horizontal: { this.RenderHorizontal(renderer, region); break; }
                default: { break; }
            }
        }

        public override Size Measure(IConsoleRenderer renderer, Size maxSize)
            => this.Orientation switch
            {
                EOrientation.Vertical => this.GetAdjustedSizeVertical(renderer, maxSize),
                EOrientation.Horizontal => this.GetAdjustedSizeHorizontal(renderer, maxSize),
                _ => throw new InvalidOperationException($"Orientation {this.Orientation} is not implemented"),
            };

        protected virtual void RenderVertical(IConsoleRenderer renderer, Region region = default)
        {
            var left = region.Left;
            var top = region.Top;
            var height = region.Height;

            foreach (var child in this.Views)
            {
                if (height <= 0) { break; }
                var size = child.Measure(renderer, new Size(region.Width, height));
                int renderHeight = Math.Min(height, size.Height);
                var r = new Region(left, top, size.Width, renderHeight);
                child.Render(renderer, r);
                top += size.Height;
                height -= renderHeight;
            }
        }

        protected virtual void RenderHorizontal(IConsoleRenderer renderer, Region region = default)
        {
            var left = region.Left;
            var top = region.Top;
            var width = region.Width;

            foreach (var child in this.Views)
            {
                if (width <= 0) { break; }
                var size = child.Measure(renderer, new Size(width, region.Height));
                var r = new Region(left, top, width, size.Height);
                child.Render(renderer, r);
                left += size.Width;
                width -= size.Width;
            }
        }

        protected virtual Size GetAdjustedSizeVertical(IConsoleRenderer renderer, Size maxSize)
        {
            int maxWidth = 0;
            int totHeight = 0;
            int height = maxSize.Height;

            foreach (var child in this.Views)
            {
                if (height <= 0) { break; }
                var size = child.Measure(renderer, new Size(maxSize.Width, height));
                height -= size.Height;
                totHeight += size.Height;
                maxWidth = Math.Max(maxWidth, size.Width);
            }

            return new Size(maxWidth, totHeight);
        }

        protected virtual Size GetAdjustedSizeHorizontal(IConsoleRenderer renderer, Size maxSize)
        {
            var maxHeight = 0;
            var totalWidth = 0;
            var width = maxSize.Width;

            foreach (var child in this.Views)
            {
                if (width <= 0)
                {
                    break;
                }
                var size = child.Measure(renderer, new Size(width, maxSize.Height));
                width -= size.Width;
                totalWidth += size.Width;
                maxHeight = Math.Max(maxHeight, size.Height);
            }

            return new Size(totalWidth, maxHeight);
        }
    }
}
