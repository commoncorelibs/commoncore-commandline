﻿# Command Arguments

Arguments are defined in much the same fashion as [options](command_options.md) with two exceptions; arguments don't have any aliases and an argument parameter MUST be prefixed with the `Argument` attribute. The rules for value type, default values, parsing, and being optional or required are the same for arguments as they are for options. Despite not having aliases, the name of the argument will be generated in the same manner as for options.

Declare two optional argument parameters to the `Main()` method, prefix them with the `Argument` attribute, and write their value to the console.

```csharp
[CommandEntryPoint]
static void Main(IConsole console,
    [Argument] string word = "bird", [Argument] int[] numbers = null,
    FileInfo output = null, bool verbose = false)
{
    console.Out.WriteLine("Hello World!!");
    console.Out.WriteLine($"{nameof(word)}:    {word ?? "null"}");
    console.Out.WriteLine($"{nameof(numbers)}: {(numbers is null ? "null" : string.Join(", ", numbers))}");
    console.Out.WriteLine($"{nameof(output)}: {output?.FullName ?? "null"}");
    console.Out.WriteLine($"{nameof(verbose)}: {verbose}");
}
```
```shell
> example-app
word:    bird
numbers: null
output:  null
verbose: False
> example-app -v false --output X:/some/dir/file.ext dog 8 6 7 5 3 0 9
word:    dog
numbers: 8, 6, 7, 5, 3, 0, 9
output:  X:/some/dir/file.ext
verbose: False
```
