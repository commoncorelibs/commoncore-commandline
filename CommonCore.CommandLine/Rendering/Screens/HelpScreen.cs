﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.CommandLine;
using System.CommandLine.Parsing;

namespace CommonCore.CommandLine.Rendering.Screens
{
    /// <summary>
    /// The <see cref="HelpScreen"/> <c>convienence</c> <c>class</c> provides an
    /// implementation of <see cref="Screen"/> populated with help for a specified
    /// <see cref="ParseResult"/>.
    /// </summary>
    public class HelpScreen : Screen
    {
        /// <summary>
        /// Gets the <c>singleton</c> <c>instance</c> of the <see cref="ErrorScreen"/> <c>class</c>.
        /// </summary>
        public static HelpScreen Instance { get; set; } = new HelpScreen();

        /// <summary>
        /// Build and render the contents of the screen for the current command.
        /// </summary>
        /// <param name="result">The <see cref="ParseResult"/> to build content from.</param>
        public void Show(ParseResult result)
            => this.Show(result, result.CommandResult.Command);

        /// <summary>
        /// Build and render the contents of the screen for the specified <paramref name="command"/>.
        /// </summary>
        /// <param name="result">The <see cref="ParseResult"/> to build content from.</param>
        /// <param name="command">The <see cref="ICommand"/> to build content for.</param>
        public void Show(ParseResult result, ICommand command)
            => this
            .AddHeader()
            .AddCopyright()
            .AddLicenseSummary()
            .Add()
            .AddSummary(command, false)
            .AddRemarks(command, false)
            .AddReturns(command, false)
            .AddErrors(result)
            .AddUsages(command)
            .AddArguments(command)
            .AddOptions(command)
            .AddSubcommands(command)
            .AddAdditionalArguments(command)
            .Render()
            ;
    }
}
