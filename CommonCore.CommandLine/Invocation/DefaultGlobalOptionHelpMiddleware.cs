﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.Threading.Tasks;
using CommonCore.CommandLine.Rendering.Screens;

namespace CommonCore.CommandLine.Invocation
{
    public class DefaultGlobalOptionHelpMiddleware : Middleware
    {
        public DefaultGlobalOptionHelpMiddleware()
            : base(EMiddlewareOrder.EarlyExclusiveOption, Middleware_HelpOption) { }

        private static async Task Middleware_HelpOption(InvocationContext context,
            Func<InvocationContext, Task> next)
        {
            if (!context.ParseResult.HasOption("--help")) { await next(context); return; }
            HelpScreen.Instance.Show(context.ParseResult);
        }
    }
}
