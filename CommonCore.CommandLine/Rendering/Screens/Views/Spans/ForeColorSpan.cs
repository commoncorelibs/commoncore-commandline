﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

namespace CommonCore.CommandLine.Rendering.Views.Spans
{
    public class ForeColorSpan : AColorSpan
    {
        public ForeColorSpan(string name, Ansi ansiControlCode)
            : base(name, ansiControlCode) { }

        public ForeColorSpan(Color color)
            : base(color, AnsiForeColors.Rgb(color.R, color.G, color.B)) { }

        public ForeColorSpan(byte r, byte g, byte b)
            : this(new Color(r, g, b)) { }

        public static ForeColorSpan Reset() => new ForeColorSpan(nameof(Reset), AnsiForeColors.Default);

        public static ForeColorSpan Black() => new ForeColorSpan(nameof(Black), AnsiForeColors.Black);

        public static ForeColorSpan Red() => new ForeColorSpan(nameof(Red), AnsiForeColors.Red);

        public static ForeColorSpan Green() => new ForeColorSpan(nameof(Green), AnsiForeColors.Green);

        public static ForeColorSpan Yellow() => new ForeColorSpan(nameof(Yellow), AnsiForeColors.Yellow);

        public static ForeColorSpan Blue() => new ForeColorSpan(nameof(Blue), AnsiForeColors.Blue);

        public static ForeColorSpan Magenta() => new ForeColorSpan(nameof(Magenta), AnsiForeColors.Magenta);

        public static ForeColorSpan Cyan() => new ForeColorSpan(nameof(Cyan), AnsiForeColors.Cyan);

        public static ForeColorSpan White() => new ForeColorSpan(nameof(White), AnsiForeColors.White);

        public static ForeColorSpan BrightBlack() => new ForeColorSpan(nameof(BrightBlack), AnsiForeColors.BrightBlack);

        public static ForeColorSpan BrightRed() => new ForeColorSpan(nameof(BrightRed), AnsiForeColors.BrightRed);

        public static ForeColorSpan BrightGreen() => new ForeColorSpan(nameof(BrightGreen), AnsiForeColors.BrightGreen);

        public static ForeColorSpan BrightYellow() => new ForeColorSpan(nameof(BrightYellow), AnsiForeColors.BrightYellow);

        public static ForeColorSpan BrightBlue() => new ForeColorSpan(nameof(BrightBlue), AnsiForeColors.BrightBlue);

        public static ForeColorSpan BrightMagenta() => new ForeColorSpan(nameof(BrightMagenta), AnsiForeColors.BrightMagenta);

        public static ForeColorSpan BrightCyan() => new ForeColorSpan(nameof(BrightCyan), AnsiForeColors.BrightCyan);

        public static ForeColorSpan BrightWhite() => new ForeColorSpan(nameof(BrightWhite), AnsiForeColors.BrightWhite);

        public static ForeColorSpan Rgb(byte r, byte g, byte b) => new ForeColorSpan(r, g, b);
    }
}
