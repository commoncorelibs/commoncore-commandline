﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.Linq;
using System.Reflection;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Symbols;

namespace CommonCore.CommandLine.AutoBuild.Extensions
{
    public static class BuildExtensions
    {
        public static bool HasCustomAttribute<TAttribute>(this Type self, bool inherit = false)
            where TAttribute : Attribute
            => self.IsDefined(typeof(TAttribute), inherit);

        public static bool HasCustomAttribute<TAttribute>(this MethodInfo self, bool inherit = false)
            where TAttribute : Attribute
            => self.IsDefined(typeof(TAttribute), inherit);

        public static bool HasCustomAttribute<TAttribute>(this ParameterInfo self, bool inherit = false)
            where TAttribute : Attribute
            => self.IsDefined(typeof(TAttribute), inherit);

        public static TValue GetDefaultValue<TValue>(this Type self)
            => (TValue)self.GetDefaultValue();

        public static TValue GetDefaultValue<TValue>(this ParameterInfo self)
            => (TValue)self.GetDefaultValue();

        public static object GetDefaultValue(this Type type)
            => type.IsValueType ? Activator.CreateInstance(type) : null;

        public static object GetDefaultValue(this ParameterInfo self)
            => self.DefaultValue is DBNull ? self.ParameterType.GetDefaultValue() : self.DefaultValue;

        public static TDelegate CreateDelegate<TDelegate>(this MethodInfo self, object target = null)
            where TDelegate : Delegate
            => (TDelegate)self.CreateDelegate(typeof(TDelegate), target);

        public static bool IsNullable(this ParameterInfo self)
        {
            //if (self.ParameterType.IsClass) { return true; }
            if (self.ParameterType.IsGenericType && self.ParameterType.GetGenericTypeDefinition() == typeof(Nullable<>))
            { return true; }
            if (self.HasDefaultValue && self.DefaultValue is null)
            { return true; }
            return false;
        }

        public static Argument BuildArgument(this ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            var name = parameter.Name.ToKebabCase();

            ParseArgument<object> parse = null;
            var parseMethod = parameter.GetCustomAttribute<ParseAttribute>()?.GetMethod(parameter.ParameterType, parameter.Member.DeclaringType);
            if (parseMethod != null) { parse = (ParseArgument<object>)parseMethod?.CreateDelegate(typeof(ParseArgument<object>)); }
            else { Services.Parsers.TryGetValue(parameter.ParameterType, out parse); }
            var argument = parse is null ? new CoreArgument(name) : new CoreArgument(name, parse);

            argument.AddAlias(name);
            argument.ArgumentType = parameter.ParameterType;
            argument.Arity = parameter.GetArity();
            argument.IsHidden = parameter.IsHidden();

            if (parameter.HasDefaultValue) { argument.SetDefaultValueFactory(parameter.GetDefaultValue); }

            var arity = parameter.GetArity();
            if (arity != null) { argument.Arity = parameter.GetArity(); }

            var choices = parameter.GetChoices();
            if (choices != null) { argument.FromAmong(choices); }

            return argument;
        }

        public static Option BuildOption(this ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            var name = parameter.Name.ToKebabCase();
            var argument = parameter.BuildArgument();
            var option = new Option(BuildOptionAliases(parameter))
            {
                Name = name,
                Argument = argument,
                Required = parameter.IsRequired(),
                IsHidden = parameter.IsHidden()
            };
            return option;
        }

        public static string[] BuildOptionAliases(this ParameterInfo parameter)
        {
            if (parameter is null) { throw new ArgumentNullException(nameof(parameter)); }

            var name = parameter.Name.ToKebabCase();
            return name.Length == 1
                ? (new[] { $"-{char.ToLowerInvariant(name[0])}" })
                : (new[] { $"--{name}", $"-{char.ToLowerInvariant(name[0])}" });
        }

        public static IArgumentArity GetArity(this ParameterInfo parameter)
            => parameter is null ? throw new ArgumentNullException(nameof(parameter))
            : parameter.GetCustomAttribute<ArityAttribute>()?.Arity;

        public static string[] GetChoices(this ParameterInfo parameter)
            => parameter is null ? throw new ArgumentNullException(nameof(parameter))
            : parameter.GetCustomAttribute<ChoicesAttribute>()?.Choices?.Select(o => o.ToString())?.ToArray()
            ?? (parameter.ParameterType.IsEnum
            ? Enum.GetNames(parameter.ParameterType)
            : null);

        public static bool IsRequired(this ParameterInfo parameter)
            => parameter is null ? throw new ArgumentNullException(nameof(parameter))
            : !parameter.HasDefaultValue || parameter.HasCustomAttribute<RequiredAttribute>();

        public static bool IsHidden(this ParameterInfo parameter)
            => parameter is null ? throw new ArgumentNullException(nameof(parameter))
            : parameter.HasCustomAttribute<HiddenAttribute>();

        public static bool IsArgument(this ParameterInfo parameter)
            => parameter is null ? throw new ArgumentNullException(nameof(parameter))
            : parameter.HasCustomAttribute<ArgumentAttribute>();

        public static bool IsOption(this ParameterInfo parameter)
            => parameter is null ? throw new ArgumentNullException(nameof(parameter))
            : parameter.HasCustomAttribute<OptionAttribute>() || !parameter.HasCustomAttribute<ArgumentAttribute>();

        public static IEnumerable<ParameterInfo> FilterAutoParameters(this MethodInfo method)
        {
            if (method is null) { throw new ArgumentNullException(nameof(method)); }
            return method.GetParameters()
                           // TODO: Should probably exclude types derived from those
                           // registered with services.
                           .Where(p => !CCUtils.OmittedTypes.Contains(p.ParameterType))
                           .Where(p => !Services.Instance.Keys.Contains(p.ParameterType));
        }
    }
}
