﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;

namespace CommonCore.CommandLine.AutoBuild.Factories
{
    public interface IServiceProvider<TService> : IServiceProvider, IEnumerable<Type>
    {
        int Count { get; }

        IReadOnlyCollection<Type> Keys { get; }

        IReadOnlyCollection<TService> Values { get; }

        TService this[Type key] { get; set; }

        TService this[int index] { get; }

        bool ContainsKey<TKey>();

        bool ContainsKey(Type key);

        bool TryGetValue<TKey>(out TService service);

        bool TryGetValue(Type key, out TService service);

        TService Get<TKey>();

        TService Get(Type key);

        void Set<TKey>(TService service);

        void Set(Type key, TService service);

        void Remove<TKey>();

        void Remove(Type key);
    }
}
