﻿# Auto Build

The auto-build system is the core component that allows the library to hide a huge amount of boilerplate code behind the scenes where the developer doesn't have to worry about it. When activated, the system generates a valid application entry point `Main()` method that wraps all the needed setup to parse the command line and pass the results to the logic of the application.

As with most of the library's features, the auto-build system has to be explicitly enabled before it will do anything.

## Enable Auto Build

To enable the auto-build system, add the following elments to the project's **csproj** file.

```xml
<EnableAutoBuild>true</EnableAutoBuild>
<GenerateDocumentationFile>true</GenerateDocumentationFile>
<DocumentationFile>$([System.IO.Path]::Combine($(MSBuildProjectDirectory), '$(AssemblyName).xml'))</DocumentationFile>
```

The first element, `EnableAutoBuild`, instructs the library to generate behind the scenes an appropriate `Main()` entry point method for the application. This allows the developer to define arguments and options for the application directly in the normal `Program.Main()` method created with the project. If this element is set as `false` or excluded entirely, then the application commands will have to be created manually.

The second element, `GenerateDocumentationFile`, ensures that the code documentation file is generated for the project. This documentation file is used by the library to generate help text for the application and any commands, options, and arguments it may have.

A third element, `DocumentationFile`, provides the location where the compiler should write the code documentation file. Of important note is that the name of the assembly is used as the documentation file name instead of the project name. This accounts for the cases where the two names differ, such as when the assembly name is set as part of a CICD process.

The library can also make use of the standard project elements `<Company>`, `<Copyright>`, `<Description>`, `<Product>`, and `<Version>`, so it is best to follow good practice and fill these elements with the appropriate information.

## Declare Command Entry Point

The auto-build system, and the library in general, avoids fishing for targets as it is a strategy prone to non-obvious errors. Instead, the system requires the command entry point method to be explicitly marked with the `CommandEntryPoint` attribute. When active, the system will throw an obvious exception if no methods are marked with the attribute or if more than one marked method is found. An exception will also be thrown if the marked method is not `static` or if it has a return type other than `void`, `int`, `Task`, or `Task<int>`.

```csharp
[CommandEntryPoint]
static void Main()
```
```csharp
[CommandEntryPoint]
static int Main()
```
```csharp
[CommandEntryPoint]
static Task Main()
```
```csharp
[CommandEntryPoint]
static Task<int> Main()
```
