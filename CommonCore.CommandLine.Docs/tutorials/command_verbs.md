﻿# Command Verbs

Verbs, aka subcommands, are defined by adding one or more `CommandReference` attributes to a command method. These attributes take a type and a method name that points to the command method representing the verb. If the type is not specified, then the declaring type of the marked command method will be used. In this way, an arbitrarily nested hierarchy of explicitly related commands is simple to create and trivial for the library to parse. Command methods must be `static` and have a return type of `void`, `int`, `Task`, or `Task<int>`.

```csharp
[CommandEntryPoint]
[CommandReference(nameof(DoIt))]
static void Main(IConsole console,
    [Argument] string word = "bird", [Argument] int[] numbers = null,
    FileInfo output = null, bool verbose = false)
{
    console.Out.WriteLine("Hello World!!");
    console.Out.WriteLine($"{nameof(word)}:    {word ?? "null"}");
    console.Out.WriteLine($"{nameof(numbers)}: {(numbers is null ? "null" : string.Join(", ", numbers))}");
    console.Out.WriteLine($"{nameof(output)}: {output?.FullName ?? "null"}");
    console.Out.WriteLine($"{nameof(verbose)}: {verbose}");
}

/// <summary>
/// Test method with two required args.
/// </summary>
/// <param name="num">A required arg.</param>
/// <param name="text">Another required arg.</param>
static void DoIt(IConsole console, [Argument] int num, [Argument] string text)
{
    console.Out.WriteLine("verb do-it");
    console.Out.WriteLine($"{nameof(num)}  : {num}");
    console.Out.WriteLine($"{nameof(text)} : {text ?? "null"}");
}
```
