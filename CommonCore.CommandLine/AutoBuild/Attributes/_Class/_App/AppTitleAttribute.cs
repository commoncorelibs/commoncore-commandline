﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Reflection;
using CommonCore.CommandLine.Reflection;

namespace CommonCore.CommandLine.AutoBuild.Attributes
{
    /// <summary>
    /// The <see cref="AppTitleAttribute"/> <c>class</c> is used to provide the system
    /// with custom information that overrides the information in the implementing
    /// console application's assembly.
    /// 
    /// <para>
    /// The attribute should be applied to the implementing console application's main <c>class</c>,
    /// ie the <c>class</c> containing the <see cref="CommandEntryPointAttribute"/> <c>method</c>.
    /// This is generally the <c>Program</c> class.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class AppTitleAttribute : Attribute
    {
        /// <summary>
        /// Gets the custom set company name or the company name value of the assembly.
        /// </summary>
        public string Company
        {
            get => this._company ??= AssemblyHelper.GetAssembly().GetCompany();
            set => this._company = value;
        }
        private string _company = null;

        /// <summary>
        /// Gets the custom set title or the default title built from assembly values.
        /// See <see cref="BuildDefaultTitle(Assembly, bool)"/> for more details.
        /// </summary>
        public string Title
        {
            get => this._title ??= this.BuildDefaultTitle(AssemblyHelper.GetAssembly(), false);
            set => this._title = value;
        }
        private string _title = null;

        /// <summary>
        /// Gets the custom set version or the default version built from assembly values.
        /// See <see cref="BuildDefaultVersion(Assembly)"/> for more details.
        /// </summary>
        public string Version
        {
            get => this._version ??= this.BuildDefaultVersion(AssemblyHelper.GetAssembly());
            set => this._version = value;
        }
        private string _version = null;

        /// <summary>
        /// Determines if the application assembly was compiled in debug mode.
        /// </summary>
        /// <returns><c>true</c> if the application assembly was compiled in debug mode; otherwise <c>false</c>.</returns>
        public bool IsDebug()
            => AssemblyHelper.GetAssembly().GetConfiguration()?.Contains("debug", StringComparison.OrdinalIgnoreCase) ?? false;

        /// <summary>
        /// Builds the default title for the specified <paramref name="assembly"/>,
        /// optionally prepended by the company name. Several assembly values are checked
        /// and the first existent one is used as the title. First the product value is
        /// checked, then the title value, followed by the name value, and finally if no
        /// values have been found the literal string <c>UNKNOWN</c> will be return.
        /// </summary>
        /// <param name="assembly">The assembly for which to build a title.</param>
        /// <param name="prependCompany">Indicate if the company name should be prepended to the title.</param>
        /// <returns>The default title string.</returns>
        public string BuildDefaultTitle(Assembly assembly, bool prependCompany)
        {
            string product = assembly.GetProduct() ?? assembly.GetTitle() ?? assembly.GetShortName() ?? "UNKNOWN";
            return !prependCompany || string.IsNullOrEmpty(this.Company) ? product : $"{this.Company} {product}";
        }

        /// <summary>
        /// Builds the default title for the specified <paramref name="assembly"/>.
        /// Several assembly values are checked and the first existent one is used as the
        /// version. First the information version value is checked, then the version value,
        /// and finally if no value has existed the literal string <c>0.0.0</c> will be returned.
        /// </summary>
        /// <param name="assembly">The assembly for which to build a version.</param>
        /// <returns>The default version string.</returns>
        public string BuildDefaultVersion(Assembly assembly)
            => assembly.GetInformationalVersion() ?? assembly.GetVersion() ?? "0.0.0";
    }
}
