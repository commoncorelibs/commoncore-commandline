﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's
// standards and conventions.
//
// Original Source: https://github.com/dotnet/command-line-api
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright (c) .NET Foundation and Contributors
//
// All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System;
using System.CommandLine.IO;
using System.Text;
using System.Xml.Schema;
using CommonCore.CommandLine.Extensions;
using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering.Visitors
{
    public abstract class ASpanRenderer : ASpanVisitor
    {
        protected ASpanRenderer(Region region = default)
        {
            this.Region = region;
        }

        protected ASpanRenderer(IStandardStreamWriter writer, Region region = default)
        {
            this.Writer = writer ?? throw new ArgumentNullException(nameof(writer));
            this.Region = region;
        }

        private readonly StringBuilder _buffer = new StringBuilder();

        private bool _lastSpanEndedWithWhitespace;
        private int _cursorLeft = -1;
        private int _cursorTop = -1;

        protected IStandardStreamWriter Writer { get; }

        public virtual int PositionOnLine { get; protected set; }

        protected int LinesWritten { get; private set; }

        protected Region Region { get; }

        protected override void Start(ISpan span)
            => this.TrySetCursorPosition(this.Region.Left, this.Region.Top);

        // Note: Any changes to logic in this method must
        // be mirrored in SpanMeasuringRenderer.
        public override void VisitContentSpan(ContentSpan span)
        {
            var text = span.ToString().AsMemory();

            // if text from the previous line was not truncated because the word was separated by an ANSI code, it should be truncated
            var skipWordRemainderFromPreviousLine =
                !this._lastSpanEndedWithWhitespace
                && this.PositionOnLine == 0
                && this.LinesWritten > 0
                && !text.Span.StartsWithWhitespace();

            foreach (var word in text.SplitForWrapping().SplitForWidth(this.RemainingWidthInRegion))
            {
                if (skipWordRemainderFromPreviousLine)
                {
                    skipWordRemainderFromPreviousLine = false;
                    continue;
                }

                if (word.IsNewLine()) { if (this.TryStartNewLine()) { this.FlushLine(); } }
                else if (!this.TryAppendWord(word.Span)) { break; }
            }
            this._lastSpanEndedWithWhitespace = text.Span.EndsWithWhitespace();

            if (this._buffer.Length > 0)
            {
                this.TryStartNewLine();
                this.Flush();
            }
        }

        protected override void Stop(ISpan span)
        {
            if (this.PositionOnLine > 0 || span.ContentLength == 0) { this.FlushLine(); }
            this.ClearRemainingHeight();
        }

        protected bool FilledRegionHeight => this.LinesWritten >= this.Region.Height;

        protected int RemainingWidthInRegion => this.Region.Width - this.PositionOnLine;

        protected virtual void TryClearRemainingWidth()
        {
            if (!this.Region.IsOverwrittenOnRender) { return; }
            this.ClearRemainingWidth();
        }

        protected void ClearRemainingWidth()
        {
            var remainingWidthOnLine = this.RemainingWidthInRegion;
            if (remainingWidthOnLine > 0)
            {
                this._buffer.Append(new string(' ', remainingWidthOnLine));
                this.PositionOnLine += remainingWidthOnLine;
            }
        }

        protected virtual void ClearRemainingHeight()
        {
            if (!this.Region.IsOverwrittenOnRender) { return; }
            while (this.TryStartNewLine()) { this.FlushLine(); }
        }

        protected void FlushLine()
        {
            this.TryClearRemainingWidth();
            this.Flush();
            this.LinesWritten++;
            this.PositionOnLine = 0;
        }

        protected void Flush()
        {
            this.Writer.Write(this._buffer.ToString());
            this._buffer.Clear();
        }

        protected bool TryAppendWord(ReadOnlySpan<char> value)
        {
            // omit whitespace if it's at the beginning of the line
            if (this.PositionOnLine == 0 && value.IsWhiteSpace() && this.LinesWritten > 0)
            { return true; }

            if (!this.TryStartNewLine()) { return false; }

            if (value.Length > this.RemainingWidthInRegion)
            {
                var endTrimmed = value.TrimEnd();
                if (endTrimmed.Length <= this.RemainingWidthInRegion)
                { value = endTrimmed; }
                else
                {
                    var chars = value;
                    while (chars.Length > this.RemainingWidthInRegion)
                    {
                        var text = chars.Slice(0, this.RemainingWidthInRegion);
                        chars = chars.Slice(this.RemainingWidthInRegion);

                        this._buffer.Append(text);
                        this.PositionOnLine += text.Length;

                        if (this.RemainingWidthInRegion <= 0)
                        { this.FlushLine(); }
                    }
                    if (chars.Length > 0)
                    {
                        this._buffer.Append(chars);
                        this.PositionOnLine += chars.Length;

                        if (this.RemainingWidthInRegion <= 0)
                        { this.FlushLine(); }
                    }
                    //this.FlushLine();
                }
            }
            else
            {
                this._buffer.Append(value);
                this.PositionOnLine += value.Length;

                if (this.RemainingWidthInRegion <= 0)
                { this.FlushLine(); }
            }

            return !this.FilledRegionHeight;
        }

        protected virtual bool TryStartNewLine()
        {
            if (this.FilledRegionHeight) { return false; }
            this.TrySetCursorPosition(this.Region.Left, this.Region.Top + this.LinesWritten);
            return true;
        }

        protected void TrySetCursorPosition(int left, int? top = null)
        {
            if (left == this._cursorLeft && top == this._cursorTop) { return; }

            this._cursorLeft = left;
            if (top != null) { this._cursorTop = top.Value; }
            this.SetCursorPosition(this._cursorLeft, this._cursorTop);
        }

        protected abstract void SetCursorPosition(int? left = null, int? top = null);
    }
}
