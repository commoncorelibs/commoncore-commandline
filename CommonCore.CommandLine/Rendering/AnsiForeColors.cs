﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerStepThrough]
    public static class AnsiForeColors
    {
        public static Ansi Black { get; } = Ansi.From('m', 30);
        public static Ansi Red { get; } = Ansi.From('m', 31);
        public static Ansi Green { get; } = Ansi.From('m', 32);
        public static Ansi Yellow { get; } = Ansi.From('m', 33);
        public static Ansi Blue { get; } = Ansi.From('m', 34);
        public static Ansi Magenta { get; } = Ansi.From('m', 35);
        public static Ansi Cyan { get; } = Ansi.From('m', 36);
        public static Ansi White { get; } = Ansi.From('m', 37);

        public static Ansi Rgb(byte r, byte g, byte b) => Ansi.From('m', 38, 2, r, g, b);

        public static Ansi Default => Ansi.From('m', 39);

        public static Ansi BrightBlack { get; } = Ansi.From('m', 90);
        public static Ansi BrightRed { get; } = Ansi.From('m', 91);
        public static Ansi BrightGreen { get; } = Ansi.From('m', 92);
        public static Ansi BrightYellow { get; } = Ansi.From('m', 93);
        public static Ansi BrightBlue { get; } = Ansi.From('m', 94);
        public static Ansi BrightMagenta { get; } = Ansi.From('m', 95);
        public static Ansi BrightCyan { get; } = Ansi.From('m', 96);
        public static Ansi BrightWhite { get; } = Ansi.From('m', 97);
    }
}
