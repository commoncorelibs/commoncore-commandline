﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine.Invocation;
using System.Diagnostics;

namespace CommonCore.CommandLine.Invocation
{

    [DebuggerDisplay("{OrderBase} + {OrderOffset} => {Order}, {Handler}")]
    public class Middleware
    {
        public Middleware(EMiddlewareOrder order, Action<InvocationContext> handler)
            : this(order, 0, handler) { }

        public Middleware(int order, Action<InvocationContext> handler)
            : this((EMiddlewareOrder)0, order, handler) { }

        public Middleware(EMiddlewareOrder orderBase, int orderOffset, Action<InvocationContext> handler)
            : this(orderBase, orderOffset, async (context, next) => { handler(context); await next(context); }) { }

        public Middleware(EMiddlewareOrder order, InvocationMiddleware handler)
            : this(order, 0, handler) { }

        public Middleware(int order, InvocationMiddleware handler)
            : this((EMiddlewareOrder)0, order, handler) { }

        public Middleware(EMiddlewareOrder orderBase, int orderOffset, InvocationMiddleware handler)
        {
            this.OrderBase = orderBase;
            this.OrderOffset = orderOffset;
            this.Handler = handler;
        }

        public EMiddlewareOrder OrderBase { get; }

        public int OrderOffset { get; }

        public InvocationMiddleware Handler { get; }

        public EMiddlewareOrder Order => this.OrderBase + this.OrderOffset;
    }
}
