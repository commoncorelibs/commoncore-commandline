﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using CommonCore.CommandLine.Rendering.Views.Spans;

namespace CommonCore.CommandLine.Rendering.Views
{
    public interface ITableViewColumn<T>
    {
        ColumnDefinition ColumnDefinition { get; }

        IView Header { get; }

        IView GetCell(T item, TextSpanFormatter formatter);
    }
}
