﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CommonCore.CommandLine.Exceptions
{
    [System.Runtime.InteropServices.ComVisible(true)]
    [Serializable]
    public class ArgumentEmptyException : ArgumentException
    {
        //public static void ThrowIf(string paramValue, string paramName)
        //{ if (paramValue is null || paramValue.Length == 0) { throw new ArgumentEmptyException(paramName); } }

        //public static void ThrowIf<T>(IReadOnlyCollection<T> paramValue, string paramName)
        //{ if (paramValue is null || paramValue.Count == 0) { throw new ArgumentEmptyException(paramName); } }

        //public static void ThrowIf<T>(IEnumerable<T> paramValue, string paramName)
        //{ if (paramValue is null || !paramValue.Any()) { throw new ArgumentEmptyException(paramName); } }

        public ArgumentEmptyException()
             : base("Value cannot be null or empty.") { }

        public ArgumentEmptyException(string paramName)
            : base($"Value cannot be null or empty. (Parameter '{paramName}')", paramName) { }

        public ArgumentEmptyException(string message, Exception innerException)
            : base(message, innerException) { }

        public ArgumentEmptyException(string paramName, string message)
            : base(message, paramName) { }

        [System.Security.SecurityCritical]
        protected ArgumentEmptyException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
