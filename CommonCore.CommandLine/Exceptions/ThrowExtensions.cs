﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace CommonCore.CommandLine.Exceptions
{
    public static class ThrowExtensions
    {
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static Thrower Add(this Thrower self, Exception exception)
        {
            if (exception is null) { throw new ArgumentNullException(nameof(exception)); }
            return (self ?? new Thrower()).AddException(exception);
        }

        public static Thrower IsNull(this Thrower self, object value, string paramName)
            => value is null
            ? self.Add(new ArgumentNullException(paramName))
            : self;

        public static Thrower IsNull<T>(this Thrower self, T? value, string paramName)
            where T : struct
            => !value.HasValue
            ? self.Add(new ArgumentNullException(paramName))
            : self;

        public static Thrower IsDefault(this Thrower self, ValueType value, string paramName)
            => value == default
            ? self.Add(new ArgumentDefaultException(paramName))
            : self;

        public static Thrower IsEmpty(this Thrower self, string value, string paramName)
            => value is null || value.Length == 0
            ? self.Add(new ArgumentEmptyException(paramName))
            : self;

        public static Thrower IsEmpty<T>(this Thrower self, IReadOnlyCollection<T> value, string paramName)
            => value is null || value.Count == 0
            ? self.Add(new ArgumentEmptyException(paramName))
            : self;

        public static Thrower IsEmpty<T>(this Thrower self, IEnumerable<T> value, string paramName)
            => value is null || !value.Any()
            ? self.Add(new ArgumentEmptyException(paramName))
            : self;

        //public static Thrower IsPositive(this Thrower self, long value, string paramName)
        //    => value > 0 ? self
        //    : self.Add(new ArgumentOutOfRangeException(paramName, "must be positive, but was " + value.ToString()));

        public static Thrower Throw(this Thrower self)
            => self?.ThrowExceptions();
    }
}
