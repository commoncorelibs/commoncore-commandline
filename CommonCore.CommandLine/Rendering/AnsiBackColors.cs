﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;

namespace CommonCore.CommandLine.Rendering
{
    [DebuggerStepThrough]
    public static class AnsiBackColors
    {
        public static Ansi Black { get; } = Ansi.From('m', 40);
        public static Ansi Red { get; } = Ansi.From('m', 41);
        public static Ansi Green { get; } = Ansi.From('m', 42);
        public static Ansi Yellow { get; } = Ansi.From('m', 43);
        public static Ansi Blue { get; } = Ansi.From('m', 44);
        public static Ansi Magenta { get; } = Ansi.From('m', 45);
        public static Ansi Cyan { get; } = Ansi.From('m', 46);
        public static Ansi White { get; } = Ansi.From('m', 47);

        public static Ansi Rgb(byte r, byte g, byte b) => Ansi.From('m', 48, 2, r, g, b);

        public static Ansi Default { get; } = Ansi.From('m', 49);

        public static Ansi BrightBlack { get; } = Ansi.From('m', 100);
        public static Ansi BrightRed { get; } = Ansi.From('m', 101);
        public static Ansi BrightGreen { get; } = Ansi.From('m', 102);
        public static Ansi BrightYellow { get; } = Ansi.From('m', 103);
        public static Ansi BrightBlue { get; } = Ansi.From('m', 104);
        public static Ansi BrightMagenta { get; } = Ansi.From('m', 105);
        public static Ansi BrightCyan { get; } = Ansi.From('m', 106);
        public static Ansi BrightWhite { get; } = Ansi.From('m', 107);
    }
}
