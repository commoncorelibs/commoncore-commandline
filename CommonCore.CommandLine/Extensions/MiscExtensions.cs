﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace CommonCore.CommandLine.Extensions
{
    public static class MiscExtensions
    {
        public static IEnumerable<T> Yield<T>(this T self) { yield return self; }

        public static bool EndsWithWhitespace(this ReadOnlySpan<char> value) =>
            value.Length > 0 && char.IsWhiteSpace(value[^1]);

        public static bool StartsWithWhitespace(this ReadOnlySpan<char> value) =>
            value.Length > 0 && char.IsWhiteSpace(value[0]);

        public static bool IsNewLine(this ReadOnlyMemory<char> self) => self.Span.IsNewLine();

        public static bool IsNewLine(this ReadOnlySpan<char> self)
            => self.Length == 1 ? self[0] == '\n' : self.Length == 2 && self[0] == '\r' && self[1] == '\n';

        public static EWordType GetWordType(this ReadOnlySpan<char> self, int index)
            => self[index] switch
            {
                '\n' => EWordType.SlashN,
                '\r' when index < self.Length - 1 && self[index + 1] == '\n' => EWordType.SlashRN,
                _ when char.IsWhiteSpace(self[index]) => EWordType.Whitespace,
                _ => EWordType.Char
            };

        public static ReadOnlySpan<char> Truncate(this ReadOnlySpan<char> self, int maxWidth)
        {
            if (maxWidth == 0) { return ReadOnlySpan<char>.Empty; }
            else if (maxWidth < 0 || self.Length <= maxWidth) { return self; }
            else
            {
                var trimmed = self.TrimEnd();
                return trimmed.Length <= maxWidth ? trimmed : self.Slice(0, maxWidth);
            }
        }

        public static IEnumerable<ReadOnlyMemory<char>> SplitForWidth(this IEnumerable<ReadOnlyMemory<char>> texts, int maxWidth)
        {
            var skip = maxWidth <= 0;
            foreach (var text in texts)
            {
                if (skip || text.Length < maxWidth || text.Span.IsNewLine()) { yield return text; }
                else
                {
                    for (int index = 0; index < text.Length; index += maxWidth)
                    {
                        int width = index + maxWidth > text.Length ? text.Length - index : maxWidth;
                        var output = text.Slice(index, width);
                        yield return output;
                    }
                }
            }
        }

        public static IEnumerable<ReadOnlyMemory<char>> SplitForWrapping(this ReadOnlyMemory<char> text)
        {
            var builder = new StringBuilder();
            var foundWhitespace = false;
            for (var index = 0; index < text.Length; index++)
            {
                var c = text.Span[index];
                var w = text.Span.GetWordType(index);

                if (w == EWordType.Whitespace)
                {
                    if (index > 0 && !foundWhitespace) { yield return __yieldValue(index); }
                    builder.Append(c);
                    foundWhitespace = true;
                }
                else if (w == EWordType.SlashN)
                {
                    if (builder.Length > 0) { yield return __yieldValue(index); }
                    yield return text.Slice(index, 1);
                }
                else if (w == EWordType.SlashRN)
                {
                    if (builder.Length > 0) { yield return __yieldValue(index); }
                    yield return text.Slice(index, 2);
                    index++;
                }
                else
                {
                    if (foundWhitespace) { yield return __yieldValue(index); }
                    builder.Append(c);
                }
            }

            if (builder.Length > 0) { yield return __yieldValue(text.Length); }

            ReadOnlyMemory<char> __yieldValue(int index)
            {
                foundWhitespace = false;
                var output = text.Slice(index - builder.Length, builder.Length);
                builder.Clear();
                
                return output;
            }
        }
    }

    public enum EWordType : int
    {
        Whitespace = 0,
        SlashN,
        SlashRN,
        Char
    }
}
